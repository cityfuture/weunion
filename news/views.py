from .models import News
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from .forms import NewsAdd
from django.core.exceptions import PermissionDenied
from allauth.account.decorators import verified_email_required
from weunion.models import Town
from datetime import datetime

#Добавить новость
@verified_email_required
def add(request, townslug):
    if(request.user.is_authenticated() and request.user.is_active):
        if request.user.isAllowedToModerate(request.session["town"], 'News'):
            if request.method == 'POST':
                town = Town.objects.get(pk=request.session["town"])
                form = NewsAdd(request.POST or None, request.FILES)
                #fill a model from a POST
                if form.is_valid():
                    instance = form.save(commit=False)
                    instance.town = town
                    instance.mainimg = form.cleaned_data['mainimg']
                    instance.author = request.user
                    instance.publish = 0
                    instance.save()
                    return redirect('../news/%s' % instance.id)
                else:
                    return render(request, 'add_news.html', {'form': form})
            else:
                form = NewsAdd()
                return render(request, 'add_news.html', {'form': form})
        else:
            raise PermissionDenied("Доступ заборонено")
    else:
        raise PermissionDenied("Доступ заборонено")

#Отобразить список новостей
def list(request, townslug):
    try:
        page = request.GET.get('page', 1)
    except PageNotAnInteger:
        page = 1

    if 'town' in request.session:
        if request.user.is_authenticated() and request.user.is_active and request.user.isAllowedToModerate(request.session["town"], 'News'):
            allowed = True
            news_list = News.objects.filter(town = request.session["town"]).all().order_by('-datetime_publish','publish')
        else:
            allowed = False
            news_list = News.objects.filter(town = request.session["town"], publish = 1).all().order_by('-datetime_publish')

        p = Paginator(news_list, 10, request=request)
        articles = p.page(page)
        return render(request, 'articles_list.html',{'articles': articles,'allowed': allowed})
    else:
        return redirect(reverse('regions'))


#Отобразить новость
def article(request, id, townslug):
       article = get_object_or_404(News,id=id)

       if not(request.session.has_key('town')):
           request.session['town'] = article.town.id
           request.session['town_name'] = article.town.name

       allowed = False
       if request.user.is_authenticated() and request.user.is_active:
            allowed = request.user.isAllowedToModerate(article.town.id, 'News')

       if (article.publish == False and allowed == False):
            raise PermissionDenied("Доступ заборонено")

       return render(request, 'article.html',{'article': article, 'allowed': allowed,})


#Удалить новость
def delete(request, id, townslug):
    if request.user.is_authenticated() and request.user.is_active and request.user.isAllowedToModerate(request.session["town"], 'News'):
        news = get_object_or_404(News,pk=id)
        news.delete()
        return redirect('../../news')



#Выставить статус опубликована
def publish(request,id, townslug):
    if request.user.is_authenticated() and request.user.is_active and request.user.isAllowedToModerate(request.session["town"], 'News'):
        news = get_object_or_404(News,pk=id)
        news.publish = 1
        if not(news.datetime_publish):
            news.datetime_publish = datetime.now()
        news.save()
        return redirect('../../news/'+id)

#Выставить статус снята с опубликации
def unpublish(request,id, townslug):
    if request.user.is_authenticated() and request.user.is_active and request.user.isAllowedToModerate(request.session["town"], 'News'):
        news = get_object_or_404(News,pk=id)
        news.publish = 0
        news.save()
        return redirect('../../news/'+id)


#Редактировать новость
def edit(request,id, townslug):
    if(request.user.is_authenticated() and request.user.is_active):
        if request.user.isAllowedToModerate(request.session["town"], 'News'):
            news = get_object_or_404(News,pk=id)
            form = NewsAdd(instance=news)
            if request.method == 'POST':
                form = NewsAdd(request.POST, request.FILES, instance=news)
                if form.is_valid():
                    instance = form.save(commit=False)
                    instance.mainimg = form.cleaned_data['mainimg']
                    instance.author = request.user
                    instance.publish = 0
                    instance.save()
                    return redirect('../../news/%s' % instance.id)
                else:
                    return render(request, 'edit_news.html', {'form': form,'id': id})
            else:
                 return render(request, 'edit_news.html', {'form': form,'id': id})
        else:
            raise PermissionDenied("Доступ заборонено")
    else:
        raise PermissionDenied("Доступ заборонено")
from django.contrib import admin
from .models import Issues, Subcontractors


class SubcontractorsAdmin(admin.ModelAdmin):
	list_display = ("name","town_ref")

admin.site.register(Subcontractors,SubcontractorsAdmin)
admin.site.register(Issues)
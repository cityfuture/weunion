-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.22-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица weunion.account_emailaddress
CREATE TABLE IF NOT EXISTS `account_emailaddress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `account_emailaddress_user_id_2b58522adc41d7c8_fk_auth_user_id` (`user_id`),
  CONSTRAINT `account_emailaddress_user_id_2b58522adc41d7c8_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.account_emailaddress: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `account_emailaddress` DISABLE KEYS */;
INSERT INTO `account_emailaddress` (`id`, `email`, `verified`, `primary`, `user_id`) VALUES
	(12, 'e.poremchuk@gmail.com', 1, 1, 14),
	(14, 'evgenia@cityfuture.org.ua', 0, 1, 20),
	(16, 'evgeniy@cityfuture.org.ua', 1, 1, 22),
	(17, 'office@aisogroup.coms', 0, 1, 23),
	(18, 'sdfgdhgj@adsfjsd.ua', 0, 1, 25),
	(22, 'evgeniy@rozumnemisto.org', 0, 1, 30);
/*!40000 ALTER TABLE `account_emailaddress` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.account_emailconfirmation
CREATE TABLE IF NOT EXISTS `account_emailconfirmation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `sent` datetime DEFAULT NULL,
  `key` varchar(64) NOT NULL,
  `email_address_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `acc_email_address_id_3cb19fb1f536708e_fk_account_emailaddress_id` (`email_address_id`),
  CONSTRAINT `acc_email_address_id_3cb19fb1f536708e_fk_account_emailaddress_id` FOREIGN KEY (`email_address_id`) REFERENCES `account_emailaddress` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.account_emailconfirmation: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `account_emailconfirmation` DISABLE KEYS */;
INSERT INTO `account_emailconfirmation` (`id`, `created`, `sent`, `key`, `email_address_id`) VALUES
	(12, '2015-12-23 22:40:12', '2015-12-23 22:40:13', '9zbpia72fikcldsvqv4dpsmbbwbwjd0i1fdocrhwgn5sozgvyublhvqjwit0nszz', 12),
	(24, '2016-01-14 20:46:30', NULL, 'tofy3ovtth9ar7rrctnypigaaj2cef3ute1vatnj7vkna4ayijqqym1mvoeibhxj', 12),
	(25, '2016-01-14 20:50:10', '2016-01-14 20:50:10', '3uhaelx4slkuhuls5bfcdrufh4xwv60le1mzyt2iyekw3xnlmnlbc1phauxbrdcs', 12),
	(28, '2016-01-31 10:24:37', '2016-01-31 10:24:37', 'rkteyfkuqkfxbrlgaheevoa7d11h2cr0i1vu2edgynuyio7f9xazacj6h3q9ttwb', 14),
	(36, '2016-02-02 18:19:25', '2016-02-02 18:19:28', 'ageboalsbvsqbmdrebmkbewk4mu8ndeg3ikfxatlngf02tpxlggdw1hrdpqpzgyc', 16),
	(37, '2016-02-08 20:56:31', '2016-02-08 20:56:37', '8mhwouzuid7kxh5nlhjxarssqbkzx5ipq4fgpkajwcoltsykqdq5arosxfsdm0oq', 17),
	(38, '2016-02-08 21:04:00', '2016-02-08 21:04:01', '83mywl2rfqzbtbezlnwse42bnehjcwcs65j3zcvrpqrzilm1ze1qjkzjrxbylawx', 17),
	(39, '2016-02-08 21:04:00', '2016-02-08 21:04:02', 'oquoe206vnrqs16diek7cm6jcwisz4bs8yapisyerbu4eis8rlr1gaf9wvxphmfx', 17),
	(40, '2016-03-20 20:57:38', '2016-03-20 20:57:40', 'sxxnf4zwh9tkha326gvbj4jrz2qlcajszw1zwqs0hwqgnewl6wi2cdlod7utf5l1', 18),
	(44, '2016-03-30 22:44:59', '2016-03-30 22:45:01', 'j26yquyc2fmurekqrsazfbkf8asbihzehhczhhbixmgqllwrl2lltkvbcwj7ec6r', 22);
/*!40000 ALTER TABLE `account_emailconfirmation` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.attachements
CREATE TABLE IF NOT EXISTS `attachements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_ref` int(11) NOT NULL,
  `document_ref` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attachement_message_idx` (`message_ref`),
  KEY `fk_attachement_document_idx` (`document_ref`),
  CONSTRAINT `fk_attachement_document` FOREIGN KEY (`document_ref`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_attachement_message` FOREIGN KEY (`message_ref`) REFERENCES `messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.attachements: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `attachements` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachements` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_group
CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_group: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` (`id`, `name`) VALUES
	(1, 'Admin'),
	(4, 'Citizen'),
	(8, 'Controller'),
	(2, 'Moderator'),
	(7, 'Moderator_Defects'),
	(5, 'Moderator_News'),
	(6, 'Moderator_Petitions'),
	(3, 'Subcontractor');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_group_permissions
CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group__permission_id_6a9d4c91a24890f6_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_6a9d4c91a24890f6_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_2b84595ed66a641f_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_group_permissions: ~85 rows (приблизительно)
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` (`id`, `group_id`, `permission_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 1, 4),
	(5, 1, 5),
	(6, 1, 6),
	(7, 1, 7),
	(8, 1, 8),
	(9, 1, 9),
	(13, 1, 13),
	(14, 1, 14),
	(15, 1, 15),
	(16, 1, 16),
	(17, 1, 17),
	(18, 1, 18),
	(19, 1, 19),
	(20, 1, 20),
	(21, 1, 21),
	(22, 1, 22),
	(23, 1, 23),
	(24, 1, 24),
	(25, 1, 25),
	(26, 1, 26),
	(27, 1, 27),
	(28, 1, 28),
	(29, 1, 29),
	(30, 1, 30),
	(31, 1, 31),
	(32, 1, 32),
	(33, 1, 33),
	(34, 1, 34),
	(35, 1, 35),
	(36, 1, 36),
	(43, 1, 43),
	(44, 1, 44),
	(45, 1, 45),
	(46, 1, 46),
	(47, 1, 47),
	(48, 1, 48),
	(49, 1, 49),
	(50, 1, 50),
	(51, 1, 51),
	(52, 1, 52),
	(53, 1, 53),
	(54, 1, 54),
	(55, 1, 55),
	(56, 1, 56),
	(57, 1, 57),
	(58, 1, 58),
	(59, 1, 59),
	(60, 1, 60),
	(61, 1, 61),
	(62, 1, 62),
	(63, 1, 63),
	(79, 2, 13),
	(80, 2, 14),
	(81, 2, 15),
	(82, 2, 34),
	(83, 2, 35),
	(84, 2, 36),
	(91, 2, 43),
	(92, 2, 44),
	(93, 2, 45),
	(94, 2, 46),
	(95, 2, 47),
	(96, 2, 48),
	(97, 2, 49),
	(98, 2, 50),
	(99, 2, 51),
	(100, 2, 52),
	(101, 2, 53),
	(102, 2, 54),
	(103, 2, 55),
	(104, 2, 56),
	(105, 2, 57),
	(106, 2, 58),
	(107, 2, 59),
	(108, 2, 60),
	(109, 2, 61),
	(110, 2, 62),
	(111, 2, 63),
	(125, 3, 55),
	(124, 3, 59),
	(127, 4, 55),
	(126, 4, 58);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_permission
CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  CONSTRAINT `auth_p_content_type_id_1f5e2b381dadf55_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=305 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_permission: ~265 rows (приблизительно)
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
	(1, 'Can add log entry', 1, 'add_logentry'),
	(2, 'Can change log entry', 1, 'change_logentry'),
	(3, 'Can delete log entry', 1, 'delete_logentry'),
	(4, 'Can add permission', 2, 'add_permission'),
	(5, 'Can change permission', 2, 'change_permission'),
	(6, 'Can delete permission', 2, 'delete_permission'),
	(7, 'Can add group', 3, 'add_group'),
	(8, 'Can change group', 3, 'change_group'),
	(9, 'Can delete group', 3, 'delete_group'),
	(13, 'Can add content type', 5, 'add_contenttype'),
	(14, 'Can change content type', 5, 'change_contenttype'),
	(15, 'Can delete content type', 5, 'delete_contenttype'),
	(16, 'Can add session', 6, 'add_session'),
	(17, 'Can change session', 6, 'change_session'),
	(18, 'Can delete session', 6, 'delete_session'),
	(19, 'Can add application', 7, 'add_application'),
	(20, 'Can change application', 7, 'change_application'),
	(21, 'Can delete application', 7, 'delete_application'),
	(22, 'Can add grant', 8, 'add_grant'),
	(23, 'Can change grant', 8, 'change_grant'),
	(24, 'Can delete grant', 8, 'delete_grant'),
	(25, 'Can add access token', 9, 'add_accesstoken'),
	(26, 'Can change access token', 9, 'change_accesstoken'),
	(27, 'Can delete access token', 9, 'delete_accesstoken'),
	(28, 'Can add refresh token', 10, 'add_refreshtoken'),
	(29, 'Can change refresh token', 10, 'change_refreshtoken'),
	(30, 'Can delete refresh token', 10, 'delete_refreshtoken'),
	(31, 'Can add cors model', 12, 'add_corsmodel'),
	(32, 'Can change cors model', 12, 'change_corsmodel'),
	(33, 'Can delete cors model', 12, 'delete_corsmodel'),
	(34, 'Can add attachements', 13, 'add_attachements'),
	(35, 'Can change attachements', 13, 'change_attachements'),
	(36, 'Can delete attachements', 13, 'delete_attachements'),
	(43, 'Can add comment attachements', 16, 'add_commentattachements'),
	(44, 'Can change comment attachements', 16, 'change_commentattachements'),
	(45, 'Can delete comment attachements', 16, 'delete_commentattachements'),
	(46, 'Can add comments', 17, 'add_comments'),
	(47, 'Can change comments', 17, 'change_comments'),
	(48, 'Can delete comments', 17, 'delete_comments'),
	(49, 'Can add documents', 18, 'add_documents'),
	(50, 'Can change documents', 18, 'change_documents'),
	(51, 'Can delete documents', 18, 'delete_documents'),
	(52, 'Can add files', 19, 'add_files'),
	(53, 'Can change files', 19, 'change_files'),
	(54, 'Can delete files', 19, 'delete_files'),
	(55, 'Can add issue files', 20, 'add_issuefiles'),
	(56, 'Can change issue files', 20, 'change_issuefiles'),
	(57, 'Can delete issue files', 20, 'delete_issuefiles'),
	(58, 'Can add issues', 11, 'add_issues'),
	(59, 'Can change issues', 11, 'change_issues'),
	(60, 'Can delete issues', 11, 'delete_issues'),
	(61, 'Can add messages', 21, 'add_messages'),
	(62, 'Can change messages', 21, 'change_messages'),
	(63, 'Can delete messages', 21, 'delete_messages'),
	(76, 'Can add country', 26, 'add_country'),
	(77, 'Can change country', 26, 'change_country'),
	(78, 'Can delete country', 26, 'delete_country'),
	(79, 'Can add region', 27, 'add_region'),
	(80, 'Can change region', 27, 'change_region'),
	(81, 'Can delete region', 27, 'delete_region'),
	(82, 'Can add city', 28, 'add_city'),
	(83, 'Can change city', 28, 'change_city'),
	(84, 'Can delete city', 28, 'delete_city'),
	(85, 'Can add IP range', 29, 'add_iprange'),
	(86, 'Can change IP range', 29, 'change_iprange'),
	(87, 'Can delete IP range', 29, 'delete_iprange'),
	(88, 'Can add email address', 30, 'add_emailaddress'),
	(89, 'Can change email address', 30, 'change_emailaddress'),
	(90, 'Can delete email address', 30, 'delete_emailaddress'),
	(91, 'Can add email confirmation', 31, 'add_emailconfirmation'),
	(92, 'Can change email confirmation', 31, 'change_emailconfirmation'),
	(93, 'Can delete email confirmation', 31, 'delete_emailconfirmation'),
	(94, 'Can add social application', 32, 'add_socialapp'),
	(95, 'Can change social application', 32, 'change_socialapp'),
	(96, 'Can delete social application', 32, 'delete_socialapp'),
	(97, 'Can add social account', 33, 'add_socialaccount'),
	(98, 'Can change social account', 33, 'change_socialaccount'),
	(99, 'Can delete social account', 33, 'delete_socialaccount'),
	(100, 'Can add social application token', 34, 'add_socialtoken'),
	(101, 'Can change social application token', 34, 'change_socialtoken'),
	(102, 'Can delete social application token', 34, 'delete_socialtoken'),
	(103, 'Can add site', 35, 'add_site'),
	(104, 'Can change site', 35, 'change_site'),
	(105, 'Can delete site', 35, 'delete_site'),
	(112, 'Can add petitions', 38, 'add_petitions'),
	(113, 'Can change petitions', 38, 'change_petitions'),
	(114, 'Can delete petitions', 38, 'delete_petitions'),
	(115, 'Can add petitions activity', 39, 'add_petitionsactivity'),
	(116, 'Can change petitions activity', 39, 'change_petitionsactivity'),
	(117, 'Can delete petitions activity', 39, 'delete_petitionsactivity'),
	(118, 'Can add petitions statuses', 40, 'add_petitionsstatuses'),
	(119, 'Can change petitions statuses', 40, 'change_petitionsstatuses'),
	(120, 'Can delete petitions statuses', 40, 'delete_petitionsstatuses'),
	(121, 'Can add petitions voices', 41, 'add_petitionsvoices'),
	(122, 'Can change petitions voices', 41, 'change_petitionsvoices'),
	(123, 'Can delete petitions voices', 41, 'delete_petitionsvoices'),
	(124, 'Can add Mail category', 42, 'add_mailcategory'),
	(125, 'Can change Mail category', 42, 'change_mailcategory'),
	(126, 'Can delete Mail category', 42, 'delete_mailcategory'),
	(127, 'Can add Mail base template', 43, 'add_mailbasetemplate'),
	(128, 'Can change Mail base template', 43, 'change_mailbasetemplate'),
	(129, 'Can delete Mail base template', 43, 'delete_mailbasetemplate'),
	(130, 'Can add Mail auth settings', 44, 'add_mailfromemailcredential'),
	(131, 'Can change Mail auth settings', 44, 'change_mailfromemailcredential'),
	(132, 'Can delete Mail auth settings', 44, 'delete_mailfromemailcredential'),
	(133, 'Can add Mail from', 45, 'add_mailfromemail'),
	(134, 'Can change Mail from', 45, 'change_mailfromemail'),
	(135, 'Can delete Mail from', 45, 'delete_mailfromemail'),
	(136, 'Can add Mail Bcc', 46, 'add_mailbcc'),
	(137, 'Can change Mail Bcc', 46, 'change_mailbcc'),
	(138, 'Can delete Mail Bcc', 46, 'delete_mailbcc'),
	(139, 'Can add Mail template', 47, 'add_mailtemplate'),
	(140, 'Can change Mail template', 47, 'change_mailtemplate'),
	(141, 'Can delete Mail template', 47, 'delete_mailtemplate'),
	(142, 'Can add Mail file', 48, 'add_mailfile'),
	(143, 'Can change Mail file', 48, 'change_mailfile'),
	(144, 'Can delete Mail file', 48, 'delete_mailfile'),
	(145, 'Can add Mail Exception', 49, 'add_maillogexception'),
	(146, 'Can change Mail Exception', 49, 'change_maillogexception'),
	(147, 'Can delete Mail Exception', 49, 'delete_maillogexception'),
	(148, 'Can add Mail log', 50, 'add_maillog'),
	(149, 'Can change Mail log', 50, 'change_maillog'),
	(150, 'Can delete Mail log', 50, 'delete_maillog'),
	(151, 'Can add Mail log email', 51, 'add_maillogemail'),
	(152, 'Can change Mail log email', 51, 'change_maillogemail'),
	(153, 'Can delete Mail log email', 51, 'delete_maillogemail'),
	(154, 'Can add Mail group', 52, 'add_mailgroup'),
	(155, 'Can change Mail group', 52, 'change_mailgroup'),
	(156, 'Can delete Mail group', 52, 'delete_mailgroup'),
	(157, 'Can add Mail group email', 53, 'add_mailgroupemail'),
	(158, 'Can change Mail group email', 53, 'change_mailgroupemail'),
	(159, 'Can delete Mail group email', 53, 'delete_mailgroupemail'),
	(160, 'Can add Mail signal', 54, 'add_signal'),
	(161, 'Can change Mail signal', 54, 'change_signal'),
	(162, 'Can delete Mail signal', 54, 'delete_signal'),
	(163, 'Can add Signal log', 55, 'add_signallog'),
	(164, 'Can change Signal log', 55, 'change_signallog'),
	(165, 'Can delete Signal log', 55, 'delete_signallog'),
	(166, 'Can add signal deferred dispatch', 56, 'add_signaldeferreddispatch'),
	(167, 'Can change signal deferred dispatch', 56, 'change_signaldeferreddispatch'),
	(168, 'Can delete signal deferred dispatch', 56, 'delete_signaldeferreddispatch'),
	(169, 'Can add Mail API', 57, 'add_apikey'),
	(170, 'Can change Mail API', 57, 'change_apikey'),
	(171, 'Can delete Mail API', 57, 'delete_apikey'),
	(172, 'Can add Mail Tracking', 58, 'add_maillogtrack'),
	(173, 'Can change Mail Tracking', 58, 'change_maillogtrack'),
	(174, 'Can delete Mail Tracking', 58, 'delete_maillogtrack'),
	(175, 'Can add Mail Subscription', 59, 'add_mailsubscription'),
	(176, 'Can change Mail Subscription', 59, 'change_mailsubscription'),
	(177, 'Can delete Mail Subscription', 59, 'delete_mailsubscription'),
	(187, 'Can add comment', 63, 'add_comment'),
	(188, 'Can change comment', 63, 'change_comment'),
	(189, 'Can delete comment', 63, 'delete_comment'),
	(190, 'Can moderate comments', 63, 'can_moderate'),
	(191, 'Can add comment flag', 64, 'add_commentflag'),
	(192, 'Can change comment flag', 64, 'change_commentflag'),
	(193, 'Can delete comment flag', 64, 'delete_commentflag'),
	(194, 'Can add tag', 65, 'add_tag'),
	(195, 'Can change tag', 65, 'change_tag'),
	(196, 'Can delete tag', 65, 'delete_tag'),
	(197, 'Can add tagged item', 66, 'add_taggeditem'),
	(198, 'Can change tagged item', 66, 'change_taggeditem'),
	(199, 'Can delete tagged item', 66, 'delete_taggeditem'),
	(200, 'Can add entry', 67, 'add_entry'),
	(201, 'Can change entry', 67, 'change_entry'),
	(202, 'Can delete entry', 67, 'delete_entry'),
	(203, 'Can view all entries', 67, 'can_view_all'),
	(204, 'Can change status', 67, 'can_change_status'),
	(205, 'Can change author(s)', 67, 'can_change_author'),
	(206, 'Can add category', 68, 'add_category'),
	(207, 'Can change category', 68, 'change_category'),
	(208, 'Can delete category', 68, 'delete_category'),
	(212, 'Can add user', 71, 'add_user'),
	(213, 'Can change user', 71, 'change_user'),
	(214, 'Can delete user', 71, 'delete_user'),
	(215, 'Can add subcontractors', 72, 'add_subcontractors'),
	(216, 'Can change subcontractors', 72, 'change_subcontractors'),
	(217, 'Can delete subcontractors', 72, 'delete_subcontractors'),
	(218, 'Can add regions', 73, 'add_regions'),
	(219, 'Can change regions', 73, 'change_regions'),
	(220, 'Can delete regions', 73, 'delete_regions'),
	(221, 'Can add town', 74, 'add_town'),
	(222, 'Can change town', 74, 'change_town'),
	(223, 'Can delete town', 74, 'delete_town'),
	(224, 'Can add town types', 75, 'add_towntypes'),
	(225, 'Can change town types', 75, 'change_towntypes'),
	(226, 'Can delete town types', 75, 'delete_towntypes'),
	(227, 'Can add town banners', 76, 'add_townbanners'),
	(228, 'Can change town banners', 76, 'change_townbanners'),
	(229, 'Can delete town banners', 76, 'delete_townbanners'),
	(230, 'Can add modules', 77, 'add_modules'),
	(231, 'Can change modules', 77, 'change_modules'),
	(232, 'Can delete modules', 77, 'delete_modules'),
	(233, 'Can add town allowed modules', 78, 'add_townallowedmodules'),
	(234, 'Can change town allowed modules', 78, 'change_townallowedmodules'),
	(235, 'Can delete town allowed modules', 78, 'delete_townallowedmodules'),
	(236, 'Can add auth user karma', 79, 'add_authuserkarma'),
	(237, 'Can change auth user karma', 79, 'change_authuserkarma'),
	(238, 'Can delete auth user karma', 79, 'delete_authuserkarma'),
	(239, 'Can add town budgets', 80, 'add_townbudgets'),
	(240, 'Can change town budgets', 80, 'change_townbudgets'),
	(241, 'Can delete town budgets', 80, 'delete_townbudgets'),
	(242, 'Can add news', 70, 'add_news'),
	(243, 'Can change news', 70, 'change_news'),
	(244, 'Can delete news', 70, 'delete_news'),
	(245, 'Can add Survey', 81, 'add_survey'),
	(246, 'Can change Survey', 81, 'change_survey'),
	(247, 'Can delete Survey', 81, 'delete_survey'),
	(248, 'Can add Question Group', 82, 'add_questiongroup'),
	(249, 'Can change Question Group', 82, 'change_questiongroup'),
	(250, 'Can delete Question Group', 82, 'delete_questiongroup'),
	(251, 'Can add Question', 83, 'add_question'),
	(252, 'Can change Question', 83, 'change_question'),
	(253, 'Can delete Question', 83, 'delete_question'),
	(254, 'Can add Offered Answer', 84, 'add_offeredanswer'),
	(255, 'Can change Offered Answer', 84, 'change_offeredanswer'),
	(256, 'Can delete Offered Answer', 84, 'delete_offeredanswer'),
	(257, 'Can add answer', 85, 'add_answer'),
	(258, 'Can change answer', 85, 'change_answer'),
	(259, 'Can delete answer', 85, 'delete_answer'),
	(260, 'Can add medicines departments', 86, 'add_medicinesdepartments'),
	(261, 'Can change medicines departments', 86, 'change_medicinesdepartments'),
	(262, 'Can delete medicines departments', 86, 'delete_medicinesdepartments'),
	(263, 'Can add medicines hospitals', 87, 'add_medicineshospitals'),
	(264, 'Can change medicines hospitals', 87, 'change_medicineshospitals'),
	(265, 'Can delete medicines hospitals', 87, 'delete_medicineshospitals'),
	(266, 'Can add medicines items', 88, 'add_medicinesitems'),
	(267, 'Can change medicines items', 88, 'change_medicinesitems'),
	(268, 'Can delete medicines items', 88, 'delete_medicinesitems'),
	(269, 'Can add towns additions', 89, 'add_townsadditions'),
	(270, 'Can change towns additions', 89, 'change_townsadditions'),
	(271, 'Can delete towns additions', 89, 'delete_townsadditions'),
	(272, 'Can add town gromada', 90, 'add_towngromada'),
	(273, 'Can change town gromada', 90, 'change_towngromada'),
	(274, 'Can delete town gromada', 90, 'delete_towngromada'),
	(275, 'Can add towns gromadas spr', 91, 'add_townsgromadasspr'),
	(276, 'Can change towns gromadas spr', 91, 'change_townsgromadasspr'),
	(277, 'Can delete towns gromadas spr', 91, 'delete_townsgromadasspr'),
	(278, 'Can add poll', 92, 'add_poll'),
	(279, 'Can change poll', 92, 'change_poll'),
	(280, 'Can delete poll', 92, 'delete_poll'),
	(281, 'Can add choice', 93, 'add_choice'),
	(282, 'Can change choice', 93, 'change_choice'),
	(283, 'Can delete choice', 93, 'delete_choice'),
	(284, 'Can add vote', 94, 'add_vote'),
	(285, 'Can change vote', 94, 'change_vote'),
	(286, 'Can delete vote', 94, 'delete_vote'),
	(287, 'Can add crowdfounding donates', 95, 'add_crowdfoundingdonates'),
	(288, 'Can change crowdfounding donates', 95, 'change_crowdfoundingdonates'),
	(289, 'Can delete crowdfounding donates', 95, 'delete_crowdfoundingdonates'),
	(290, 'Can add crowdfounding projects', 96, 'add_crowdfoundingprojects'),
	(291, 'Can change crowdfounding projects', 96, 'change_crowdfoundingprojects'),
	(292, 'Can delete crowdfounding projects', 96, 'delete_crowdfoundingprojects'),
	(293, 'Can add crowdfounding statuses', 97, 'add_crowdfoundingstatuses'),
	(294, 'Can change crowdfounding statuses', 97, 'change_crowdfoundingstatuses'),
	(295, 'Can delete crowdfounding statuses', 97, 'delete_crowdfoundingstatuses'),
	(296, 'Can add smartroads issues', 98, 'add_smartroadsissues'),
	(297, 'Can change smartroads issues', 98, 'change_smartroadsissues'),
	(298, 'Can delete smartroads issues', 98, 'delete_smartroadsissues'),
	(299, 'Can add smartroads statuses', 99, 'add_smartroadsstatuses'),
	(300, 'Can change smartroads statuses', 99, 'change_smartroadsstatuses'),
	(301, 'Can delete smartroads statuses', 99, 'delete_smartroadsstatuses'),
	(302, 'Can add auth user api keys', 100, 'add_authuserapikeys'),
	(303, 'Can change auth user api keys', 100, 'change_authuserapikeys'),
	(304, 'Can delete auth user api keys', 100, 'delete_authuserapikeys');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_user
CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_user: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `middle_name`, `email`, `is_staff`, `is_active`, `date_joined`, `phone`) VALUES
	(1, 'pbkdf2_sha256$20000$Oq28KtHXJ1YL$P1mHJ7mXLlGkPG/mBvxqJsgYgHQuGiFk9Al/XtnPI3k=', '2016-01-14 06:52:46', 1, 'evil', 'Злой', 'Сатансон', 'Батькович', 'evil@mega-bot.com', 1, 1, '2015-11-18 14:05:59', ''),
	(14, 'pbkdf2_sha256$20000$Q6ildqdl1JrA$wfmE16v08Y/QjFIsIbzEjmHLDaUTEXwoisXTfDY+7Ns=', '2016-05-04 16:07:02', 1, 'e.poremchuk', 'Евгений', 'Поремчук', 'Володимирович', 'e.poremchuk@gmail.com', 1, 1, '2015-12-23 22:40:12', '+380978486092'),
	(20, 'pbkdf2_sha256$20000$XfzoTQR1LOZb$+KQNFw+55oQ09LZHeGeJgM9ImiUt0nttLib4dZ2PXcA=', '2016-01-31 10:24:37', 0, 'evgenia', 'Евгений', 'поремчук', 'Владимирович', 'evgenia@cityfuture.org.ua', 0, 1, '2016-01-31 10:24:37', '097848092'),
	(22, 'pbkdf2_sha256$20000$37gXn55aDLME$aypbO62GChFiaO3KWyOevA3TtPMxUjY3GUwBJIORFKA=', '2016-02-03 08:53:27', 0, 'evgeniy', 'Тест', 'Михайлець', 'Тестович', 'evgeniy@cityfuture.org.ua', 0, 1, '2016-02-02 18:19:25', '5452415545454'),
	(23, 'pbkdf2_sha256$20000$W6P5IOskAXWf$jwibvr7tPtJcpil8jjvwzAmUni9OzTrK7nrCfFunlkk=', '2016-02-08 20:56:37', 0, 'dfgdfgdgdfgd', 'dfgdfgdgdfgd', 'Ntcn', 'sgdfgsdfg', 'office@aisogroup.coms', 0, 1, '2016-02-08 20:56:31', '34534523453534'),
	(24, 'pbkdf2_sha256$20000$2ig3cNU8U4cG$Yv8f8wjB/1SrlT5Z6ymeceEW3NahI4RbMIZaU4Q90vE=', NULL, 0, 'fsdfdsfsdfsd', 'fsdfdsfsdfsd', 'sdfsdfsdf', '', 'dsfgsdfg@dfhfhf.ua', 0, 1, '2016-03-20 20:54:56', NULL),
	(25, 'pbkdf2_sha256$20000$d8vhVGlqWXtM$Ja2wDeD9r4bQy8wJCb0x21Ufcw6llwMmMvjNOXVWCaM=', '2016-03-20 20:57:40', 0, 'aaasasdadasd', 'Aaasasdadasd', 'Asd\'adasdasd', 'Sadsdфыовп', 'sdfgdhgj@adsfjsd.ua', 0, 1, '2016-03-20 20:57:38', '324234234234'),
	(30, 'pbkdf2_sha256$20000$fT7yffhQ3vsY$jIz1LXfNkf0s7h+DzvOCfDKqKwgaPNlU0lvaABDBubc=', '2016-03-30 22:45:01', 0, 'sdfsdfsdf', 'Sdfsdfsdf', 'Fsfsdfsdfs', 'Sdfsdfsdf', 'evgeniy@rozumnemisto.org', 0, 1, '2016-03-30 22:44:58', '3242432423');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_user_api_keys
CREATE TABLE IF NOT EXISTS `auth_user_api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apikey` varchar(33) DEFAULT NULL,
  `user_ref` int(11) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `isblock` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `apikey` (`apikey`),
  UNIQUE KEY `user_ref` (`user_ref`),
  CONSTRAINT `FK_auth_user_api_keys_auth_user` FOREIGN KEY (`user_ref`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Сохраняем ключики для АПИ';

-- Дамп данных таблицы weunion.auth_user_api_keys: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `auth_user_api_keys` DISABLE KEYS */;
INSERT INTO `auth_user_api_keys` (`id`, `apikey`, `user_ref`, `date_create`, `isblock`) VALUES
	(8, 'a1x4e80bs8omr6vumjxvw56hwggv4d', 14, '2016-03-24 20:12:46', 0);
/*!40000 ALTER TABLE `auth_user_api_keys` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_user_groups
CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_78e2bbe32c5ad14a_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_78e2bbe32c5ad14a_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE,
  CONSTRAINT `auth_user_groups_user_id_29b3000578cea70_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_user_groups: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` (`id`, `user_id`, `group_id`) VALUES
	(8, 1, 2),
	(9, 1, 3),
	(10, 1, 5),
	(11, 1, 6),
	(12, 1, 7),
	(6, 14, 1),
	(14, 14, 2),
	(7, 14, 5),
	(13, 14, 7);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_user_karma
CREATE TABLE IF NOT EXISTS `auth_user_karma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_ref` int(11) DEFAULT NULL,
  `points` int(5) DEFAULT NULL,
  `forwhat` varchar(255) NOT NULL COMMENT 'За что насчитано балы',
  `module_ref` int(11) NOT NULL COMMENT 'Модуль системы',
  `datetime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_auth_user_karma_auth_user` (`user_ref`),
  KEY `FK_auth_user_karma_modules` (`module_ref`),
  CONSTRAINT `FK_auth_user_karma_auth_user` FOREIGN KEY (`user_ref`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_auth_user_karma_modules` FOREIGN KEY (`module_ref`) REFERENCES `modules` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='Кармические баллы пользователя на сайте';

-- Дамп данных таблицы weunion.auth_user_karma: ~45 rows (приблизительно)
/*!40000 ALTER TABLE `auth_user_karma` DISABLE KEYS */;
INSERT INTO `auth_user_karma` (`id`, `user_ref`, `points`, `forwhat`, `module_ref`, `datetime`) VALUES
	(1, 14, -1, 'Для тестування', 2, '2016-01-29 12:14:47'),
	(2, 14, 3, 'Ще один тест', 1, '2016-01-29 12:15:37'),
	(3, 14, 2, 'Ще один тест', 1, '2016-01-29 14:12:54'),
	(4, 14, 1, 'Голосування за петицію', 2, '2016-01-29 16:42:13'),
	(5, 22, 1, 'Голосування за петицію', 2, '2016-02-02 19:10:26'),
	(6, 14, 10, 'Створення власної петиції, що пройшлв модерацію', 2, '2016-02-02 20:15:25'),
	(7, 14, 30, 'Ваша петиція набрала необхідну кількість голосів і пройшла їх перевірку', 2, '2016-02-02 21:36:17'),
	(8, 22, 10, 'Створення власної петиції, що пройшлв модерацію', 2, '2016-02-02 21:41:01'),
	(9, 14, 1, 'Голосування за петицію', 2, '2016-02-02 21:41:08'),
	(10, 22, -1, 'Забрано голос з петиції', 2, '2016-02-02 21:41:34'),
	(11, 14, 1, 'Голосування за петицію', 2, '2016-02-02 21:42:05'),
	(12, 14, 1, 'Коментар до дефекту', 1, '2016-02-03 01:39:34'),
	(13, 14, -5, 'Блокування коментаря', 1, '2016-02-03 01:44:31'),
	(14, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-02-03 01:45:44'),
	(15, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-02-03 02:32:49'),
	(16, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-02-03 02:41:19'),
	(17, 14, 1, 'Коментар до дефекту', 1, '2016-02-03 08:52:58'),
	(18, 22, 1, 'Коментар до дефекту', 1, '2016-02-03 08:53:36'),
	(19, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-02-03 09:07:31'),
	(20, 14, 1, 'Коментар до дефекту', 1, '2016-02-08 11:35:03'),
	(21, 14, 1, 'Коментар до дефекту', 1, '2016-02-08 11:35:04'),
	(22, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:49'),
	(23, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:51'),
	(24, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:52'),
	(25, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:52'),
	(26, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:52'),
	(27, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:53'),
	(28, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:53'),
	(29, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:53'),
	(30, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:53'),
	(31, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-04-28 21:32:53'),
	(32, 14, 10, 'Створення власної петиції, що пройшла модерацію', 2, '2016-05-02 03:52:34'),
	(33, 14, 10, 'Створення власної петиції, що пройшла модерацію', 2, '2016-05-02 03:54:01'),
	(34, 14, 10, 'Створення власної петиції, що пройшла модерацію', 2, '2016-05-02 03:54:58'),
	(35, 14, 10, 'Створення власної петиції, що пройшла модерацію', 2, '2016-05-02 03:56:10'),
	(36, 14, 1, 'Голосування за петицію', 2, '2016-05-02 03:58:41'),
	(37, 14, -1, 'Забрано голос з петиції', 2, '2016-05-02 03:59:38'),
	(38, 14, 1, 'Голосування за петицію', 2, '2016-05-02 03:59:40'),
	(39, 14, 1, 'Коментар до дефекту', 1, '2016-05-02 23:28:35'),
	(40, 14, -5, 'Блокування коментаря', 1, '2016-05-02 23:28:42'),
	(41, 14, 1, 'Коментар до дефекту', 1, '2016-05-02 23:29:21'),
	(42, 14, -5, 'Блокування коментаря', 1, '2016-05-02 23:29:27'),
	(43, 14, 1, 'Коментар до дефекту', 1, '2016-05-02 23:29:59'),
	(44, 14, 1, 'Коментар до дефекту', 1, '2016-05-02 23:29:59'),
	(45, 14, -5, 'Блокування коментаря', 1, '2016-05-02 23:30:07'),
	(46, 14, 5, 'Додано дефект який погодив модератор', 1, '2016-05-04 16:07:24');
/*!40000 ALTER TABLE `auth_user_karma` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_user_towns
CREATE TABLE IF NOT EXISTS `auth_user_towns` (
  `user_id` int(11) NOT NULL,
  `town_id` int(11) NOT NULL,
  KEY `fk_user_town_user_idx` (`user_id`),
  KEY `fk_user_town_town_idx` (`town_id`),
  CONSTRAINT `fk_user_town_town` FOREIGN KEY (`town_id`) REFERENCES `town` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_town_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_user_towns: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `auth_user_towns` DISABLE KEYS */;
INSERT INTO `auth_user_towns` (`user_id`, `town_id`) VALUES
	(14, 1),
	(1, 1),
	(1, 4),
	(1, 5),
	(20, 4),
	(22, 1),
	(23, 4),
	(25, 8),
	(30, 5);
/*!40000 ALTER TABLE `auth_user_towns` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_user_user_permissions
CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_u_permission_id_56d6f7a45ae63954_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_u_permission_id_56d6f7a45ae63954_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_1b955cd1bd2c9a55_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_user_user_permissions: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.auth_user_work_for
CREATE TABLE IF NOT EXISTS `auth_user_work_for` (
  `subcontractors_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `fk_work_rel_subcontructor_idx` (`subcontractors_id`),
  KEY `fk_work_rel_user_idx` (`user_id`),
  CONSTRAINT `fk_work_rel_subcontractors` FOREIGN KEY (`subcontractors_id`) REFERENCES `subcontractors` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_rel_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.auth_user_work_for: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `auth_user_work_for` DISABLE KEYS */;
INSERT INTO `auth_user_work_for` (`subcontractors_id`, `user_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(7, 14);
/*!40000 ALTER TABLE `auth_user_work_for` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) DEFAULT NULL,
  `body` varchar(2048) DEFAULT NULL,
  `attachements` tinyint(1) NOT NULL DEFAULT '0',
  `issue_ref` int(11) NOT NULL,
  `owner_ref` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `block` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_comment_task_idx` (`issue_ref`),
  KEY `fk_comment_owner_idx` (`owner_ref`),
  CONSTRAINT `fk_comment_issue` FOREIGN KEY (`issue_ref`) REFERENCES `issues` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_owner` FOREIGN KEY (`owner_ref`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.comments: ~18 rows (приблизительно)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`id`, `title`, `body`, `attachements`, `issue_ref`, `owner_ref`, `created`, `block`) VALUES
	(2, NULL, 'Чому раніше не виконали роботи? У мене дитина чекає', 1, 51, 14, '2016-01-19 13:12:09', 0),
	(3, NULL, 'Чомусь статус підкладається під статус', 0, 52, 14, '2016-01-19 13:19:57', 0),
	(4, NULL, 'Прикол конечно', 1, 52, 14, '2016-01-20 07:05:58', 0),
	(5, NULL, 'віааві', 1, 52, 14, '2016-01-20 07:09:19', 0),
	(6, NULL, 'sdf', 0, 62, 14, '2016-02-01 21:02:00', 1),
	(7, NULL, 'klkl', 0, 65, 14, '2016-02-01 21:53:34', 1),
	(8, NULL, '', 0, 56, 22, '2016-02-02 19:08:44', 1),
	(9, NULL, '', 0, 56, 22, '2016-02-02 19:10:06', 1),
	(10, NULL, '', 0, 56, 22, '2016-02-02 19:19:01', 1),
	(11, NULL, 'ів', 1, 72, 14, '2016-02-02 22:42:13', NULL),
	(12, NULL, 'А че?', 0, 73, 14, '2016-02-02 22:43:04', NULL),
	(13, NULL, 'Отлично! Что теперь?', 0, 74, 14, '2016-02-02 23:06:48', NULL),
	(14, NULL, 'Мій комментар', 0, 79, 14, '2016-02-03 00:21:44', NULL),
	(15, NULL, 'А на коли?', 0, 83, 14, '2016-02-03 01:05:35', 1),
	(16, NULL, 'фів', 0, 83, 14, '2016-02-03 01:39:34', NULL),
	(17, NULL, 'dsfdf', 0, 119, 14, '2016-05-02 23:28:35', 1),
	(18, NULL, 'sdfs', 0, 119, 14, '2016-05-02 23:29:21', 1),
	(19, NULL, 'saasd', 1, 119, 14, '2016-05-02 23:29:59', 1);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.comment_attachements
CREATE TABLE IF NOT EXISTS `comment_attachements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_ref` int(11) NOT NULL,
  `document_ref` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attachement_document_idx` (`document_ref`),
  KEY `fk_comment_attachement_comment_idx` (`comment_ref`),
  CONSTRAINT `fk_comment_attachement_comment` FOREIGN KEY (`comment_ref`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_attachement_document` FOREIGN KEY (`document_ref`) REFERENCES `documents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.comment_attachements: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `comment_attachements` DISABLE KEYS */;
INSERT INTO `comment_attachements` (`id`, `comment_ref`, `document_ref`) VALUES
	(2, 2, 4),
	(3, 4, 6),
	(4, 4, 7),
	(5, 11, 15),
	(6, 11, 16),
	(7, 19, 36);
/*!40000 ALTER TABLE `comment_attachements` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.corsheaders_corsmodel
CREATE TABLE IF NOT EXISTS `corsheaders_corsmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cors` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.corsheaders_corsmodel: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `corsheaders_corsmodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `corsheaders_corsmodel` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.django_admin_log
CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `djang_content_type_id_3a93d0377a2a9c77_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_6eb4e418183aef56_fk_auth_user_id` (`user_id`),
  CONSTRAINT `djang_content_type_id_3a93d0377a2a9c77_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_6eb4e418183aef56_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.django_admin_log: ~34 rows (приблизительно)
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
	(9, '2015-11-26 17:26:22', '1', 'Admin', 1, '', 3, 1),
	(10, '2015-11-26 17:27:30', '2', 'Moderator', 1, '', 3, 1),
	(11, '2015-11-26 17:28:23', '3', 'Subcontractor', 1, '', 3, 1),
	(12, '2015-11-26 17:29:01', '4', 'Citizen', 1, '', 3, 1),
	(13, '2015-12-01 01:01:53', '1', 'Evil Satanson', 2, 'Змінено groups.', 26, 1),
	(15, '2015-12-01 11:13:46', '2', 'Testing Tester', 2, 'Змінено password та groups.', 26, 1),
	(16, '2015-12-01 11:15:56', '2', 'Testing Tester', 2, 'Змінено groups.', 26, 1),
	(17, '2015-12-01 11:19:28', '3', 'Testing Tester', 1, '', 26, 1),
	(18, '2015-12-01 11:43:43', '3', 'Testing Tester', 2, 'Змінено password.', 26, 1),
	(19, '2015-12-01 11:43:50', '3', 'Testing Tester', 2, 'Змінено password.', 26, 1),
	(20, '2015-12-01 11:44:41', '3', 'Testing Tester', 2, 'Поля не змінені.', 26, 1),
	(21, '2015-12-01 11:44:48', '3', 'Testing Tester', 2, 'Поля не змінені.', 26, 1),
	(22, '2015-12-01 11:44:57', '3', 'Testing Tester', 2, 'Змінено password.', 26, 1),
	(23, '2015-12-01 11:45:16', '3', 'Testing Tester', 2, 'Поля не змінені.', 26, 1),
	(24, '2015-12-01 11:45:28', '3', 'Testing Tester', 2, 'Змінено password.', 26, 1),
	(25, '2015-12-01 11:47:12', '3', 'Testing Tester', 2, 'Змінено password.', 26, 1),
	(26, '2015-12-01 11:50:35', '3', 'Testing Tester', 2, 'Змінено password.', 26, 1),
	(27, '2015-12-01 11:52:38', '3', 'Testing Tester', 2, 'Змінено username.', 26, 1),
	(28, '2015-12-01 11:54:07', '3', 'Testing Tester', 2, 'Змінено is_staff.', 26, 1),
	(29, '2015-12-01 11:54:27', '3', 'Testing Tester', 2, 'Змінено password.', 26, 1),
	(30, '2015-12-01 11:54:47', '3', 'Testing Tester', 2, 'Змінено groups.', 26, 1),
	(31, '2015-12-01 11:58:21', '4', '', 1, '', 26, 1),
	(32, '2015-12-01 11:58:48', '4', 'Test2', 2, 'Змінено first_name.', 26, 1),
	(33, '2015-12-01 12:02:28', '4', 'Test2', 2, 'Змінено last_login.', 26, 1),
	(34, '2015-12-01 19:15:59', '3', 'Testing Tester', 2, 'Змінено password та groups.', 26, 1),
	(37, '2015-12-09 23:09:18', '3', 'Testing Tester', 2, 'Змінено towns.', 26, 1),
	(40, '2016-01-08 12:33:56', '1', 'News object', 1, '', 70, 14),
	(41, '2016-01-14 08:17:16', '1', 'Сатансон З. Б.', 2, 'Змінено work_for та towns.', 71, 1),
	(42, '2016-01-14 08:23:35', '1', 'Сатансон З. Б.', 2, 'Змінено groups.', 71, 1),
	(43, '2016-02-04 09:26:50', '1', 'Survey object', 1, '', 81, 14),
	(44, '2016-02-04 09:34:37', '1', 'QuestionGroup object', 1, '', 82, 14),
	(45, '2016-02-04 09:36:02', '1', 'OfferedAnswer object', 1, '', 84, 14),
	(46, '2016-02-04 09:36:19', '2', 'OfferedAnswer object', 1, '', 84, 14),
	(47, '2016-05-03 00:04:54', '4', 'dsafsdf', 1, '', 92, 14);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.django_content_type
CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_661ebe726f07a5e0_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.django_content_type: ~88 rows (приблизительно)
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
	(30, 'account', 'emailaddress'),
	(31, 'account', 'emailconfirmation'),
	(1, 'admin', 'logentry'),
	(85, 'anonsurvey', 'answer'),
	(84, 'anonsurvey', 'offeredanswer'),
	(83, 'anonsurvey', 'question'),
	(82, 'anonsurvey', 'questiongroup'),
	(81, 'anonsurvey', 'survey'),
	(3, 'auth', 'group'),
	(2, 'auth', 'permission'),
	(5, 'contenttypes', 'contenttype'),
	(12, 'corsheaders', 'corsmodel'),
	(95, 'crowdfunding', 'crowdfoundingdonates'),
	(96, 'crowdfunding', 'crowdfoundingprojects'),
	(97, 'crowdfunding', 'crowdfoundingstatuses'),
	(57, 'dbmail', 'apikey'),
	(43, 'dbmail', 'mailbasetemplate'),
	(46, 'dbmail', 'mailbcc'),
	(42, 'dbmail', 'mailcategory'),
	(48, 'dbmail', 'mailfile'),
	(45, 'dbmail', 'mailfromemail'),
	(44, 'dbmail', 'mailfromemailcredential'),
	(52, 'dbmail', 'mailgroup'),
	(53, 'dbmail', 'mailgroupemail'),
	(50, 'dbmail', 'maillog'),
	(51, 'dbmail', 'maillogemail'),
	(49, 'dbmail', 'maillogexception'),
	(58, 'dbmail', 'maillogtrack'),
	(59, 'dbmail', 'mailsubscription'),
	(47, 'dbmail', 'mailtemplate'),
	(54, 'dbmail', 'signal'),
	(56, 'dbmail', 'signaldeferreddispatch'),
	(55, 'dbmail', 'signallog'),
	(13, 'defects', 'attachements'),
	(16, 'defects', 'commentattachements'),
	(17, 'defects', 'comments'),
	(18, 'defects', 'documents'),
	(19, 'defects', 'files'),
	(20, 'defects', 'issuefiles'),
	(11, 'defects', 'issues'),
	(21, 'defects', 'messages'),
	(63, 'django_comments', 'comment'),
	(64, 'django_comments', 'commentflag'),
	(28, 'django_geoip', 'city'),
	(26, 'django_geoip', 'country'),
	(29, 'django_geoip', 'iprange'),
	(27, 'django_geoip', 'region'),
	(86, 'medicines', 'medicinesdepartments'),
	(87, 'medicines', 'medicineshospitals'),
	(88, 'medicines', 'medicinesitems'),
	(70, 'news', 'news'),
	(9, 'oauth2_provider', 'accesstoken'),
	(7, 'oauth2_provider', 'application'),
	(8, 'oauth2_provider', 'grant'),
	(10, 'oauth2_provider', 'refreshtoken'),
	(38, 'petitions', 'petitions'),
	(39, 'petitions', 'petitionsactivity'),
	(40, 'petitions', 'petitionsstatuses'),
	(41, 'petitions', 'petitionsvoices'),
	(93, 'polls', 'choice'),
	(92, 'polls', 'poll'),
	(94, 'polls', 'vote'),
	(6, 'sessions', 'session'),
	(35, 'sites', 'site'),
	(100, 'smartapi', 'authuserapikeys'),
	(98, 'smartroads', 'smartroadsissues'),
	(99, 'smartroads', 'smartroadsstatuses'),
	(33, 'socialaccount', 'socialaccount'),
	(32, 'socialaccount', 'socialapp'),
	(34, 'socialaccount', 'socialtoken'),
	(65, 'tagging', 'tag'),
	(66, 'tagging', 'taggeditem'),
	(79, 'weunion', 'authuserkarma'),
	(77, 'weunion', 'modules'),
	(73, 'weunion', 'regions'),
	(72, 'weunion', 'subcontractors'),
	(74, 'weunion', 'town'),
	(78, 'weunion', 'townallowedmodules'),
	(76, 'weunion', 'townbanners'),
	(80, 'weunion', 'townbudgets'),
	(90, 'weunion', 'towngromada'),
	(89, 'weunion', 'townsadditions'),
	(91, 'weunion', 'townsgromadasspr'),
	(75, 'weunion', 'towntypes'),
	(71, 'weunion', 'user'),
	(69, 'zinnia', 'author'),
	(68, 'zinnia', 'category'),
	(67, 'zinnia', 'entry');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.django_migrations
CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.django_migrations: ~37 rows (приблизительно)
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
	(1, 'contenttypes', '0001_initial', '2015-11-18 12:30:08'),
	(2, 'auth', '0001_initial', '2015-11-18 12:30:09'),
	(3, 'admin', '0001_initial', '2015-11-18 12:30:09'),
	(4, 'contenttypes', '0002_remove_content_type_name', '2015-11-18 12:30:09'),
	(5, 'auth', '0002_alter_permission_name_max_length', '2015-11-18 12:30:09'),
	(6, 'auth', '0003_alter_user_email_max_length', '2015-11-18 12:30:09'),
	(7, 'auth', '0004_alter_user_username_opts', '2015-11-18 12:30:09'),
	(8, 'auth', '0005_alter_user_last_login_null', '2015-11-18 12:30:09'),
	(9, 'auth', '0006_require_contenttypes_0002', '2015-11-18 12:30:09'),
	(10, 'sessions', '0001_initial', '2015-11-18 12:30:09'),
	(11, 'oauth2_provider', '0001_initial', '2015-11-18 14:05:32'),
	(12, 'oauth2_provider', '0002_08_updates', '2015-11-18 14:05:32'),
	(13, 'defects', '0001_initial', '2015-11-24 13:49:24'),
	(14, 'django_geoip', '0001_initial', '2015-11-26 18:58:08'),
	(15, 'account', '0001_initial', '2015-11-29 10:54:13'),
	(16, 'account', '0002_email_max_length', '2015-11-29 10:54:13'),
	(17, 'sites', '0001_initial', '2015-11-29 10:54:13'),
	(18, 'socialaccount', '0001_initial', '2015-11-29 10:54:14'),
	(19, 'socialaccount', '0002_token_max_lengths', '2015-11-29 10:54:14'),
	(20, 'dbmail', '0001_initial', '2015-12-14 06:08:07'),
	(21, 'dbmail', '0002_auto_20150321_1539', '2015-12-14 06:08:07'),
	(22, 'dbmail', '0003_maillog_backend', '2015-12-14 06:08:07'),
	(23, 'dbmail', '0004_auto_20150321_2214', '2015-12-14 06:08:08'),
	(24, 'dbmail', '0005_auto_20150506_2201', '2015-12-14 06:08:08'),
	(25, 'dbmail', '0006_auto_20150708_0714', '2015-12-14 06:08:08'),
	(26, 'dbmail', '0007_auto_20150708_2016', '2015-12-14 06:08:09'),
	(27, 'dbmail', '0008_auto_20151007_1918', '2015-12-14 06:08:09'),
	(28, 'account', '0003_auto_20151221_2221', '2015-12-21 20:22:32'),
	(29, 'defects', '0002_auto_20151221_2221', '2015-12-21 20:22:32'),
	(30, 'petitions', '0001_initial', '2015-12-21 20:22:32'),
	(31, 'django_comments', '0001_initial', '2016-01-05 03:30:54'),
	(32, 'django_comments', '0002_update_user_email_field_length', '2016-01-05 03:30:54'),
	(33, 'tagging', '0001_initial', '2016-01-05 03:30:54'),
	(34, 'zinnia', '0001_initial', '2016-01-05 03:31:06'),
	(35, 'zinnia', '0002_lead_paragraph_and_image_caption', '2016-01-05 03:31:06'),
	(36, 'news', '0001_initial', '2016-02-04 09:48:48'),
	(39, 'smartroads', '0001_initial', '2016-04-02 13:25:32');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.django_session
CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.django_session: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
	('piqbf0l6iwfi52rht75j853v9ow5x0in', 'NjVlYzA3ODYyZmJlYWVlMWE2ZDhlY2Q3NjY5N2VmOTMzOGFkMzRjMzp7InRvd25fbmFtZSI6InNlbG8iLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJhbGxhdXRoLmFjY291bnQuYXV0aF9iYWNrZW5kcy5BdXRoZW50aWNhdGlvbkJhY2tlbmQiLCJfbWVzc2FnZXMiOiJbW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzggXFx1MDQzMlxcdTA0MzhcXHUwNDM5XFx1MDQ0OFxcdTA0M2JcXHUwNDM4LlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDIzXFx1MDQ0MVxcdTA0M2ZcXHUwNDU2XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0NDNcXHUwNDMyXFx1MDQ1NlxcdTA0MzlcXHUwNDQ4XFx1MDQzYlxcdTA0MzggXFx1MDQ0ZlxcdTA0M2EgZS5wb3JlbWNodWsuXCJdXSIsIl9zZXNzaW9uX2V4cGlyeSI6MTgxNDQwMCwiX2F1dGhfdXNlcl9oYXNoIjoiYzY3OGMyODNmYmUxNjU4MDljYjhjYTc2MzhmZjhjMjEyYjdmNTljYSIsIl9hdXRoX3VzZXJfaWQiOiIxNCIsInRvd24iOjF9', '2016-05-25 01:47:05'),
	('zan6mbzui0mhe2o6ju9tnl75cgyau2fe', 'MGM4NGU5OWYzNmM0OGI4OWMwZDI3ZWJlMDhhY2JhZTlhNjBmZGIxNDp7InRvd25fbmFtZSI6Ilx1MDQxN1x1MDQzZVx1MDQ0MFx1MDQ0ZiIsInRvd24iOjUsIl9hdXRoX3VzZXJfaWQiOiIxNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImFsbGF1dGguYWNjb3VudC5hdXRoX2JhY2tlbmRzLkF1dGhlbnRpY2F0aW9uQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MTgxNDQwMCwiX2F1dGhfdXNlcl9oYXNoIjoiYzY3OGMyODNmYmUxNjU4MDljYjhjYTc2MzhmZjhjMjEyYjdmNTljYSJ9', '2016-05-25 16:37:04');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.django_site
CREATE TABLE IF NOT EXISTS `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.django_site: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
	(1, 'rozumnemisto.org', '"Розумне місто" - сервіс електронного врядування');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.documents
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_ref` int(11) NOT NULL,
  `file_ref` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `file_name` varchar(256) NOT NULL,
  `type_name` varchar(45) NOT NULL,
  `size` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_document_owner_idx` (`owner_ref`),
  KEY `fk_document_file_idx` (`file_ref`),
  CONSTRAINT `fk_document_file` FOREIGN KEY (`file_ref`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_owner` FOREIGN KEY (`owner_ref`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.documents: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` (`id`, `owner_ref`, `file_ref`, `name`, `file_name`, `type_name`, `size`, `created`) VALUES
	(27, 14, NULL, 'CzJhC9T7_wM.jpg', 'cf6636c3-c479-4507-a77b-752c52612ebf.jpg', 'image', 52706, '2016-03-20 20:47:54'),
	(28, 14, NULL, 'miNEp9EXVzo.jpg', '3c38d6f3-6b31-4926-964e-b2968b70d0dc.jpg', 'image', 48698, '2016-03-20 20:47:54'),
	(29, 14, NULL, 'o0G_qd0qyYw.jpg', 'aa82a2b3-e0ed-4825-92c4-fdfe4c291df3.jpg', 'image', 35657, '2016-03-20 20:47:54'),
	(30, 14, NULL, 'ODCoHtkVEuU.jpg', '125f26ac-314f-48be-a687-54e3df3fc79c.jpg', 'image', 55712, '2016-03-20 20:47:54'),
	(31, 14, NULL, 'Selection_001.png', '946962fa-5c2e-4539-aec4-733dee1044c4.png', 'image', 104885, '2016-03-20 20:47:54'),
	(32, 14, NULL, 'Selection_002.png', '0eaec2eb-738b-41e7-afec-efbbf86d1cb0.png', 'image', 158895, '2016-03-20 20:47:55'),
	(33, 14, NULL, '12325691_1107257576004553_1306308598_o (1).jpg', '82caf8a8-771a-4d9d-80c0-35f78f10aa9b.jpg', 'image', 299977, '2016-03-20 21:17:13'),
	(34, 14, NULL, '12887409_1107257569337887_2003260420_o.jpg', 'dcb347d5-f975-436e-b20b-7a783bf9fcb8.jpg', 'image', 313301, '2016-03-20 21:17:14'),
	(35, 14, NULL, '12516838_1107257586004552_1177112209_o.jpg', '04331d83-205c-4089-affa-106b5506969e.jpg', 'image', 363498, '2016-03-20 21:17:14'),
	(36, 14, NULL, 'avatarka.jpg', '4b2fa82d-dc91-4bab-b02e-cc4b7eae78ec.jpg', 'image', 68586, '2016-05-02 23:29:59'),
	(37, 14, NULL, 'radiviliv_otg.jpg', '02dd42d0-d789-45e6-91b4-706d970277ee.jpg', 'image', 55773, '2016-05-04 16:06:00');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.edata
CREATE TABLE IF NOT EXISTS `edata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) DEFAULT NULL,
  `trans_date` varchar(50) DEFAULT NULL,
  `recipt_bank` varchar(100) DEFAULT NULL,
  `recipt_name` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `payment_details` varchar(250) DEFAULT NULL,
  `payer_bank` varchar(100) DEFAULT NULL,
  `payer_edrpou` varchar(50) DEFAULT NULL,
  `recipt_edrpou` varchar(50) DEFAULT NULL,
  `payer_name` varchar(100) DEFAULT NULL,
  `recipt_mfo` varchar(50) DEFAULT NULL,
  `payer_mfo` varchar(50) DEFAULT NULL,
  `town_ref` int(11) NOT NULL,
  `rand_ind` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_edata_town` (`town_ref`),
  KEY `id` (`id`),
  CONSTRAINT `FK_edata_town` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.edata: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `edata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edata` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.files
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.files: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.issues
CREATE TABLE IF NOT EXISTS `issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_ref` int(11) NOT NULL,
  `parent_task_ref` int(11) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `assigned_to` int(11) DEFAULT NULL,
  `town_ref` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(2048) NOT NULL,
  `address` varchar(512) NOT NULL,
  `map_lon` varchar(128) DEFAULT NULL,
  `map_lat` varchar(128) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_task_owner_idx` (`owner_ref`),
  KEY `fk_task_town_idx` (`town_ref`),
  KEY `fk_task_parent_task_idx` (`parent_task_ref`),
  KEY `fk_task_assigned_to_subcontructor_idx` (`assigned_to`),
  CONSTRAINT `fk_task_owner` FOREIGN KEY (`owner_ref`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_task_parent_task` FOREIGN KEY (`parent_task_ref`) REFERENCES `issues` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_task_town` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.issues: ~19 rows (приблизительно)
/*!40000 ALTER TABLE `issues` DISABLE KEYS */;
INSERT INTO `issues` (`id`, `owner_ref`, `parent_task_ref`, `status`, `assigned_to`, `town_ref`, `title`, `description`, `address`, `map_lon`, `map_lat`, `created`) VALUES
	(103, 14, NULL, 0, 2, 1, 'Pfzdrfgasdfasdfasdfasf', 'asdfsadfsa', 'майдан Незалежності, 6, Кузнецовськ, Рівненська область, Україна, 34400', '25.85031509399414', '51.34760894636592', '2016-03-20 20:47:54'),
	(104, 14, NULL, 0, 2, 1, 'ФЫВФвфЫВФВвыапвыпвпыв', 'ывапвыпвыап', 'проспект Шевченка, Кузнецовськ, Рівненська область, Україна, 34400', '25.85383415222168', '51.344285047412896', '2016-03-20 21:17:13'),
	(117, 14, 103, 2, 2, 1, 'Змiна статусу', 'sad', '', NULL, NULL, '2016-05-02 23:25:56'),
	(118, 14, 117, 1, 2, 1, 'Змiна статусу', 'edfsd', '', NULL, NULL, '2016-05-02 23:26:09'),
	(119, 14, 118, 5, 2, 1, 'Змiна статусу', 'dw', '', NULL, NULL, '2016-05-02 23:26:22'),
	(120, 14, 119, 2, 2, 1, 'Змiна статусу', 'па', '', NULL, NULL, '2016-05-04 15:36:19'),
	(121, 14, 120, 1, 2, 1, 'Змiна статусу', 'авп', '', NULL, NULL, '2016-05-04 15:37:18'),
	(122, 14, 104, 2, 2, 1, 'Змiна статусу', 'sa', '', NULL, NULL, '2016-05-04 15:40:33'),
	(123, 14, 121, 2, 2, 1, 'Змiна статусу', 'hjk', '', NULL, NULL, '2016-05-04 15:44:05'),
	(124, 14, 123, 1, 2, 1, 'Змiна статусу', 'xc', '', NULL, NULL, '2016-05-04 15:45:04'),
	(125, 14, 124, 5, 2, 1, 'Змiна статусу', 'tr', '', NULL, NULL, '2016-05-04 15:50:45'),
	(126, 14, 125, 2, 2, 1, 'Змiна статусу', 'jh', '', NULL, NULL, '2016-05-04 15:52:52'),
	(127, 14, 126, 1, 2, 1, 'Змiна статусу', 'k;l', '', NULL, NULL, '2016-05-04 15:59:18'),
	(128, 14, 122, 1, 2, 1, 'Змiна статусу', '564', '', NULL, NULL, '2016-05-04 16:02:29'),
	(129, 14, 128, 2, 2, 1, 'Змiна статусу', '', '', NULL, NULL, '2016-05-04 16:04:03'),
	(130, 14, NULL, 0, 2, 1, 'Назва дефектуНазва дефектуНазва дефекту', 'Назва дефектуНазва дефектуНазва дефекту', 'бульвар Шевченка, 79, Маріуполь, Донецька область, Україна', '37.557334896409884', '47.10811994912506', '2016-05-04 16:05:59'),
	(131, 14, 130, 1, 3, 1, 'Змiна статусу', 'Вiдправлено на виконання до Кузнецовське міське комунальне підприємство', '', NULL, NULL, '2016-05-04 16:07:23'),
	(132, 14, 131, 2, 3, 1, 'Змiна статусу', 'іва', '', NULL, NULL, '2016-05-04 16:08:13'),
	(133, 14, 132, 1, 3, 1, 'Змiна статусу', 'ів', '', NULL, NULL, '2016-05-04 16:10:19');
/*!40000 ALTER TABLE `issues` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.issue_files
CREATE TABLE IF NOT EXISTS `issue_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_ref` int(11) NOT NULL,
  `document_ref` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attachement_document_idx` (`document_ref`),
  KEY `fk_taskfile_task_idx` (`issue_ref`),
  CONSTRAINT `fk_issuefile_document` FOREIGN KEY (`document_ref`) REFERENCES `documents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_issuefile_task` FOREIGN KEY (`issue_ref`) REFERENCES `issues` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.issue_files: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `issue_files` DISABLE KEYS */;
INSERT INTO `issue_files` (`id`, `issue_ref`, `document_ref`) VALUES
	(18, 103, 27),
	(19, 103, 28),
	(20, 103, 29),
	(21, 103, 30),
	(22, 103, 31),
	(23, 103, 32),
	(24, 104, 33),
	(25, 104, 34),
	(26, 104, 35),
	(27, 130, 37);
/*!40000 ALTER TABLE `issue_files` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `text` varchar(2048) DEFAULT NULL,
  `attachments` tinyint(1) NOT NULL DEFAULT '0',
  `sender_ref` int(11) NOT NULL,
  `rcpt_ref` int(11) NOT NULL,
  `hidden_sender` tinyint(1) NOT NULL DEFAULT '0',
  `hidden_rcpt` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_messages_sender_ref_idx` (`sender_ref`),
  KEY `fk_messages_rcpt_ref_idx` (`rcpt_ref`),
  CONSTRAINT `fk_messages_rcpt_ref` FOREIGN KEY (`rcpt_ref`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_sender_ref` FOREIGN KEY (`sender_ref`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.messages: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `slug` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Інструрменти, що дозволені для міста';

-- Дамп данных таблицы weunion.modules: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `title`, `slug`) VALUES
	(1, 'Дефекти ЖКГ', 'defects'),
	(2, 'Петиції', 'petitions'),
	(3, 'Новини міста', 'news'),
	(4, 'Електронні послуги', 'igov'),
	(5, 'Відкритий бюджет', 'openbudget'),
	(6, 'Електронні закупівлі', 'prozorro'),
	(7, 'Реєстр ліків', 'medicines'),
	(8, 'Черги на житло', 'flats');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datetime_publish` timestamp NULL DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `shortdesc` varchar(300) NOT NULL,
  `text` mediumtext NOT NULL,
  `mainimg` varchar(250) NOT NULL DEFAULT 'media/news/noimage.jpg',
  `publish` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK_news_town` (`town`),
  KEY `FK_news_auth_user` (`author`),
  CONSTRAINT `FK_news_auth_user` FOREIGN KEY (`author`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `FK_news_town` FOREIGN KEY (`town`) REFERENCES `town` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Новости для городов';

-- Дамп данных таблицы weunion.news: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `town`, `author`, `datetime`, `datetime_publish`, `title`, `shortdesc`, `text`, `mainimg`, `publish`) VALUES
	(3, 1, 14, '2016-01-21 07:03:18', '2016-01-21 11:05:59', 'Місто Майбутнього проводить бізнес-тренінг для молоді', 'Дорогі друзі, ми проводимо безкоштовний бізнес-тренінг для учнів 9-11 класів. Спікер - Юрій Годунок.', '<p>Власне, новина</p>\r\n', 'news/2db39052-eb37-4ee5-8109-79fdbac5602f.jpg', 1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.oauth2_provider_accesstoken
CREATE TABLE IF NOT EXISTS `oauth2_provider_accesstoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `expires` datetime NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `d0043c04e7a0ca3852ab1c41e0331518` (`application_id`),
  KEY `oauth2_provider_accessto_user_id_bb8f4c3c5d706a6_fk_auth_user_id` (`user_id`),
  KEY `oauth2_provider_accesstoken_94a08da1` (`token`),
  CONSTRAINT `d0043c04e7a0ca3852ab1c41e0331518` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  CONSTRAINT `oauth2_provider_accessto_user_id_bb8f4c3c5d706a6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.oauth2_provider_accesstoken: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `oauth2_provider_accesstoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_provider_accesstoken` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.oauth2_provider_application
CREATE TABLE IF NOT EXISTS `oauth2_provider_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(100) NOT NULL,
  `redirect_uris` longtext NOT NULL,
  `client_type` varchar(32) NOT NULL,
  `authorization_grant_type` varchar(32) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `skip_authorization` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`),
  KEY `oauth2_provider_applica_user_id_4689117135815622_fk_auth_user_id` (`user_id`),
  KEY `oauth2_provider_application_9d667c2b` (`client_secret`),
  CONSTRAINT `oauth2_provider_applica_user_id_4689117135815622_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.oauth2_provider_application: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `oauth2_provider_application` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_provider_application` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.oauth2_provider_grant
CREATE TABLE IF NOT EXISTS `oauth2_provider_grant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `expires` datetime NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `D5737af6e25c6ff40993583a723c930b` (`application_id`),
  KEY `oauth2_provider_grant_user_id_476ecacfc78dc1fc_fk_auth_user_id` (`user_id`),
  KEY `oauth2_provider_grant_c1336794` (`code`),
  CONSTRAINT `D5737af6e25c6ff40993583a723c930b` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  CONSTRAINT `oauth2_provider_grant_user_id_476ecacfc78dc1fc_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.oauth2_provider_grant: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `oauth2_provider_grant` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_provider_grant` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.oauth2_provider_refreshtoken
CREATE TABLE IF NOT EXISTS `oauth2_provider_refreshtoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `access_token_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token_id` (`access_token_id`),
  KEY `D996876e4a97d7c4c5ac1ef825bda879` (`application_id`),
  KEY `oauth2_provider_refresh_user_id_70b17d227ca8ab96_fk_auth_user_id` (`user_id`),
  KEY `oauth2_provider_refreshtoken_94a08da1` (`token`),
  CONSTRAINT `D3f0b8ae52d35b61273050cc362e8901` FOREIGN KEY (`access_token_id`) REFERENCES `oauth2_provider_accesstoken` (`id`),
  CONSTRAINT `D996876e4a97d7c4c5ac1ef825bda879` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  CONSTRAINT `oauth2_provider_refresh_user_id_70b17d227ca8ab96_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.oauth2_provider_refreshtoken: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `oauth2_provider_refreshtoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_provider_refreshtoken` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.petitions
CREATE TABLE IF NOT EXISTS `petitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '0',
  `image` varchar(100) NOT NULL DEFAULT 'petitions/empty.gif',
  `resized` varchar(100) DEFAULT NULL,
  `text` varchar(1000) NOT NULL DEFAULT '0',
  `claim` varchar(1000) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `create_date` timestamp NULL DEFAULT NULL,
  `owner_user` int(11) NOT NULL,
  `resolution` varchar(3000) DEFAULT NULL,
  `town` int(11) NOT NULL,
  `when_approve` timestamp NULL DEFAULT NULL,
  `anonymous` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_petitions_auth_user` (`owner_user`),
  KEY `FK_petitions_petitions_statuses` (`status`),
  KEY `FK_petitions_town` (`town`),
  CONSTRAINT `FK_petitions_auth_user` FOREIGN KEY (`owner_user`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `FK_petitions_petitions_statuses` FOREIGN KEY (`status`) REFERENCES `petitions_statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_petitions_town` FOREIGN KEY (`town`) REFERENCES `town` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='Таблица представляет петиции';

-- Дамп данных таблицы weunion.petitions: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `petitions` DISABLE KEYS */;
INSERT INTO `petitions` (`id`, `title`, `image`, `resized`, `text`, `claim`, `status`, `create_date`, `owner_user`, `resolution`, `town`, `when_approve`, `anonymous`) VALUES
	(26, 'Подивитись на лавандові поля до Франції?? Ні до Кузнецовська.', 'petitions/863315cf-a7ed-4434-b987-9fc4e79e84d8.png', '', 'Навіщо їхати за кордон, робити візу і купувати дорогий квиток щоб подивитись на лавандові поля?? Якщо їх можна створити у нашому невеличкому містечку. Одразу скажу що я нікого не закликаю щоб не подорожувати, зрозумійте правильно. Ви тільки уявіть лавандові поля у нас в місті який запах цих чарівних рослин,і ми з Вами можемо ними насолоджуватись.', 'Засіяти лавандові поля провінції м. Кузнецовськ', 2, '2016-01-13 12:52:17', 14, 'df', 1, '2016-02-02 20:15:25', 0),
	(27, 'Разместить новогоднюю ёлку в центре парковой зоны, около детской площадки "аваль"', 'petitions/67e07f86-6a29-483e-bd32-39b52091c24b.png', '', 'Безопасность людей при проведении новогодних праздников.', 'Разместить новогоднюю ёлку в центре парковой зоны, около большой игровой площадки (бывший аваль). Такое положение ёлки будет радовать глаз горожан и гостей нашего города и сохранять их безопасность. Детвора может смело бегать и резвиться.', 6, '2016-01-14 15:00:50', 14, 'df', 1, '2016-02-02 21:36:16', 1),
	(28, 'Стінка для скелелазіння', 'petitions/2a24949b-bd32-40cb-bbcb-8752b345c26b.png', '', 'Скелелазіння — вид спорту, який полягає у вільному лазінні (найчастіше – сходженню) по природному (скелі) або штучному (скалодром) рельєфу. Це самостійний вид спорту, який вийшов з альпінізму й нерозривно пов\'язаний з ним. Кузнецовськ також міг би стати центром скелелазіння. Планувалося побудувати скеледром у ЗОШ №5, було розроблено проект, депутати міської ради виділили фінансування, проте управління освіти не дало дозвіл на будівництво, мотивуючи це тим, що скелелазіння – небезпечний вид спорту. Тому підлітки, замість того, щоб тренуватися у спортзалі школи під наглядом досвідчених наставників і з надійним страхуванням, їздити на змагання і показувати свої вміння серед однолітків на скеледромах і природніх скелях, поки що освоюють наш «довгобуд» у центрі міста і інші будівальні майданчики...', 'Підняти питання про встановлення скелелазної стінки у місті для розвитку дуже популярного виду спорту - скелелазіння. Діти в місти не знають чим зайнятись тому більшість молоді у свої молоді роки вже вживають алкогольні напої та тютюнові вироби тому давайте розвиватися у різних спортивних напрямках. Скелелазіння - гарне зайняття для теперішньої молоді. Крім того що корисно, та ще й дуже цікаво.', 7, '2016-01-14 17:03:04', 14, 'efd', 1, '2016-01-14 19:03:17', 0),
	(29, 'Пластикові кришки для протезів пораненим в ато.', 'petitions/6fb64a51-6195-4fed-a9e9-a5bf28ce5845.png', '', 'Вже декілька місяців в Україні набирає оберти акція від благодійного фонду ОВЕС «ПЛАСТИКОВІ КРИШКИ ДЛЯ ПРОТЕЗІВ ПОРАНЕНИМ В АТО». Мета акції – збір пластикових кришичок у спеціальні контейнери та подальша їх утилізація. Частина пластика піде на протезі, а непластмасові деталі та роботу буде оплачена з коштів, що отримуються з продажу сировини (перероблених кришичок). Протези виробляються у Польщі, індивідуально для кожного бійця. Все, що потрібно від громади – не викидати кришичку у сміття, а приносити у спеціальні контейнери. Окрім допомоги нашим захисникам, ми значно покращимо екологічну ситуацію, адже пластикові кришички розкладаються протягом 200 років! Рівне вже підключилось до акції, готуються Остріг та Сарни. До речі, акція проходить з узгодження Міністерства екології України.', '- Встановити контейнері для збору пластикових кришичок у громадських місцях. - Організувати пункти прийому пластикових кришичок. - Організувати збір кришичок у закладах харчування та кафе/барах, які погодяться прийняти участь у акції. - Виділити приміщення для зберігання кришечок. -Надати інформаційну підтримку акції. *Щодо вивозу – організація вивозить самостійно при об’ємі 200 кг.', 7, '2016-01-14 19:04:59', 14, NULL, 1, '2016-01-14 21:05:02', 0),
	(30, '<script>alert("Hi!")</script>', '', '', '<script>alert("Hi!")</script>', '<script>alert("Hi!")</script>', 3, '2016-01-16 03:05:31', 14, 'uyuy', 1, NULL, 0),
	(31, 'глроолрррррррррррррррр', '', '', 'олролллллллллллолрррррррррр', 'олрррррррррррррррррррррррлрлрлрл', 1, '2016-01-16 10:42:13', 14, NULL, 1, NULL, 0),
	(32, 'Моя тестова петиція без особливої мети', 'petitions/abc41da6-2290-4212-a576-f872f21718b9.JPG', '', 'Відвідування лікарні для кожного мешканця це жах. Довжелезні черги, де хворі чекають разом з здоровими людьми. Ні один лікар не прийме Вас без запису чи "талончика" і обов\'язкового так називаємого "благодійного внеску".І навіть прийшовши на свій час потрібно чекати. Лікарі які направляють купляти ліки в свої аптеки, або на прийом до себе в платний приватний кабінет по місту. Поводження з тобою як з нелюдом. Продажність лікарняних листків, а якщо людина дійсно хвора то складність їх отримати. В стаціонарах Вам дають величезні списки від мила і порошку для санітарок і кілометри марлі, які вони відразу ж здають назад в аптеку. І саме прикро що це не поодинокі випадки. І це при таких великих витратах з бютжету Кузнецовська на її утримання.', 'Я пропоную. Ввести можливість записуватись на прийом через Інтернет. Хворих з хворобами, що передаються повітряно-крапельним шляхом, приймали окремі лікарі. Так як в багатьох вже немає домашніх провідних телефонів, ввести мобільні номера в реєстратури. Неможливість лікарям приховувати списки запису до лікаря. Доплати і надбавки виплачувати тільки за фактичну велику кількість прийнятих хворих. На сайті СМСЧ створити можливість оставляти відгуки від хворих. При негативних відгуках вирішувати доцільність зайнятій посаді лікаря. Оновити склад лікарів. Навчити ставитись до пацієнтів з повагою і розумінням. Оперативно змінювати відпустки і відгули при необхідності (сезонні хвороби, масові медкомісії). Заборонити займатись одночасно підприємницькою діяльністю і бути держслужбовцем. Офіційно затвердити перелік "товарів" в стаціонари, а також слідкувати за їхнім використанням. Заборонити проймати лікарям будь-яку подяку (від цукерок до грошей). Скоротити неактуальні посади. Створити незалежний ', 1, '2016-02-02 18:21:49', 22, NULL, 1, NULL, 0),
	(33, 'Встановити фонтан в центры мыста', 'petitions/89bf53d0-4408-4b30-bad3-120aec6419f8.jpg', '', 'Нам не вистачає красивих дизайнерских фонтанів у мысты', 'Встановити дизайнерский фонтан на вул. Л.Українки навпроти ДК', 8, '2016-02-02 21:40:09', 22, NULL, 1, '2016-02-02 21:41:01', 0),
	(34, 'sdfdsddddddddddddddddddddddddddddddddddd', '', '', 'sdfsdfsdfsdfsdfffffffffffffffffffffffffffffffffffffffff\r\n', 'sdfsdfsdfsdfssdfsdddddddddddddddddddddddddsdfdsf', 1, '2016-03-25 21:25:56', 14, NULL, 1, NULL, 0),
	(35, 'ALTER TABLE `petitions` 	CHANGE COLUMN `resized` `resized` VARCHAR(100) NULL AFTER `image`;', 'petitions/3969a8c5-ac13-404d-ac64-0cf1ecbe3ba2.jpg', NULL, 'ALTER TABLE `petitions`\r\n	CHANGE COLUMN `resized` `resized` VARCHAR(100) NULL AFTER `image`;', 'ALTER TABLE `petitions`\r\n	CHANGE COLUMN `resized` `resized` VARCHAR(100) NULL AFTER `image`;', 2, '2016-05-02 03:40:01', 14, 'Все пучком', 5, '2016-05-02 03:56:10', 1);
/*!40000 ALTER TABLE `petitions` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.petitions_activity
CREATE TABLE IF NOT EXISTS `petitions_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datatime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activity` varchar(500) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `ip` varchar(500) DEFAULT NULL,
  `petition` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_petitions_activity_petitions` (`petition`),
  KEY `FK_petitions_activity_auth_user` (`user`),
  CONSTRAINT `FK_petitions_activity_auth_user` FOREIGN KEY (`user`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_petitions_activity_petitions` FOREIGN KEY (`petition`) REFERENCES `petitions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COMMENT='Активність по петиціям';

-- Дамп данных таблицы weunion.petitions_activity: ~50 rows (приблизительно)
/*!40000 ALTER TABLE `petitions_activity` DISABLE KEYS */;
INSERT INTO `petitions_activity` (`id`, `datatime`, `activity`, `user`, `ip`, `petition`) VALUES
	(25, '2016-01-14 20:52:17', 'Петиція створена', 14, '127.0.0.1', 26),
	(26, '2016-01-14 20:52:27', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 26),
	(27, '2016-01-14 21:00:50', 'Петиція створена', 14, '127.0.0.1', 27),
	(28, '2016-01-14 21:00:55', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 27),
	(29, '2016-01-14 21:03:04', 'Петиція створена', 14, '127.0.0.1', 28),
	(30, '2016-01-14 21:03:18', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 28),
	(31, '2016-01-14 21:04:59', 'Петиція створена', 14, '127.0.0.1', 29),
	(32, '2016-01-14 21:05:03', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 29),
	(33, '2016-01-15 13:24:50', 'Зміна статусу на "Повернута на збір підписів. Причина: "Багато фейків', 14, '127.0.0.1', 26),
	(34, '2016-01-15 13:32:41', 'Зміна статусу на "Повернута на збір підписів. Причина: "Ось так', 14, '127.0.0.1', 26),
	(35, '2016-01-15 13:33:32', 'Зміна статусу на "Повернута на збір підписів. Причина: "Ось так', 14, '127.0.0.1', 26),
	(36, '2016-01-15 13:34:30', 'Зміна статусу на "Повернута на збір підписів. Причина: "Ось так', 14, '127.0.0.1', 26),
	(37, '2016-01-16 07:05:31', 'Петиція створена', 14, '127.0.0.1', 30),
	(38, '2016-01-16 07:08:27', 'Зміна статусу на "Відхилена модератором Причина: "hjg', 14, '127.0.0.1', 30),
	(39, '2016-01-16 09:48:58', 'Автоматична зміна статусу на "На перевірці голосів"', 14, '127.0.0.1', 27),
	(40, '2016-01-16 10:07:28', 'Автоматична зміна статусу на "На перевірці голосів"', 14, '127.0.0.1', 26),
	(41, '2016-01-16 10:08:58', 'Зміна статусу на "Розглядається"', 14, '127.0.0.1', 26),
	(42, '2016-01-16 10:10:00', 'Зміна статусу на "Розглядається"', 14, '127.0.0.1', 26),
	(43, '2016-01-16 10:42:15', 'Петиція створена', 14, '127.0.0.1', 31),
	(44, '2016-01-16 12:30:41', 'Зміна статусу на "Відхилена модератором Причина: "НЕ хочу', 14, '127.0.0.1', 26),
	(45, '2016-01-16 12:30:55', 'Зміна статусу на "Модератором відхилив і приховав петицію Причина: "віа', 14, '127.0.0.1', 26),
	(46, '2016-01-16 12:58:34', 'Зміна статусу на "Модератором відхилив і приховав петицію Причина: "efd', 14, '127.0.0.1', 28),
	(47, '2016-01-19 08:06:37', 'Автоматична зміна статусу на "На перевірці голосів"', 14, '127.0.0.1', 26),
	(48, '2016-01-19 08:06:54', 'Зміна статусу на "Розглядається"', 14, '127.0.0.1', 26),
	(49, '2016-01-19 08:09:01', 'Зміна статусу на "Повернута на збір підписів. Причина: "Тестування"', 14, '127.0.0.1', 26),
	(50, '2016-01-24 13:11:09', 'Зміна статусу на "Повернута на збір підписів. Причина: df"', 14, '127.0.0.1', 27),
	(51, '2016-01-25 12:17:25', 'Зміна статусу на "Петиція розглянута Резолюція: rtyr"', 14, '127.0.0.1', 26),
	(52, '2016-01-25 12:22:21', 'Зміна статусу на "Відхилена модератором Причина: uyuy"', 14, '127.0.0.1', 30),
	(53, '2016-01-26 12:13:41', 'Заблоковано голос(-и)', 14, '127.0.0.1', 27),
	(54, '2016-01-26 12:13:41', 'Заблокований користувач 14', 14, '127.0.0.1', 27),
	(55, '2016-01-29 16:42:14', 'Автоматична зміна статусу на "На перевірці голосів"', 14, '127.0.0.1', 27),
	(56, '2016-01-30 21:22:02', 'Зміна статусу на "Модератором відхилив і приховав петицію Причина: df"', 14, '127.0.0.1', 26),
	(57, '2016-01-30 21:22:04', 'Зміна статусу на "Повернуто на домодерацію"', 14, '127.0.0.1', 26),
	(58, '2016-01-30 21:22:06', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 26),
	(59, '2016-02-02 18:21:49', 'Петиція створена', 22, '127.0.0.1', 32),
	(60, '2016-02-02 19:59:26', 'Зміна статусу на "Повернуто на домодерацію"', 14, '127.0.0.1', 26),
	(61, '2016-02-02 20:15:25', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 26),
	(62, '2016-02-02 21:36:17', 'Зміна статусу на "Розглядається"', 14, '127.0.0.1', 27),
	(63, '2016-02-02 21:40:09', 'Петиція створена', 22, '127.0.0.1', 33),
	(64, '2016-02-02 21:41:01', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 33),
	(65, '2016-02-02 21:42:05', 'Автоматична зміна статусу на "На перевірці голосів"', 14, '127.0.0.1', 33),
	(66, '2016-03-25 21:25:56', 'Петиція створена', 14, '127.0.0.1', 34),
	(67, '2016-05-02 03:40:01', 'Петиція створена', 14, '127.0.0.1', 35),
	(68, '2016-05-02 03:52:34', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 35),
	(69, '2016-05-02 03:54:01', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 35),
	(70, '2016-05-02 03:54:58', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 35),
	(71, '2016-05-02 03:56:10', 'Зміна статусу на Опублікована', 14, '127.0.0.1', 35),
	(72, '2016-05-02 03:57:05', 'Зміна статусу на "Відхилена модератором Причина: Причина"', 14, '127.0.0.1', 35),
	(73, '2016-05-02 03:57:20', 'Зміна статусу на "Модератором відхилив і приховав петицію Причина: ПОрушення"', 14, '127.0.0.1', 35),
	(74, '2016-05-02 03:57:35', 'Зміна статусу на "Петиція розглянута Резолюція: Все пучком"', 14, '127.0.0.1', 35);
/*!40000 ALTER TABLE `petitions_activity` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.petitions_statuses
CREATE TABLE IF NOT EXISTS `petitions_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Статусы петиций';

-- Дамп данных таблицы weunion.petitions_statuses: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `petitions_statuses` DISABLE KEYS */;
INSERT INTO `petitions_statuses` (`id`, `title`) VALUES
	(1, 'На модерації'),
	(2, 'Триває збір підписів'),
	(3, 'Відхилена модератором'),
	(4, 'Розглянута'),
	(5, 'Архівна'),
	(6, 'Розглядається'),
	(7, 'Прихована'),
	(8, 'На перевірці голосів');
/*!40000 ALTER TABLE `petitions_statuses` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.petitions_voices
CREATE TABLE IF NOT EXISTS `petitions_voices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `petition` int(11) DEFAULT '0',
  `user` int(11) DEFAULT '0',
  `block` int(1) DEFAULT '0' COMMENT 'Можно админу заблокировать подозрительные голоса',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`user`,`petition`),
  KEY `FK_petitions_voices_petitions` (`petition`),
  KEY `FK_petitions_voices_auth_user` (`user`),
  CONSTRAINT `FK_petitions_voices_auth_user` FOREIGN KEY (`user`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `FK_petitions_voices_petitions` FOREIGN KEY (`petition`) REFERENCES `petitions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='голоса';

-- Дамп данных таблицы weunion.petitions_voices: ~7 rows (приблизительно)
/*!40000 ALTER TABLE `petitions_voices` DISABLE KEYS */;
INSERT INTO `petitions_voices` (`id`, `petition`, `user`, `block`, `created`, `ip`) VALUES
	(12, 28, 14, NULL, '2016-01-14 21:03:21', '127.0.0.1'),
	(13, 29, 14, NULL, '2016-01-14 21:05:06', '127.0.0.1'),
	(23, 26, 14, NULL, '2016-01-19 08:06:37', '127.0.0.1'),
	(24, 27, 14, NULL, '2016-01-29 16:42:12', '127.0.0.1'),
	(25, 26, 22, NULL, '2016-02-02 19:10:26', '127.0.0.1'),
	(27, 33, 14, NULL, '2016-02-02 21:42:05', '127.0.0.1'),
	(29, 35, 14, NULL, '2016-05-02 03:59:40', '127.0.0.1');
/*!40000 ALTER TABLE `petitions_voices` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.polls_choice
CREATE TABLE IF NOT EXISTS `polls_choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `choice` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `polls_choice_poll_id_72c5b9c8_fk_polls_poll_id` (`poll_id`),
  CONSTRAINT `polls_choice_poll_id_72c5b9c8_fk_polls_poll_id` FOREIGN KEY (`poll_id`) REFERENCES `polls_poll` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.polls_choice: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `polls_choice` DISABLE KEYS */;
INSERT INTO `polls_choice` (`id`, `poll_id`, `choice`) VALUES
	(7, 3, 'Нормально'),
	(8, 3, 'Херово'),
	(9, 4, 'yep');
/*!40000 ALTER TABLE `polls_choice` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.polls_poll
CREATE TABLE IF NOT EXISTS `polls_poll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town_ref` int(11) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `archive` int(1) DEFAULT '0',
  `question` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_polls_poll_town` (`town_ref`),
  CONSTRAINT `FK_polls_poll_town` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.polls_poll: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `polls_poll` DISABLE KEYS */;
INSERT INTO `polls_poll` (`id`, `town_ref`, `date_start`, `date_end`, `active`, `archive`, `question`, `description`) VALUES
	(3, 5, '2016-02-10', '2016-02-18', 1, 0, 'Ну як ви там?', 'Простеньке опитування'),
	(4, 1, '2016-05-03', '2016-05-25', 1, 0, 'dsafsdf', 'sdfsaf');
/*!40000 ALTER TABLE `polls_poll` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.polls_vote
CREATE TABLE IF NOT EXISTS `polls_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`poll_id`),
  KEY `polls_vote_choice_id_400bbb6_fk_polls_choice_id` (`choice_id`),
  KEY `polls_vote_poll_id_4e6e812c_fk_polls_poll_id` (`poll_id`),
  CONSTRAINT `polls_vote_choice_id_400bbb6_fk_polls_choice_id` FOREIGN KEY (`choice_id`) REFERENCES `polls_choice` (`id`) ON DELETE CASCADE,
  CONSTRAINT `polls_vote_poll_id_4e6e812c_fk_polls_poll_id` FOREIGN KEY (`poll_id`) REFERENCES `polls_poll` (`id`) ON DELETE CASCADE,
  CONSTRAINT `polls_vote_user_id_5f1aef7b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.polls_vote: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `polls_vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `polls_vote` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.regions
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.regions: ~25 rows (приблизительно)
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` (`id`, `name`) VALUES
	(1, 'АР Крим'),
	(2, 'Вінницька область'),
	(3, 'Волинська область'),
	(4, 'Дніпропетровська область'),
	(5, 'Донецька область'),
	(6, 'Житомирська область'),
	(7, 'Закарпатська область'),
	(8, 'Запорізька область'),
	(9, 'Івано-Франківська область'),
	(10, 'Київська область'),
	(11, 'Кіровоградська область'),
	(12, 'Луганська область'),
	(13, 'Львівська область'),
	(14, 'Миколаївська область'),
	(15, 'Одеська область'),
	(16, 'Полтавська область'),
	(17, 'Рівненська область'),
	(18, 'Сумська область'),
	(19, 'Тернопільська область'),
	(20, 'Харківська область'),
	(21, 'Херсонська область'),
	(22, 'Хмельницька область'),
	(23, 'Черкаська область'),
	(24, 'Чернігівська область'),
	(25, 'Чернівецька область');
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.smartroads_activity
CREATE TABLE IF NOT EXISTS `smartroads_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` int(11) DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `user_ref` int(11) DEFAULT NULL,
  `type` enum('ISSUE','ACCESENT') DEFAULT NULL,
  `essence_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Активность по модулю умные дороги\r\n';

-- Дамп данных таблицы weunion.smartroads_activity: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `smartroads_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `smartroads_activity` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.smartroads_issues
CREATE TABLE IF NOT EXISTS `smartroads_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `what_to_do` text,
  `cost` varchar(15) DEFAULT NULL,
  `resolution` text,
  `done_date` datetime DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lat` varchar(25) NOT NULL,
  `lon` varchar(25) NOT NULL,
  `isblock` int(11) NOT NULL DEFAULT '0',
  `status_ref` int(11) NOT NULL DEFAULT '2',
  `town_ref` int(11) NOT NULL,
  `user_ref` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `smartroads_issues_15795c21` (`status_ref`),
  KEY `smartroads_issues_c06a8b03` (`town_ref`),
  KEY `smartroads_issues_d7cb3867` (`user_ref`),
  CONSTRAINT `smartroads_issues_town_ref_14ede56027260244_fk_town_id` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`),
  CONSTRAINT `smartroads_issues_user_ref_111fdf3698b29b13_fk_auth_user_id` FOREIGN KEY (`user_ref`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `smartroads_status_ref_1f407432efa17263_fk_smartroads_statuses_id` FOREIGN KEY (`status_ref`) REFERENCES `smartroads_statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.smartroads_issues: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `smartroads_issues` DISABLE KEYS */;
INSERT INTO `smartroads_issues` (`id`, `title`, `description`, `what_to_do`, `cost`, `resolution`, `done_date`, `created_date`, `lat`, `lon`, `isblock`, `status_ref`, `town_ref`, `user_ref`) VALUES
	(5, 'Зона 52 (вулиця Космонафтів - Набержна)', 'Здесь поменьше аварий', 'Поставить отбойники', '150 000 грн.', NULL, NULL, '2016-04-02 14:24:35', '47.1099834', '37.5480407', 0, 3, 1, 14),
	(7, 'Зона 12. Вулиця Мяслицького - Нова Айдарівська', '<p>После того, как в Киеве рухнул дом на Богдана Хмельницкого, забрав с собой две жизни строителей, Виталий Кличко поручил срочно проверить состояние всех аварийных домов. Их в мэрии насчитали больше ста. &quot;Страна&quot; решила помочь киевским чиновникам, и сама отправилась инспектировать здания.</p>\r\n\r\n<p><strong>Аварийность по-царски</strong></p>\r\n\r\n<p>- А шо, нас признали аварийным домом?</p>\r\n\r\n<p>- Та не такие уж мы и аварийные. По сравнению с другими домами &ndash; так вообще!</p>\r\n\r\n<p>- Ага, когда тебе штукатурка на голову упадет &ndash; я на тебя посмотрю!</p>\r\n\r\n<p>В филиале института Ботаники на Большой Житомирской несколько человек спорят &ndash; считать или не считать их дом аварийным.&nbsp;</p>\r\n\r\n<p>Я осматриваюсь. Фасад некогда роскшного&nbsp;здания весь в трещинах. Старые балконы разрушены. Вход во двор закрывают когда-то красивые, а ныне покосившиеся чугунные ворота. За ними &ndash; горы мусора и кирпича.</p>\r\n\r\n<p>Внутри картина особо не меняется. В предбаннике стены в разводах &ndash; следы недавнего дождя. Вдоль стены идет большая трещина. В библиотеке уже немного уютнее. Высокие потолки с лепниной, большие окна, даже камин.</p>\r\n\r\n<p><img alt="" src="/media/news/content/2016/04/21/0e50f67759f2f4dfb19e218d755ad02a_2jboIPu.jpg" style="height:576px; width:960px" /></p>\r\n', 'В принципі все', '150 000 грн.', '', NULL, '2016-04-21 00:18:18', '47.11009313662249', '37.56156921386719', 0, 2, 1, 14),
	(8, 'Зона 52 (вулиця Космонафтів - Набержна)', '<p>Здесь поменьше аварий</p>\r\n', '<p>Поставить отбойники</p>\r\n', '150 000 грн.', '', NULL, '2016-04-21 01:12:38', '47.11196240504518', '37.5732421875', 0, 2, 1, 14),
	(9, 'Зона 52 (вулиця Космонафтів - Набержна)', '<p>Здесь поменьше аварий</p>\r\n', '<p>Поставить отбойники</p>\r\n', '150 000 грн.', '', NULL, '2016-04-21 01:26:56', '47.11196240504518', '37.5732421875', 0, 3, 1, 14);
/*!40000 ALTER TABLE `smartroads_issues` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.smartroads_statuses
CREATE TABLE IF NOT EXISTS `smartroads_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.smartroads_statuses: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `smartroads_statuses` DISABLE KEYS */;
INSERT INTO `smartroads_statuses` (`id`, `title`) VALUES
	(1, 'Аварійна зона не змінена '),
	(2, 'Зона відпрацьовується'),
	(3, 'Аварійна зона змінена');
/*!40000 ALTER TABLE `smartroads_statuses` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.socialaccount_socialaccount
CREATE TABLE IF NOT EXISTS `socialaccount_socialaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(30) NOT NULL,
  `uid` varchar(191) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  `extra_data` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `socialaccount_socialaccount_provider_7e705e2318442ae_uniq` (`provider`,`uid`),
  KEY `socialaccount_socialacc_user_id_66b42b7ddd1b5a59_fk_auth_user_id` (`user_id`),
  CONSTRAINT `socialaccount_socialacc_user_id_66b42b7ddd1b5a59_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.socialaccount_socialaccount: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `socialaccount_socialaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialaccount` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.socialaccount_socialapp
CREATE TABLE IF NOT EXISTS `socialaccount_socialapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(30) NOT NULL,
  `name` varchar(40) NOT NULL,
  `client_id` varchar(191) NOT NULL,
  `secret` varchar(191) NOT NULL,
  `key` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.socialaccount_socialapp: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `socialaccount_socialapp` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialapp` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.socialaccount_socialapp_sites
CREATE TABLE IF NOT EXISTS `socialaccount_socialapp_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialapp_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `socialapp_id` (`socialapp_id`,`site_id`),
  KEY `socialaccount_sociala_site_id_52aec3816e2a1ef5_fk_django_site_id` (`site_id`),
  CONSTRAINT `soci_socialapp_id_73c6086cf27ef1ce_fk_socialaccount_socialapp_id` FOREIGN KEY (`socialapp_id`) REFERENCES `socialaccount_socialapp` (`id`),
  CONSTRAINT `socialaccount_sociala_site_id_52aec3816e2a1ef5_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.socialaccount_socialapp_sites: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `socialaccount_socialapp_sites` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialapp_sites` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.socialaccount_socialtoken
CREATE TABLE IF NOT EXISTS `socialaccount_socialtoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` longtext NOT NULL,
  `token_secret` longtext NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `socialaccount_socialtoken_app_id_7d87dd9ad900a570_uniq` (`app_id`,`account_id`),
  KEY `so_account_id_1176293ce0baa990_fk_socialaccount_socialaccount_id` (`account_id`),
  CONSTRAINT `so_account_id_1176293ce0baa990_fk_socialaccount_socialaccount_id` FOREIGN KEY (`account_id`) REFERENCES `socialaccount_socialaccount` (`id`),
  CONSTRAINT `socialaccou_app_id_4ef98d38f2281cb_fk_socialaccount_socialapp_id` FOREIGN KEY (`app_id`) REFERENCES `socialaccount_socialapp` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.socialaccount_socialtoken: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `socialaccount_socialtoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialtoken` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.subcontractors
CREATE TABLE IF NOT EXISTS `subcontractors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `town_ref` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-sumcontructors-town-ref_idx` (`town_ref`),
  CONSTRAINT `fk_subcontructors_town_ref` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.subcontractors: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `subcontractors` DISABLE KEYS */;
INSERT INTO `subcontractors` (`id`, `name`, `town_ref`) VALUES
	(1, 'All Admins', NULL),
	(2, 'All Moderators', NULL),
	(3, 'Кузнецовське міське комунальне підприємство', 1),
	(4, 'Кузнецовський відділ освіти', 1),
	(5, 'Нетішенське комунальне підприємство', 4),
	(6, 'КП "Благоустрій"', 4),
	(7, 'ГО "Місто Майбутнього"', 1),
	(8, 'Модератор міста Кузнецовськ', 1);
/*!40000 ALTER TABLE `subcontractors` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.subscriptions
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_ref` int(11) DEFAULT NULL,
  `user_ref` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `parameter` varchar(50) NOT NULL,
  `comparison` varchar(10) NOT NULL,
  `value` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_ref_user_ref_parameter_comparison_value` (`type_ref`,`user_ref`,`parameter`,`comparison`,`value`),
  KEY `FK_subscriptions_auth_user` (`user_ref`),
  KEY `parameter_comparison_value_created_date` (`parameter`,`comparison`,`value`,`created_date`),
  CONSTRAINT `FK_subscriptions_auth_user` FOREIGN KEY (`user_ref`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_subscriptions_subscriptions_types` FOREIGN KEY (`type_ref`) REFERENCES `subscriptions_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Подписка пользователей\r\n';

-- Дамп данных таблицы weunion.subscriptions: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` (`id`, `type_ref`, `user_ref`, `is_active`, `parameter`, `comparison`, `value`, `created_date`) VALUES
	(2, 1, 14, 1, 'recipient_edrpou', '==', '4554687', '0000-00-00 00:00:00'),
	(3, 1, 14, 1, 'amount', '==', '25000', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.subscriptions_types
CREATE TABLE IF NOT EXISTS `subscriptions_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `is_active` int(1) NOT NULL DEFAULT '1',
  `periodic` int(1) NOT NULL DEFAULT '1' COMMENT 'Периодичность дней',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Типы подписок';

-- Дамп данных таблицы weunion.subscriptions_types: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `subscriptions_types` DISABLE KEYS */;
INSERT INTO `subscriptions_types` (`id`, `name`, `is_active`, `periodic`) VALUES
	(1, 'edata', 1, 7);
/*!40000 ALTER TABLE `subscriptions_types` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.town
CREATE TABLE IF NOT EXISTS `town` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `slug` varchar(45) NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `votes` int(11) NOT NULL DEFAULT '100' COMMENT 'Сколько голосов нужно что бы петиция стала подписаной',
  `pet_days` int(5) NOT NULL DEFAULT '90' COMMENT 'Сколько дней петиция должна находиться для сбора голосов',
  `region_ref` int(11) NOT NULL,
  `pet_number_templ` varchar(15) NOT NULL DEFAULT '01/%s-ЕЗ' COMMENT 'Шаблон номера петиції для міста(напр. 12/<id>-ЕП)',
  `map_radius` int(128) DEFAULT NULL,
  `map_lat` varchar(128) NOT NULL,
  `map_lon` varchar(128) NOT NULL,
  `zoom` int(2) NOT NULL,
  `menu` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_town_region_idx` (`region_ref`),
  KEY `FK_town_town_types` (`type_id`),
  CONSTRAINT `FK_town_town_types` FOREIGN KEY (`type_id`) REFERENCES `town_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_town_region` FOREIGN KEY (`region_ref`) REFERENCES `regions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.town: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `town` DISABLE KEYS */;
INSERT INTO `town` (`id`, `name`, `slug`, `type_id`, `votes`, `pet_days`, `region_ref`, `pet_number_templ`, `map_radius`, `map_lat`, `map_lon`, `zoom`, `menu`) VALUES
	(1, 'Кузнецовськ', 'kuzn', 1, 100, 90, 17, '10/%s-ЕП', NULL, '37.5631469', '47.1169929', 12, '\r\n<li class="header">Інструменти для м. Кузнецовськ (тест): </li>\r\n                <li >\r\n              <a href="/kuzn">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/kuzn/news">\r\n                <i class="fa fa-newspaper-o" data-target="\\/news"></i> <span>Новини міста</span> \r\n              </a>\r\n            </li>\r\n            <li>\r\n              <a href="/kuzn/defects" >\r\n                <i class="fa fa-search"></i>\r\n                <span>Дефекти ЖКГ</span><i class="fa fa-angle-left pull-right"></i>\r\n              </a>\r\n              <ul class="treeview-menu">\r\n                <li><a href="/kuzn/defects/list"><i class="fa fa-th-list"></i> Реєстр заявок</a></li>\r\n                <li><a href="/kuzn/defects/add"><i class="fa fa-plus-square-o"></i> Додати заявку</a></li>\r\n                <li><a href="/kuzn/defects/mydefects"><i class="fa fa-user-secret"></i> Мої заявки</a></li>\r\n	<li><a href="/kuzn/defects/rules"><i class="fa  fa-info"></i> Правила</a></li>\r\n                <li><a href="/kuzn/defects/help"><i class="fa fa-question-circle"></i>Допомога</a></li>\r\n              </ul>\r\n            </li>\r\n\r\n\r\n             <li><a href="/kuzn/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/kuzn/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/kuzn/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/kuzn/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/kuzn/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/kuzn/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/kuzn/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/kuzn/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/kuzn/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="/kuzn/budget">\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/kuzn/edata">\r\n                <i class="fa fa-credit-card"></i>\r\n                <span>Відкриті фінанси</span>\r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/kuzn/polls">\r\n                <i class="fa  fa-check"></i> <span>Опитування</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/kuzn/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги "IGov"</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/kuzn/prozorro">\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі "Prozorro"</span>\r\n              </a>\r\n            </li>\r\n              <li class="treeview">\r\n              <a href="/kuzn/donor">\r\n                <i class="fa fa-heartbeat"></i> <span>Донорство крові</span>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="/kuzn/medicine">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n\r\n              </a>\r\n            </li>\r\n              <li class="treeview">\r\n              <a href="/kuzn/flats">\r\n                <i class="fa fa-users"></i>\r\n	            <span>Черги на житло</span>\r\n\r\n              </a>\r\n            </li>\r\n    <li>\r\n              <a href="/kuzn/smartroads">\r\n                <i class="fa fa-road"></i>\r\n                <span>Розумні дороги</span>\r\n\r\n              </a>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/regions">\r\n                <i class="fa fa-object-group    "></i> <span>Бюджет участі</span>\r\n                  <small class="label pull-right bg-yellow">у розробці</small>\r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/regions">\r\n                <i class="fa fa-taxi"></i>\r\n                <span>Розшук поліції</span>\r\n                <small class="label pull-right bg-grey">план</small>\r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/regions">\r\n                <i class="fa fa-tree"></i>\r\n                <span>Енергозбереження</span>\r\n                <small class="label pull-right bg-grey">план</small>\r\n              </a>\r\n            </li>\r\n         <li class="treeview">\r\n              <a href="/regions">\r\n                <i class="fa fa-legal"></i>\r\n                <span>Аукціон держ. майна</span>\r\n                <small class="label pull-right bg-grey">план</small>\r\n              </a>\r\n            </li>\r\n</ul>'),
	(4, 'Нетішин', 'netishyn', 1, 100, 90, 22, '10/00%s-ЕП', NULL, '26.6034254', '50.3269261', 14, '\r\n<li class="header">Інструменти для м. Нетішин: </li>\r\n                <li >\r\n              <a href="/town/netishyn">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/news">\r\n                <i class="fa fa-newspaper-o"></i> <span>Новини міста</span> \r\n              </a>\r\n            </li>\r\n            <li>\r\n              <a href="/defects" >\r\n                <i class="fa fa-search"></i>\r\n                <span>Дефекти ЖКГ</span><i class="fa fa-angle-left pull-right"></i>\r\n              </a>\r\n              <ul class="treeview-menu">\r\n                <li><a href="/defects"><i class="fa fa-th-list"></i> Реєстр заявок</a></li>\r\n                <li><a href="/defects/add"><i class="fa fa-plus-square-o"></i> Додати заявку</a></li>\r\n                <li><a href="/defects/mydefects"><i class="fa fa-user-secret"></i> Мої заявки</a></li>\r\n	<li><a href="/defects/mydefects"><i class="fa  fa-info"></i> Правила</a></li>\r\n                <li><a href="/defects/help"><i class="fa fa-question-circle"></i>Допомога</a></li>\r\n              </ul>\r\n            </li>\r\n\r\n\r\n             <li><a href="/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/budget" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n'),
	(5, 'Зоря', 'zorya', 3, 75, 90, 17, '10/10%s-ЕП', NULL, '26.0211181', '50.7257172', 14, '\r\n<li class="header">Інструменти для c. Зоря: </li>\r\n                <li >\r\n              <a href="/zorya/">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/zorya/news">\r\n                <i class="fa fa-newspaper-o"></i> <span>Новини міста</span> \r\n              </a>\r\n            </li>\r\n            \r\n\r\n\r\n             <li><a href="/zorya/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/zorya/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/zorya/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/zorya/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/zorya/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/zorya/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/zorya/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/zorya/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/zorya/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/zorya/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n'),
	(8, 'Здолбунів', 'zdolbuniv', 1, 100, 90, 17, '01/%s-ЕЗ', NULL, '26.2248411', '50.5095888', 14, '\r\n<li class="header">Інструменти для м. Здолбунів: </li>\r\n                <li >\r\n              <a href="/town/zdolbuniv">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/news">\r\n                <i class="fa fa-newspaper-o"></i> <span>Новини міста</span> \r\n              </a>\r\n            </li>\r\n            \r\n\r\n\r\n             <li><a href="/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n'),
	(10, 'Костопіль', 'kostopil', 1, 100, 90, 17, '01/%s-ЕЗ', NULL, '26.411046', '50.8831255', 14, '\r\n<li class="header">Інструменти для м. Костопіль: </li>\r\n                <li >\r\n              <a href="/town/kostopil">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/news">\r\n                <i class="fa fa-newspaper-o"></i> <span>Новини міста</span> \r\n              </a>\r\n            </li>\r\n            \r\n\r\n\r\n             <li><a href="/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n'),
	(11, 'Єлізаветівка', 'elizavetivka', 3, 75, 90, 4, '01/%s-ЕЗ', NULL, '34.6211288', '48.6092006', 14, '\r\n<li class="header">Інструменти для c. Єлізоветівка: </li>\r\n                <li >\r\n              <a href="/town/elizavetivka">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/news">\r\n                <i class="fa fa-newspaper-o"></i> <span>Новини села</span> \r\n              </a>\r\n            </li>\r\n            \r\n\r\n\r\n             <li><a href="/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n'),
	(13, 'Нікополь', 'nikopol', 1, 100, 90, 4, '01/%s-ЕЗ', NULL, '34.3770644', '47.5750649', 14, '\r\n<li class="header">Інструменти для м. Нікополь: </li>\r\n                <li >\r\n              <a href="/town/nikopol">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/news">\r\n                <i class="fa fa-newspaper-o" ></i> <span>Новини міста</span> \r\n              </a>\r\n            </li>\r\n            <li>\r\n              <a href="/defects" >\r\n                <i class="fa fa-search"></i>\r\n                <span>Дефекти ЖКГ</span><i class="fa fa-angle-left pull-right"></i>\r\n              </a>\r\n              <ul class="treeview-menu">\r\n                <li><a href="/defects/list"><i class="fa fa-th-list"></i> Реєстр заявок</a></li>\r\n                <li><a href="/defects/add"><i class="fa fa-plus-square-o"></i> Додати заявку</a></li>\r\n                <li><a href="/defects/mydefects"><i class="fa fa-user-secret"></i> Мої заявки</a></li>\r\n	<li><a href="/defects/rules"><i class="fa  fa-info"></i> Правила</a></li>\r\n                <li><a href="/defects/help"><i class="fa fa-question-circle"></i>Допомога</a></li>\r\n              </ul>\r\n            </li>\r\n\r\n\r\n             <li><a href="/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#s">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n'),
	(15, 'Білгород-Дністровський', 'bd', 1, 100, 90, 15, '01/%s-ЕЗ', NULL, '30.3211459', '46.1883877', 14, '\r\n<li class="header">Інструменти для м. Білгород-Дністровський: </li>\r\n                <li >\r\n              <a href="/town/bd">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/news">\r\n                <i class="fa fa-newspaper-o" ></i> <span>Новини міста</span> \r\n              </a>\r\n            </li>\r\n            <li>\r\n              <a href="/defects" >\r\n                <i class="fa fa-search"></i>\r\n                <span>Дефекти ЖКГ</span><i class="fa fa-angle-left pull-right"></i>\r\n              </a>\r\n              <ul class="treeview-menu">\r\n                <li><a href="/defects/list"><i class="fa fa-th-list"></i> Реєстр заявок</a></li>\r\n                <li><a href="/defects/add"><i class="fa fa-plus-square-o"></i> Додати заявку</a></li>\r\n                <li><a href="/defects/mydefects"><i class="fa fa-user-secret"></i> Мої заявки</a></li>\r\n	<li><a href="/defects/rules"><i class="fa  fa-info"></i> Правила</a></li>\r\n                <li><a href="/defects/help"><i class="fa fa-question-circle"></i>Допомога</a></li>\r\n              </ul>\r\n            </li>\r\n\r\n\r\n             <li><a href="/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#s">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n'),
	(17, 'Клевань', 'klevan', 2, 100, 90, 17, '01/%s-ЕЗ', NULL, '37.5599899', '47.1358506', 14, '\r\n<li class="header">Інструменти для м. Клевань: </li>\r\n                <li >\r\n              <a href="/town/klevan">\r\n                <i class="fa fa-dashboard"></i> <span>Зведена інформація</span> \r\n              </a>\r\n            </li>\r\n            <li class="treeview">\r\n              <a href="/news">\r\n                <i class="fa fa-newspaper-o" ></i> <span>Новини Клевані</span> \r\n              </a>\r\n            </li>\r\n            <li>\r\n              <a href="/defects" >\r\n                <i class="fa fa-search"></i>\r\n                <span>Дефекти ЖКГ</span><i class="fa fa-angle-left pull-right"></i>\r\n              </a>\r\n              <ul class="treeview-menu">\r\n                <li><a href="/defects/list"><i class="fa fa-th-list"></i> Реєстр заявок</a></li>\r\n                <li><a href="/defects/add"><i class="fa fa-plus-square-o"></i> Додати заявку</a></li>\r\n                <li><a href="/defects/mydefects"><i class="fa fa-user-secret"></i> Мої заявки</a></li>\r\n	<li><a href="/defects/rules"><i class="fa  fa-info"></i> Правила</a></li>\r\n                <li><a href="/defects/help"><i class="fa fa-question-circle"></i>Допомога</a></li>\r\n              </ul>\r\n            </li>\r\n\r\n\r\n             <li><a href="/petitions" ><i class="fa fa-commenting-o"></i> <span>Петиції</span><i class="fa fa-angle-left pull-right"></i></a>\r\n\r\n                <ul class="treeview-menu">\r\n                <li><a href="/petitions/add"><i class="fa fa-plus-square-o"></i> Додати петицію</a></li>\r\n                <li><a href="/petitions/status/2" ><i class="fa fa-circle-o text-light-blue"></i> Активні петиції</a></li>\r\n                <li><a href="/petitions/status/6"><i class="fa fa-circle-o text-yellow"></i> Розглядаються</a></li>\r\n                <li><a href="/petitions/status/4"><i class="fa fa-circle-o text-green"></i> Розглянуті</a></li>\r\n                <li><a href="/petitions/status/3"><i class="fa fa-circle-o text-red"></i> Відхилені</a></li>\r\n                <li><a href="/petitions/status/5"><i class="fa fa-circle-o"></i> Архівні</a></li>\r\n                <li><a href="/petitions/rules"><i class="fa fa-info"></i> Привила</a></li>\r\n                <li><a href="/petitions/help"><i class="fa fa-question-circle"></i> Допомога</a></li>\r\n\r\n              </ul>\r\n\r\n            </li>\r\n\r\n            <li >\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="/igov">\r\n                <i class="fa  fa-briefcase"></i> <span>Електронні послуги</span>\r\n              </a>\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-pie-chart"></i> <span>Відкритий бюджет</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            </li>\r\n                <li class="treeview">\r\n              <a href="#" >\r\n                <i class="fa  fa-money"></i> <span>Електронні закупівлі</span>\r\n<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n            <li class="treeview">\r\n              <a href="#s">\r\n                <i class="fa fa-edit"></i> <span>Реєстр ліків</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n\r\n              </a>\r\n              <li class="treeview">\r\n              <a href="#">\r\n                <i class="fa fa-users"></i> \r\n	<span>Черги на житло</span>\r\n	<small class="label pull-right bg-yellow">на черзі</small>\r\n              </a>\r\n            </li>\r\n\r\n          </ul>\r\n');
/*!40000 ALTER TABLE `town` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.towns_additions
CREATE TABLE IF NOT EXISTS `towns_additions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town_ref` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `body` mediumtext NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_towns_additions_town` (`town_ref`),
  CONSTRAINT `FK_towns_additions_town` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Табличка с дополнительными полями для города';

-- Дамп данных таблицы weunion.towns_additions: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `towns_additions` DISABLE KEYS */;
INSERT INTO `towns_additions` (`id`, `town_ref`, `type`, `body`, `title`, `description`) VALUES
	(1, 1, 'igov', 'asIDPlaceUA=5600000000,5610700000&bShowEmptyFolders=false', NULL, NULL),
	(2, 4, 'igov', 'asIDPlaceUA=6800000000,6810500000&bShowEmptyFolders=false', NULL, NULL),
	(3, 5, 'igov', 'asIDPlaceUA=5600000000,5624684900&bShowEmptyFolders=false', NULL, NULL),
	(4, 8, 'igov', 'asIDPlaceUA=5600000000,5622610100&bShowEmptyFolders=false', NULL, NULL),
	(5, 10, 'igov', 'asIDPlaceUA=5600000000,5623410100&bShowEmptyFolders=false', NULL, NULL),
	(6, 1, 'test', 'asIDPlaceUA=1200000000,1223780800&bShowEmptyFolders=false', 'sdklfjsdfljk', 'sdfsdfsdfsd2342354234'),
	(7, 15, 'igov', 'asIDPlaceUA=5100000000,5110300000&bShowEmptyFolders=false', NULL, NULL),
	(8, 13, 'igov', 'asIDPlaceUA=1200000000,1211600000&bShowEmptyFolders=false', NULL, NULL),
	(9, 1, 'prozorro', '{"Page":1,"PageSize":500,"OrderColumn":"tenderStartDate","OrderDirection":"desc","SearchFilter":{"UserName":"","PriceFrom":"","PriceTo":"","Statuses":["active.enquiries","active.tendering","active.auction","active.qualification","active.awarded","unsuccessful","complete","cancelled"],"ColumnFilters":[{"Name":"Title","Filter":""},{"Name":"Description","Filter":""},{"Name":"OrganizationName","Filter":""},{"Name":"tenderPeriodEndFrom"},{"Name":"tenderPeriodEndTo"},{"Name":"tenderPeriodStartFrom"},{"Name":"tenderPeriodStartTo"},{"Name":"classificationCpv"},{"Name":"classificationDkpp"}],"isProductionMode":true,"codeEDRPOUs":["05391005"]}}', NULL, NULL),
	(10, 1, 'medicines', '<iframe width="960px" height="650px" src="https://docs.google\r\n.com/spreadsheets/d/1JrDHjQePGnqQCOovv34qONvfgEKS2eGlUTM0OVK5nyI/pubhtml\r\n?widget\r\n=true&amp;headers=false"></iframe>\r\n', 'Реєстр ліків закуплений за рахунок коштів місцевого бюджету', 'Відповідальний за список - секретар поліклініки Войтюк М.С.  (тел. 65-12-12)<br/>\r\nОтримати ліки можна у Поліклінніці в кабінеті №233 з собою мати посвідчення особи та довідку від лікаря. Довідки за телефоном - 63-12-23');
/*!40000 ALTER TABLE `towns_additions` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.towns_gromadas_villages
CREATE TABLE IF NOT EXISTS `towns_gromadas_villages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '0',
  `map_lat` varchar(15) DEFAULT '0',
  `map_lon` varchar(15) DEFAULT '0',
  `type_ref` int(11) DEFAULT '0',
  `gromada_ref` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_towns_gromadas_villages_town_gromada` (`gromada_ref`),
  KEY `FK_towns_gromadas_villages_town_types` (`type_ref`),
  CONSTRAINT `FK_towns_gromadas_villages_town_gromada` FOREIGN KEY (`gromada_ref`) REFERENCES `town_gromada` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_towns_gromadas_villages_town_types` FOREIGN KEY (`type_ref`) REFERENCES `town_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Села входящие в состав громады';

-- Дамп данных таблицы weunion.towns_gromadas_villages: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `towns_gromadas_villages` DISABLE KEYS */;
INSERT INTO `towns_gromadas_villages` (`id`, `title`, `map_lat`, `map_lon`, `type_ref`, `gromada_ref`) VALUES
	(1, 'Село1', '0', '0', 3, 1),
	(3, 'Село2', '0', '0', 3, 1),
	(4, 'selo', '0', '0', 3, 1),
	(5, 'selo fsd', '0', '0', 3, 1);
/*!40000 ALTER TABLE `towns_gromadas_villages` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.town_allowed_modules
CREATE TABLE IF NOT EXISTS `town_allowed_modules` (
  `town` int(11) DEFAULT NULL,
  `module` int(11) DEFAULT NULL,
  UNIQUE KEY `uniq` (`town`,`module`),
  KEY `FK__town` (`town`),
  KEY `FK_town_allowed_modules_modules` (`module`),
  CONSTRAINT `FK__town` FOREIGN KEY (`town`) REFERENCES `town` (`id`),
  CONSTRAINT `FK_town_allowed_modules_modules` FOREIGN KEY (`module`) REFERENCES `modules` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Дозволені модулі для міста';

-- Дамп данных таблицы weunion.town_allowed_modules: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `town_allowed_modules` DISABLE KEYS */;
INSERT INTO `town_allowed_modules` (`town`, `module`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 4),
	(4, 2);
/*!40000 ALTER TABLE `town_allowed_modules` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.town_banners
CREATE TABLE IF NOT EXISTS `town_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgsource` varchar(100) DEFAULT NULL,
  `town` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_town_banners_town` (`town`),
  CONSTRAINT `FK_town_banners_town` FOREIGN KEY (`town`) REFERENCES `town` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Банер(и) мера та організацій, що модерують проект у місті';

-- Дамп данных таблицы weunion.town_banners: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `town_banners` DISABLE KEYS */;
INSERT INTO `town_banners` (`id`, `imgsource`, `town`) VALUES
	(1, 'citybanners/kuzn.jpg', 1),
	(3, 'citybanners/zorya.jpg', 5),
	(4, 'citybanners/netishin.jpg', 4),
	(5, 'citybanners/zdolbuniv.jpg', 8),
	(6, 'citybanners/bd.jpg', 15);
/*!40000 ALTER TABLE `town_banners` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.town_budgets
CREATE TABLE IF NOT EXISTS `town_budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `town_ref` int(11) DEFAULT NULL,
  `body` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_town_budgets_town` (`town_ref`),
  CONSTRAINT `FK_town_budgets_town` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Бюджеты городов по годам';

-- Дамп данных таблицы weunion.town_budgets: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `town_budgets` DISABLE KEYS */;
INSERT INTO `town_budgets` (`id`, `year`, `town_ref`, `body`) VALUES
	(1, 2015, 4, '\r\n\r\n        <!-- Main content -->\r\n        <section class="content">\r\n\r\n          <!-- Default box -->\r\n          <div class="box">\r\n\r\n            <div class="box-body">\r\n\r\n\r\n\r\n<div class="text-center"><h2>Доходи та витрати бюджету м.Нетішин за 2015 рік</h2>\r\n     <a><i class="fa fa-facebook-official" style="font-size: 24px"></i></a>\r\n   <a> <i class="fa fa-vk" style="font-size: 24px"></i></a>\r\n   <a> <i class="fa fa-odnoklassniki-square" style="font-size: 24px"></i></a>\r\n   <a> <i class="fa  fa-twitter" style="font-size: 24px"></i></a> </div>\r\n\r\n\r\n\r\n<br/>\r\n           <div class="col-md-12">\r\n\r\n                    <div class="callout callout-success">\r\n\r\n                <p><div class="text-center"><h3>Надходження бюджету</h3></div></p>\r\n              </div>\r\n                    </div>\r\n\r\n        <div class="col-md-7">\r\n   <h3> Загальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті доходу\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Отримано, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="bud-tab-main">\r\n    <th colspan="2">\r\nПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n92 274,7\r\n    </th>\r\n	<th>\r\n98 155,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">106,4%</div>\r\n    </th>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n        11000000\r\n\r\n    </th>\r\n    <th>\r\n        <span>+</span> Податки на доходи, податки на прибуток, податки на збільшення ринкової вартості\r\n\r\n    </th>\r\n    <th>\r\n        73 307,6\r\n    </th>\r\n	<th>\r\n        78 284,7\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">106,8%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n       Податок та збір на доходи фізичних осіб\r\n    </td>\r\n    <td>\r\n       73 161,1\r\n    </td>\r\n	<td>\r\n        78 300,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">107,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n        Податок на прибуток підприємств\r\n    </td>\r\n    <td>\r\n       146,5\r\n    </td>\r\n	<td>\r\n        -15,4\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">-10,5%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n        13000000\r\n    </th>\r\n    <th>\r\n        Рентна плата та плата за використання інших природних ресурсів\r\n\r\n    </th>\r\n    <th>\r\n        30,5\r\n    </th>\r\n	<th>\r\n        30,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">100,1%</div>\r\n    </th>\r\n</tr>\r\n\r\n<tr class="togg-budget">\r\n    <th>\r\n14000000\r\n    </th>\r\n    <th>\r\n    Акцизний податок (внутрішні податки на товари та послуги)  \r\n    </th>\r\n    <th>\r\n3 750,0\r\n    </th>\r\n	<th>\r\n4 216,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">112,4%</div>\r\n    </th>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n18000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Місцеві податки\r\n    </th>\r\n    <th>\r\n15 128,4\r\n    </th>\r\n	<th>\r\n15 566,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,9%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПодаток на майно\r\n    </td>\r\n    <td>\r\n9 941,4\r\n    </td>\r\n	<td>\r\n10 228,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">102,9%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТуристичний збір \r\n    </td>\r\n    <td>\r\n3,0\r\n    </td>\r\n	<td>\r\n3,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">112,3%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗбір за провадження деяких видів підприємницької діяльності, що справлявся до 1 січня 2015 року\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n-40,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-red">0,0%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЄдиний податок  \r\n    </td>\r\n    <td>\r\n5 184,0\r\n    </td>\r\n	<td>\r\n5 374,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">103,7%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n19000000\r\n    </th>\r\n    <th>\r\n        Екологічний податок \r\n    </th>\r\n    <th>\r\n58,2\r\n    </th>\r\n	<th>\r\n57,3\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">98,5%</div>\r\n    </th>\r\n</tr>\r\n  <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nНЕПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n1 009,3\r\n    </th>\r\n	<th>\r\n1 032,0\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,2%</div>\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n 21000000\r\n   </th>\r\n    <th>\r\n        <span>+</span> Доходи від  власності та підприємницької діяльності\r\n\r\n    </th>\r\n    <th>\r\n114,9\r\n    </th>\r\n	<th>\r\n116,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">101,5%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЧастина чистого прибутку (доходу) державних або комунальних унітарних підприємств та їх об\'єднань, що вилучається до відповідного бюджету, та дивіденди (дохід), нараховані на акції (частки, паї) господарських товариств, у статутних капіталах яких є державна або комунальна власність\r\n    </td>\r\n    <td>\r\n78,4\r\n    </td>\r\n	<td>\r\n78,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші надходження\r\n    </td>\r\n    <td>\r\n36,5\r\n    </td>\r\n	<td>\r\n38,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">104,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n22000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Адміністративні збори та платежі, доходи від некомерційної господарської діяльності \r\n\r\n    </th>\r\n    <th>\r\n765,1\r\n    </th>\r\n	<th>\r\n782,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,2%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПлата за надання адміністративних послуг\r\n\r\n    </td>\r\n    <td>\r\n193,9\r\n    </td>\r\n	<td>\r\n196,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">101,2%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадходження від орендної плати за користування цілісним майновим комплексом та іншим державним майном  \r\n    </td>\r\n    <td>\r\n466,7\r\n    </td>\r\n	<td>\r\n467,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДержавне мито\r\n    </td>\r\n    <td>\r\n104,5\r\n    </td>\r\n	<td>\r\n118,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">113,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n24000000\r\n    </th>\r\n    <th>\r\n     Інші неподаткові надходження  \r\n    </th>\r\n    <th>\r\n129,3\r\n    </th>\r\n	<th>\r\n133,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">103,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n<tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nОФІЦІЙНІ ТРАНСФЕРТИ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n121 042,9\r\n    </th>\r\n	<th>\r\n120 959,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">99,9%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nДОХОДИ ВІД ОПЕРАЦІЙ З КАПІТАЛОМ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n4,8\r\n    </th>\r\n	<th>\r\n5,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">114,6%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget">\r\n    <th colspan="2">\r\nВСЬОГО:\r\n\r\n    </th>\r\n\r\n    <th>\r\n214 447,1\r\n    </th>\r\n	<th>\r\n220 260,3\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,7%</div>\r\n    </th>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Загальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-red"></i> Податкові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 6,4%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Неподаткові надходження <span class="pull-right\r\n                text-green"><i class="fa\r\n                fa-angle-up"></i> 2,2%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Офіційны трансфери\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 0,1%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 14,6%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n            <div class="box-footer no-padding">\r\n\r\n            </div>\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n\r\n         <div class="col-md-7">\r\n   <h3> Cпеціальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті доходу\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Отримано, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="bud-tab-main">\r\n    <th colspan="2">\r\nПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n0\r\n    </th>\r\n	<th>\r\n-7,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-red">0,0%</div>\r\n    </th>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n        12000000\r\n\r\n    </th>\r\n    <th>\r\n        Податки на власність\r\n    </th>\r\n    <th>\r\n        0\r\n    </th>\r\n	<th>\r\n        1,7\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">0,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n<tr class="togg-budget">\r\n    <th>\r\n18000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Місцеві податки і збори\r\n    </th>\r\n    <th>\r\n0,0\r\n    </th>\r\n	<th>\r\n-9,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-red">0,0%</div>\r\n    </th>\r\n</tr>\r\n  <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nНЕПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n4 148,9\r\n    </th>\r\n	<th>\r\n6 111,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">147,3%</div>\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n 24000000\r\n   </th>\r\n    <th>\r\n        <span>+</span> Інші неподаткові надходження\r\n    </th>\r\n    <th>\r\n85,2\r\n    </th>\r\n	<th>\r\n179,1\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">210,2%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадходження коштів пайової участі у розвитку інфраструктури населеного пункту\r\n    </td>\r\n    <td>\r\n80,0\r\n    </td>\r\n	<td>\r\n159,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">199,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші надходження\r\n    </td>\r\n    <td>\r\n5,2\r\n    </td>\r\n	<td>\r\n19,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">368,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n25000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Власні надходження бюджетних установ\r\n    </th>\r\n    <th>\r\n4 063,7\r\n    </th>\r\n	<th>\r\n5 932,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">146,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадходження від плати за послуги, що надаються бюджетними установами згідно із законодавством \r\n\r\n    </td>\r\n    <td>\r\n4 063,7\r\n    </td>\r\n	<td>\r\n4 200,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">103,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші джерела власних надходжень бюджетних установ  \r\n  \r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n1 731,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n<tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nДОХОДИ ВІД ОПЕРАЦІЙ З КАПІТАЛОМ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n0\r\n    </th>\r\n	<th>\r\n12,4\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">0,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nЦІЛЬОВІ ФОНДИ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n45,0\r\n    </th>\r\n	<th>\r\n26,0\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">57,9%</div>\r\n    </th>\r\n</tr>\r\n     <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nІНШІ СУБВЕНЦІЇ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n1 662,5\r\n    </th>\r\n	<th>\r\n1 638,0\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">98,5%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget">\r\n    <th colspan="2">\r\nВСЬОГО:\r\n\r\n    </th>\r\n\r\n    <th>\r\n5 856,4\r\n    </th>\r\n	<th>\r\n7 780,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">132,9%</div>\r\n    </th>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Спеціальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart1" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-grey"></i> Податкові надходження\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                 <li><i class="fa fa-circle-o text-red"></i> Неподаткові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 47,3%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Інші субвенції <span class="pull-right\r\n                text-red"><i class="fa\r\n                fa-angle-down"></i> 1,5%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Цільові фонди\r\n                  <span class="pull-right text-yellow"><i class="fa fa-angle-down"></i> 42,1%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n\r\n                <div class="col-md-12">\r\n\r\n                    <div class="callout callout-danger">\r\n\r\n                <p><div class="text-center"><h3>Видатки бюджету</h3></div></p>\r\n              </div>\r\n                    </div>\r\n\r\n\r\n       <div class="col-md-7">\r\n   <h3> Загальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті витрати\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Витрачено, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВИКОНАВЧИЙ КОМІТЕТ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n63 984,0\r\n    </th>\r\n	<th>\r\n58 048,7\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">90,7%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n      Органи місцевого самоврядування\r\n    </td>\r\n    <td>\r\n      7 798,2\r\n    </td>\r\n	<td>\r\n    7 793,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n        Спеціалізовані лікарні та інші спеціалізовані заклади (центри, диспансери, госпіталі для інвалідів ВВВ, лепрозорії, медико - санітарні частини тощо, що мають ліжкову мережу)\r\n    </td>\r\n    <td>\r\n       46 739,1\r\n    </td>\r\n	<td>\r\n     41 143,5\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">88,0%</span>\r\n    </td>\r\n</tr>\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n        Інші видатки на соціальний захист населення\r\n    </td>\r\n    <td>\r\n        315,4\r\n    </td>\r\n	<td>\r\n        273,9\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">86,8%</div>\r\n    </td>\r\n</tr>\r\n\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n    Інші програми соціального захисту дітей\r\n \r\n    </td>\r\n    <td>\r\n17,5\r\n    </td>\r\n	<td>\r\n17,4\r\n    </td>\r\n    <th>\r\n        <div class="badge bg-green">99,9%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n     Соціальні програми і заходи державних органів у справах молоді\r\n    </td>\r\n    <td>\r\n16,6\r\n    </td>\r\n	<td>\r\n16,6\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">100,0%</div>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи з оздоровлення та відпочинку дітей, крім заходів з оздоровлення дітей, що здійснюються за рахунок коштів на оздоровлення громадян, які постраждали внаслідок Чорнобильської катастрофи\r\n\r\n    </td>\r\n    <td>\r\n73,1\r\n    </td>\r\n	<td>\r\n73,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЖитлово - експлуатаційне господарство\r\n\r\n    </td>\r\n    <td>\r\n184,1\r\n    </td>\r\n	<td>\r\n182,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,0%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nБлагоустрій міст, сіл, селищ\r\n\r\n    </td>\r\n    <td>\r\n5 064,8\r\n    </td>\r\n	<td>\r\n5 006,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-red">98,9%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВідшкодування різниці між розміром ціни (тарифу) на житлово - комунальні послуги, що затверджувалися або погоджувалися рішенням місцевого органу виконавчої влади та органу місцевого самоврядування, та розміру економічно обгрунтованих витрат на їх виробництво\r\n  \r\n    </td>\r\n    <td>\r\n400,0\r\n    </td>\r\n	<td>\r\n400,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПеріодичні видання (газети та журнали)\r\n\r\n    </td>\r\n    <td>\r\n180,0\r\n    </td>\r\n	<td>\r\n180,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПроведення навчально - тренувальних зборів і змагань\r\n\r\n    </td>\r\n    <td>\r\n277,0\r\n    </td>\r\n	<td>\r\n276,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nРозробка схем та проектних рішень масового застосування\r\n    </td>\r\n    <td>\r\n16,2\r\n    </td>\r\n	<td>\r\n16,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗемлеустрій\r\n    </td>\r\n    <td>\r\n220,1\r\n    </td>\r\n	<td>\r\n196,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">89,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші заходи у сфері автомобільного транспорту\r\n\r\n    </td>\r\n    <td>\r\n110,0\r\n    </td>\r\n	<td>\r\n16,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">15,2%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на проведення робіт, пов"язаних із будівництвом, реконструкцією, ремонтом та утриманням автомобільних доріг\r\n\r\n    </td>\r\n    <td>\r\n1 157,3\r\n    </td>\r\n	<td>\r\n1 157,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші природоохоронні заходи\r\n\r\n    </td>\r\n    <td>\r\n58,2\r\n    </td>\r\n	<td>\r\n44,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">75,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи у сфері захисту населенняі територій від надзвичайниї ситуацій техногенного та природного характеру\r\n\r\n    </td>\r\n    <td>\r\n161,5\r\n    </td>\r\n	<td>\r\n111,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">69,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПроведення виборів депутатів місцевих рад та сільських, селищних, міських голів\r\n\r\n    </td>\r\n    <td>\r\n422,5\r\n    </td>\r\n	<td>\r\n410,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">97,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n417,9\r\n    </td>\r\n	<td>\r\n417,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки\r\n\r\n    </td>\r\n    <td>\r\n354,4\r\n    </td>\r\n	<td>\r\n314,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">88,9%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ФІНАНСОВЕ УПРАВЛІННЯ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n16 861,1\r\n    </th>\r\n	<th>\r\n16 681,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">98,9%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n    </td>\r\n    <td>\r\n1 609,4\r\n    </td>\r\n	<td>\r\n1 609,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nРезервний фонд\r\n\r\n    </td>\r\n    <td>\r\n16,0\r\n    </td>\r\n	<td>\r\n0,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nРеверсна дотація\r\n\r\n    </td>\r\n    <td>\r\n15 056,4\r\n    </td>\r\n	<td>\r\n15 056,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n179,2\r\n    </td>\r\n	<td>\r\n16,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">8,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВІДДІЛ ОСВІТИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n59 899,6\r\n    </th>\r\n	<th>\r\n59 676,3\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,6%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n460,5\r\n    </td>\r\n	<td>\r\n460,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДошкільні заклади освіти\r\n\r\n    </td>\r\n    <td>\r\n24 795,7\r\n    </td>\r\n	<td>\r\n24 792,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗагальноосвітні школи ( в т.ч. школа-дитячий садок, інтернат при школі, спеціалізовані школи, ліцеї, гімназії, колегіуми)\r\n\r\n    </td>\r\n    <td>\r\n29 780,4\r\n    </td>\r\n	<td>\r\n29 564,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПозашкільні заклади освіти, заходи із позашкільної роботи з дітьми\r\n    </td>\r\n    <td>\r\n2 622,0\r\n    </td>\r\n	<td>\r\n2 622,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМетодична робота, інші заходи у сфері народної освіти\r\n\r\n    </td>\r\n    <td>\r\n748,4\r\n    </td>\r\n	<td>\r\n748,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦентралізовані бухгалтерії обласних, міських, районних відділів освіти\r\n    </td>\r\n    <td>\r\n457,6\r\n    </td>\r\n	<td>\r\n457,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nГрупи централізованого господарського обслуговування\r\n    </td>\r\n    <td>\r\n632,3\r\n    </td>\r\n	<td>\r\n632,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога дітям сиротам та дітям позбавлених батьківського піклування, яким виповнюється 18 років\r\n\r\n    </td>\r\n    <td>\r\n7,2\r\n    </td>\r\n	<td>\r\n7,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи з оздоровлення та відпочинку дітей, крім заходів з оздоровлення дітей, що здійснюються за рахунок коштів на оздоровлення громадян, які постраждали внаслідок Чорнобильської катастрофи\r\n\r\n    </td>\r\n    <td>\r\n313,9\r\n    </td>\r\n	<td>\r\n309,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">98,7%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n81,5\r\n    </td>\r\n	<td>\r\n81,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> УПРАВЛІННЯ ПРАЦІ ТА СОЦІАЛЬНОГО ЗАХИСТУ НАСЕЛЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n47 376,3\r\n    </th>\r\n	<th>\r\n47 299,9\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,8%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДитячі будинки (в т.ч. сімейного типу, прийомні сім"ї).\r\n\r\n    </td>\r\n    <td>\r\n231,4\r\n    </td>\r\n	<td>\r\n230,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги ветеранам війни, особам, на яких поширюється чинність Закону України "Про статус ветеранів війни, гарантії їх соціального захисту, особам які мають особливі заслуги перед Батьківщиною, вдовам (вдівцям) та батькам померлих (загиблих) осіб, які мають особливі заслуги перед Батьківщиною, жертвам нацистських переслідувань та реабілітованим громадянам , які стали інвалідами внаслідок репресій або є пенсіонерами на житлово - комунальні послуги\r\n\r\n    </td>\r\n    <td>\r\n732,4\r\n    </td>\r\n	<td>\r\n732,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги ветеранам війни, особам, на яких поширюється чинність Закону України "Про статус ветеранів війни, гарантії їх соціального захисту, особам які мають особливі заслуги перед Батьківщиною, вдовам (вдівцям) та батькам померлих (загиблих) осіб які мають особливі заслуги перед Батьківщиною, вдовам (вдівцям)  та батькам померлих (загиблих) осіб, які мають особливі заслуги перед Батьківщиною на придбання твердого палива та скрапленого газу\r\n\r\n    </td>\r\n    <td>\r\n20,9\r\n    </td>\r\n	<td>\r\n20,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші пільги ветеранам війни, ветеранам праці,  особам, на яких поширюється чинність Закону України "Про статус ветеранів війни, гарантії їх соціального захисту, особам які мають особливі  заслуги перед Батьківщиною, вдовам (вдівцям) та батькам померлих (загиблих) осіб, які мають особливі заслуги перед Батьківщиною, ветеранам праці, громадянам, які стали інвалідами внаслідок репресій або є пенсіонерами\r\n\r\n    </td>\r\n    <td>\r\n27,2\r\n    </td>\r\n	<td>\r\n17,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">65,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги ветеранам військової служби та ветеранам органів внутрішніх справ, ветеранам податкової міліції, ветеранам державної пожедної охорони, ветеранам Державної кримінально - виконавчої служби, ветеранам служби цивільного захисту, ветеранам Державної служби\r\n\r\n    </td>\r\n    <td>\r\n134,9\r\n    </td>\r\n	<td>\r\n134,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги громадян, які постраждали внаслідок Чорнобильської катастрофи, дружинам (чоловікам) та опікунам (на час опікунства) дітей померлих громадян, смерть яких пов"язана х Чорнобильської катастрофою на житлово-комунальні послуги\r\n\r\n    </td>\r\n    <td>\r\n981,1\r\n    </td>\r\n	<td>\r\n981,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги громадян, які постраждали внаслідок Чорнобильської катастрофи, дружинам (чоловікам) та опікунам (на час опікунства) дітей померлих громадян, смерть яких пов"язана х Чорнобильської катастрофою на придбання твердого палива\r\n\r\n    </td>\r\n    <td>\r\n1,9\r\n    </td>\r\n	<td>\r\n1,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші пільги громадян, які постраждали внаслідок Чорнобильської катастрофи, дружинам (чоловікам) та опікунам (на час опікунства) дітей померлих громадян, смерть яких пов"язана з Чорнобильською катастрофою\r\n\r\n    </td>\r\n    <td>\r\n14,0\r\n    </td>\r\n	<td>\r\n6,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">45,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги пенсіонерам з числа спеціалістів із захисту рослин, передбачені частиною чертвертою статті 20 Закону України "Про захист рослин" громадянам , передбачені пунктом "ї"частини першої статті 77 Основ законодавства про охорону здоров"я\r\n\r\n    </td>\r\n    <td>\r\n1,2\r\n    </td>\r\n	<td>\r\n1,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги на медичне обслуговування громадян, які постраждали внаслідок Чорнобильської катастрофи\r\n\r\n    </td>\r\n    <td>\r\n87,3\r\n    </td>\r\n	<td>\r\n87,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги окремим категоріям громадян з послуг зв"язку\r\n\r\n    </td>\r\n    <td>\r\n155,2\r\n    </td>\r\n	<td>\r\n149,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">96,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги багатодітним сім\'ям, дитячим будинкам сімейного типу та прийомним сім"ям , в яких не менше року проживають відповідно троє або більше дітей, а також сім"ям (крім багатодітних сімей), в в яких не менше року проживають троє і більше дітей, враховуючи тих, над якими встановлено опіку чи піклування на житлово-комунальні послуги\r\n\r\n    </td>\r\n    <td>\r\n362,6\r\n    </td>\r\n	<td>\r\n362,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги багатодітним сім\'ям, дитячим будинкам сімейного типу та прийомним сім"ям , в яких не менше року проживають відповідно троє або більше дітей, а також сім"я  (крім багатодітних сімей) , в яких не менше року проживають троє і більше дітей, враховуючи тих, над якими встановлено опіку чи піклування  на придбання твердого палива та скрапленого газу\r\n\r\n    </td>\r\n    <td>\r\n4,5\r\n    </td>\r\n	<td>\r\n4,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога в з"язку з вагітністю і пологами\r\n\r\n    </td>\r\n    <td>\r\n338,9\r\n    </td>\r\n	<td>\r\n338,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на догляд за дитиною віком до 3-х років\r\n\r\n    </td>\r\n    <td>\r\n298,3\r\n    </td>\r\n	<td>\r\n298,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога при народженні дитини\r\n\r\n    </td>\r\n    <td>\r\n20 609,6\r\n    </td>\r\n	<td>\r\n20 609,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на дітей, над якими встановлено опіку чи піклування\r\n\r\n    </td>\r\n    <td>\r\n1 416,7\r\n    </td>\r\n	<td>\r\n1 416,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на дітей одиноким матерям\r\n\r\n    </td>\r\n    <td>\r\n2 235,6\r\n    </td>\r\n	<td>\r\n2 235,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТимчасова державна допомога дітям\r\n\r\n    </td>\r\n    <td>\r\n801,7\r\n    </td>\r\n	<td>\r\n801,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога при усиновленні дитини\r\n\r\n    </td>\r\n    <td>\r\n71,4\r\n    </td>\r\n	<td>\r\n71,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДержавна соціальна допомога малозабезпеченим сім"ям\r\n\r\n    </td>\r\n    <td>\r\n4 897,9\r\n    </td>\r\n	<td>\r\n4 897,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nСубсидії населенню для відшкодування витрат на оплату житлово-комунальних послуг\r\n\r\n    </td>\r\n    <td>\r\n1 858,4\r\n    </td>\r\n	<td>\r\n1 858,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nСубвенція населенню на відшкодування витрат на придбання твердого та рідкого палива та скрапленого газу\r\n\r\n    </td>\r\n    <td>\r\n59,3\r\n    </td>\r\n	<td>\r\n56,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">95,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсація населенню додаткових витрат на оплату послуг газопостачання, центрального опалення та центрального постачання гарячої води\r\n\r\n    </td>\r\n    <td>\r\n0,02\r\n    </td>\r\n	<td>\r\n0,02\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки на соціальний захист населення\r\n\r\n    </td>\r\n    <td>\r\n523,4\r\n    </td>\r\n	<td>\r\n508,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">97,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на догляд за інвалідами І чи ІІ групи внаслідок психічного розгляду\r\n\r\n    </td>\r\n    <td>\r\n1 146,7\r\n    </td>\r\n	<td>\r\n1 146,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсація особам, які згідно із статтями 43 та 48 Гірничого закону України мають право на безоплатне отримання вугілля на побутові потреби, але проживають у будинках, що мають центральне опалення\r\n\r\n    </td>\r\n    <td>\r\n0,2\r\n    </td>\r\n	<td>\r\n0,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВитрати на поховання учасників бойових дій та інвалідів війни\r\n\r\n    </td>\r\n    <td>\r\n9,9\r\n    </td>\r\n	<td>\r\n3,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">33,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТериторіальні центри соціального обслуговування (надання соціальних послуг)\r\n\r\n    </td>\r\n    <td>\r\n1 001,6\r\n    </td>\r\n	<td>\r\n996,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВиплати грошової компенсації фізичним особам, які надають соціальні послуги гроиадянам похилого віку, інвалідам, дітям - інвалідам, хворим, які не здатні до самообслуговування і потребують сторонньої допомоги\r\n\r\n    </td>\r\n    <td>\r\n103,1\r\n    </td>\r\n	<td>\r\n102,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦентри рсоціальної реабілітації дітей інвалідів, центри професійної реабілітації інвалідів\r\n\r\n    </td>\r\n    <td>\r\n1 076,9\r\n    </td>\r\n	<td>\r\n1 076,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги що надаються населенню (крім  ВВв та праці, органів внутрішніх справ та громадянам , які постраждали внаслідок Чорнобильської катастрофи) на оплату житлово-комунальних послуг та природного газу\r\n\r\n    </td>\r\n    <td>\r\n136,6\r\n    </td>\r\n	<td>\r\n136,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nФінансова підтримка громадських організацій інвалідів і ветеранів\r\n\r\n    </td>\r\n    <td>\r\n21,0\r\n    </td>\r\n	<td>\r\n21,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДержавна соціальна допомога інвалідам з дитинства та дітям-інвалідам\r\n\r\n    </td>\r\n    <td>\r\n4 850,5\r\n    </td>\r\n	<td>\r\n4 850,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсаційні виплати інвалідам на бензин, ремонт, тех. обслуговування автотранспорту та транспортне обслуговування\r\n\r\n    </td>\r\n    <td>\r\n18,1\r\n    </td>\r\n	<td>\r\n16,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">93,2%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсаційні виплати на пільговий проїзд автомобільним транспортом окремим категоріям населення\r\n\r\n    </td>\r\n    <td>\r\n152,0\r\n    </td>\r\n	<td>\r\n131,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">86,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n13,2\r\n    </td>\r\n	<td>\r\n13,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n2 979,9\r\n    </td>\r\n	<td>\r\n2 979,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n  <th colspan="2">\r\n        <span>+</span> ЦЕНТР СОЦІАЛЬНИХ СЛУЖБ ДЛЯ МОЛОДІ:\r\n    </th>\r\n    <th>\r\n816,9\r\n    </th>\r\n	<th>\r\n815,9\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,9%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nУтримання центрів соціальних служб сім"ї дітей та молоді\r\n\r\n    </td>\r\n    <td>\r\n814,7\r\n    </td>\r\n	<td>\r\n813,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи з оздоровлення та відпочинку дітей\r\n\r\n    </td>\r\n    <td>\r\n1,5\r\n    </td>\r\n	<td>\r\n1,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n0,6\r\n    </td>\r\n	<td>\r\n0,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n <th colspan="2">\r\n        <span>+</span> ФОНД КОМУНАЛЬНОГО МАЙНА:\r\n    </th>\r\n    <th>\r\n182,0\r\n    </th>\r\n	<th>\r\n181,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,7%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n181,9\r\n    </td>\r\n	<td>\r\n181,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,7%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n0,08\r\n    </td>\r\n	<td>\r\n0,08\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Загальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart2" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-red"></i> Податкові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 6,4%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Неподаткові надходження <span class="pull-right\r\n                text-green"><i class="fa\r\n                fa-angle-up"></i> 2,2%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Офіційны трансфери\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 0,1%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 14,6%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n            <div class="box-footer no-padding">\r\n\r\n            </div>\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n\r\n\r\n         <div class="col-md-7">\r\n   <h3> Cпеціальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті витрати\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Витрачено, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВИКОНАВЧИЙ КОМІТЕТ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n16 136,5\r\n    </th>\r\n	<th>\r\n15 126,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">93,7%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n      Органи місцевого самоврядування\r\n    </td>\r\n    <td>\r\n     969,9\r\n    </td>\r\n	<td>\r\n    1 028,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">106,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n       Дошкільні заклади освіти\r\n    </td>\r\n    <td>\r\n      99,0\r\n    </td>\r\n	<td>\r\n     0,0\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">0,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n        Спеціалізовані лікарні та інші спеціалізовані заклади (центри, диспансери, госпіталі для інвалідів ВВВ, лепрозорії, медико - санітарні частини тощо, що мають ліжкову мережу)\r\n    </td>\r\n    <td>\r\n       1 705,5\r\n    </td>\r\n	<td>\r\n    1 593,4\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">93,4%</span>\r\n    </td>\r\n</tr>\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n      Житлово - експлуатаційне господарство\r\n\r\n    </td>\r\n    <td>\r\n        64,7\r\n    </td>\r\n	<td>\r\n        64,7\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">100,0%</div>\r\n    </td>\r\n</tr>\r\n\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n    Капітальний ремонт житлового фонду місцевих органів влади\r\n\r\n    </td>\r\n    <td>\r\n5 228,9\r\n    </td>\r\n	<td>\r\n4 569,7\r\n    </td>\r\n    <th>\r\n        <div class="badge bg-green">87,4%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n    Благоустрій міст, сіл, селищ\r\n    </td>\r\n    <td>\r\n2 198,9\r\n    </td>\r\n	<td>\r\n2 194,2\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">99,8%</div>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомбінати комунальних підприємств , районні виробничі об"єднання та інші підприємства, установи та організації житлово - комунального господарства\r\n    </td>\r\n    <td>\r\n60,0\r\n    </td>\r\n	<td>\r\n60,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПроведення навчально-тренувальних зборів і змагань\r\n\r\n\r\n    </td>\r\n    <td>\r\n231,0\r\n    </td>\r\n	<td>\r\n230,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,8%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКапітальні вкладення\r\n    </td>\r\n    <td>\r\n2 221,8\r\n    </td>\r\n	<td>\r\n2 102,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-red">94,6%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на проведення робіт, пов"язаних із будівництвом, реконструкцію, ремонтом та утриманням автомобільних доріг\r\n  \r\n    </td>\r\n    <td>\r\n1 460,2\r\n    </td>\r\n	<td>\r\n1 424,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">97,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОхорона та раціональне використання природних ресурсів\r\n    </td>\r\n    <td>\r\n59,3\r\n    </td>\r\n	<td>\r\n57,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">96,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦільові фонди, утворені Верховною радою Автономної Республіки Крим, органами місцевого самоврядування і місцевими органами виконавчої влади\r\n    </td>\r\n    <td>\r\n45,0\r\n    </td>\r\n	<td>\r\n0,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n    </td>\r\n    <td>\r\n1 342,5\r\n    </td>\r\n	<td>\r\n1 321,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">98,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки\r\n\r\n    </td>\r\n    <td>\r\n449,6\r\n    </td>\r\n	<td>\r\n480,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">106,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадання пільгового довгострокового кредиту громадянам на будівництво (реконструкцію)та придбання житла\r\n\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n0,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n        <span>+</span> ВІДДІЛ ОСВІТИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n    </th>\r\n    <th>\r\n7 904,4\r\n    </th>\r\n	<th>\r\n 9 089,4\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">115,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДошкільні заклади освіти\r\n\r\n    </td>\r\n    <td>\r\n4 194,5\r\n    </td>\r\n	<td>\r\n4 801,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">114,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗагальноосвітні школи ( в т.ч. школа-дитячий садок, інтернат при школі) спеціалізовані школи, ліцеї, гімназії, колегіуми\r\n\r\n    </td>\r\n    <td>\r\n3 302,7\r\n    </td>\r\n	<td>\r\n3 740,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">113,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПозашкільні заклади освіти, заходи із позашкільної роботи з дітьми\r\n\r\n    </td>\r\n    <td>\r\n25,9\r\n    </td>\r\n	<td>\r\n75,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">290,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМетодична робота, інші заходи у сфері народної освіти\r\n\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n77,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nГрупи централізованого господарського обслуговування\r\n\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n13,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n381,2\r\n    </td>\r\n	<td>\r\n381,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ФІНАНСОВЕ УПРАВЛІННЯ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n31,4\r\n    </th>\r\n	<th>\r\n30,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">96,1%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n    </td>\r\n    <td>\r\n31,4\r\n    </td>\r\n	<td>\r\n30,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">96,1%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВІДДІЛ ОСВІТИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n7 904,0\r\n    </th>\r\n	<th>\r\n9 089,4\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">115,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДошкільні заклади освіти\r\n\r\n    </td>\r\n    <td>\r\n4 194,5\r\n    </td>\r\n	<td>\r\n4 801,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">114,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗагальноосвітні школи ( в т.ч. школа-дитячий садок, інтернат при школі, спеціалізовані школи, ліцеї, гімназії, колегіуми)\r\n\r\n    </td>\r\n    <td>\r\n3 302,7\r\n    </td>\r\n	<td>\r\n3 740,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">113,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПозашкільні заклади освіти, заходи із позашкільної роботи з дітьми\r\n    </td>\r\n    <td>\r\n25,9\r\n    </td>\r\n	<td>\r\n75,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">260,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМетодична робота, інші заходи у сфері народної освіти\r\n\r\n    </td>\r\n    <td>\r\n0\r\n    </td>\r\n	<td>\r\n77,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nГрупи централізованого господарського обслуговування\r\n    </td>\r\n    <td>\r\n0\r\n    </td>\r\n	<td>\r\n13,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n381,2\r\n    </td>\r\n	<td>\r\n381,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВІДДІЛ КУЛЬТУРИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n2 140,6\r\n    </th>\r\n	<th>\r\n2 274,1\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">90,7%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nБібліотеки\r\n\r\n    </td>\r\n    <td>\r\n53,1\r\n    </td>\r\n	<td>\r\n76,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">143,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМузеї і виставки\r\n\r\n    </td>\r\n    <td>\r\n34,0\r\n    </td>\r\n	<td>\r\n49,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">146,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПалаци і будинки культури, клуби та інші заклади клубного типу\r\n\r\n    </td>\r\n    <td>\r\n1 361,1\r\n    </td>\r\n	<td>\r\n1 361,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nШколи естетичного виховання дітей\r\n\r\n    </td>\r\n    <td>\r\n503,9\r\n    </td>\r\n	<td>\r\n586,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">116,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші культурно - освітні заклади та заходи\r\n\r\n    </td>\r\n    <td>\r\n16,5\r\n    </td>\r\n	<td>\r\n29,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">176,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n171,9\r\n    </td>\r\n	<td>\r\n171,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> УПРАВЛІННЯ ПРАЦІ ТА СОЦІАЛЬНОГО ЗАХИСТУ НАСЕЛЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n602,9\r\n    </th>\r\n	<th>\r\n955,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">158,5%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n67,6\r\n    </td>\r\n	<td>\r\n74,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">110,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки на соціальний захист населення\r\n\r\n    </td>\r\n    <td>\r\n50,1\r\n    </td>\r\n	<td>\r\n44,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">87,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТериторіальні центри соціального обслуговування (надання соціальних послуг)\r\n\r\n    </td>\r\n    <td>\r\n29,3\r\n    </td>\r\n	<td>\r\n341,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">1164,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦентри соціальної реабілітації дітей-інвалідів, центри професійної реабілітації інвалідів\r\n\r\n    </td>\r\n    <td>\r\n438,1\r\n    </td>\r\n	<td>\r\n477,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">109,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n17,9\r\n    </td>\r\n	<td>\r\n17,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n        <span>+</span> ЦЕНТР СОЦІАЛЬНИХ СЛУЖБ ДЛЯ МОЛОДІ:\r\n    </th>\r\n    <th>\r\n0\r\n    </th>\r\n	<th>\r\n6,9\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">0,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nУтримання центрів соціальних служб сім"ї дітей та молоді\r\n\r\n    </td>\r\n    <td>\r\n0\r\n    </td>\r\n	<td>\r\n6,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr class="togg-budget">\r\n\r\n    <th colspan="2">\r\n        <span>+</span> ФОНД КОМУНАЛЬНОГО МАЙНА:\r\n    </th>\r\n    <th>\r\n35,9\r\n    </th>\r\n	<th>\r\n43,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">121,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n35,9\r\n    </td>\r\n	<td>\r\n43,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">121,0%</span>\r\n    </td>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Спеціальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart3" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-grey"></i> Податкові надходження\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                 <li><i class="fa fa-circle-o text-red"></i> Неподаткові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 47,3%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Інші субвенції <span class="pull-right\r\n                text-red"><i class="fa\r\n                fa-angle-down"></i> 1,5%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Цільові фонди\r\n                  <span class="pull-right text-yellow"><i class="fa fa-angle-down"></i> 42,1%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n\r\n\r\n    <script>\r\n\r\n  //-------------\r\n  //- PIE CHART -\r\n  //-------------\r\n  // Get context with jQuery - using jQuery\'s .get() method.\r\n  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");\r\n  var pieChart = new Chart(pieChartCanvas);\r\n  var PieData = [\r\n    {\r\n      value: 35,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Податкові надходження: 98155,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Неподаткові надходження: 1032,0 тис.грн."\r\n    },\r\n    {\r\n      value: 45,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Офіційний трансферт: 120959,8 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Доходи від операцій з капіталом: 5,5 тис.грн."\r\n    },\r\n\r\n  ];\r\n  var pieChartCanvas = $("#pieChart1").get(0).getContext("2d");\r\n  var pieChart1 = new Chart(pieChartCanvas);\r\n  var PieData1 = [\r\n    {\r\n      value: 70,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Неподаткові надходження: 6111,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Інші субвенції: 1638,0 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Доходи від операцій з капіталом: 12,4 тис.грн."\r\n    },\r\n    {\r\n      value: 10,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Цільові фонди: 26,0 тис.грн."\r\n    },\r\n\r\n  ];\r\n  //-------------\r\n  //- PIE CHART -\r\n  //-------------\r\n  // Get context with jQuery - using jQuery\'s .get() method.\r\n  var pieChartCanvas = $("#pieChart2").get(0).getContext("2d");\r\n  var pieChart2 = new Chart(pieChartCanvas);\r\n  var PieData2 = [\r\n    {\r\n      value: 35,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Податкові надходження: 98155,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Неподаткові надходження: 1032,0 тис.грн."\r\n    },\r\n    {\r\n      value: 45,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Офіційний трансферт: 120959,8 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Доходи від операцій з капіталом: 5,5 тис.грн."\r\n    },\r\n\r\n  ];\r\n    //-------------\r\n  //- PIE CHART -\r\n  //-------------\r\n  // Get context with jQuery - using jQuery\'s .get() method.\r\n  var pieChartCanvas = $("#pieChart3").get(0).getContext("2d");\r\n  var pieChart3 = new Chart(pieChartCanvas);\r\n  var PieData3 = [\r\n    {\r\n      value: 35,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Податкові надходження: 98155,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Неподаткові надходження: 1032,0 тис.грн."\r\n    },\r\n    {\r\n      value: 45,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Офіційний трансферт: 120959,8 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Доходи від операцій з капіталом: 5,5 тис.грн."\r\n    },\r\n\r\n  ];\r\n  var pieOptions = {\r\n    //Boolean - Whether we should show a stroke on each segment\r\n    segmentShowStroke: true,\r\n    //String - The colour of each segment stroke\r\n    segmentStrokeColor: "#fff",\r\n    //Number - The width of each segment stroke\r\n    segmentStrokeWidth: 1,\r\n    //Number - The percentage of the chart that we cut out of the middle\r\n    percentageInnerCutout: 50, // This is 0 for Pie charts\r\n    //Number - Amount of animation steps\r\n    animationSteps: 100,\r\n    //String - Animation easing effect\r\n    animationEasing: "easeOutBounce",\r\n    //Boolean - Whether we animate the rotation of the Doughnut\r\n    animateRotate: true,\r\n    //Boolean - Whether we animate scaling the Doughnut from the centre\r\n    animateScale: false,\r\n    //Boolean - whether to make the chart responsive to window resizing\r\n    responsive: true,\r\n    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container\r\n    maintainAspectRatio: false,\r\n    //String - A legend template\r\n    legendTemplate: "dfs",\r\n    //String - A tooltip template\r\n    tooltipTemplate: "<%=label%>"\r\n  };\r\n  //Create pie or douhnut chart\r\n  // You can switch between pie and douhnut using the method below.\r\n\r\n   pieChart.Doughnut(PieData, pieOptions);\r\n   pieChart1.Doughnut(PieData1, pieOptions);\r\n   pieChart2.Doughnut(PieData2, pieOptions);\r\n   pieChart3.Doughnut(PieData3, pieOptions)\r\n    </script>\r\n\r\n  </div><!-- /.box-body -->\r\n\r\n          </div><!-- /.box -->\r\n\r\n        </section><!-- /.content -->\r\n\r\n\r\n{% endblock %}\r\n\r\n{% block footerjs %}\r\n\r\n\r\n    <script type="application/javascript">\r\n $(\'.togg-budget\').nextUntil(\'tr.togg-budget\').toggle();\r\n\r\n$(\'.togg-budget\').click(function(){\r\n   $(this).find(\'span\').text(function(_, value){return value==\'-\'?\'+\':\'-\'});\r\n    $(this).nextUntil(\'tr.togg-budget\').slideToggle(100, function(){\r\n    });\r\n});\r\n</script>'),
	(2, 2015, 1, '       \r\n        <!-- Main content -->\r\n        <section class="content">\r\n\r\n          <!-- Default box -->\r\n          <div class="box">\r\n\r\n            <div class="box-body">\r\n\r\n\r\n\r\n<div class="text-center"><h2>Доходи та витрати бюджету м.Кузнецовськ за 2015 рік</h2>\r\n     <a><i class="fa fa-facebook-official" style="font-size: 24px"></i></a>\r\n   <a> <i class="fa fa-vk" style="font-size: 24px"></i></a>\r\n   <a> <i class="fa fa-odnoklassniki-square" style="font-size: 24px"></i></a>\r\n   <a> <i class="fa  fa-twitter" style="font-size: 24px"></i></a> </div>\r\n\r\n\r\n\r\n<br/>\r\n           <div class="col-md-12">\r\n\r\n                    <div class="callout callout-success">\r\n\r\n                <p><div class="text-center"><h3>Надходження бюджету</h3></div></p>\r\n              </div>\r\n                    </div>\r\n\r\n        <div class="col-md-7">\r\n   <h3> Загальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті доходу\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Отримано, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="bud-tab-main">\r\n    <th colspan="2">\r\nПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n92 274,7\r\n    </th>\r\n	<th>\r\n98 155,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">106,4%</div>\r\n    </th>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n        11000000\r\n\r\n    </th>\r\n    <th>\r\n        <span>+</span> Податки на доходи, податки на прибуток, податки на збільшення ринкової вартості\r\n\r\n    </th>\r\n    <th>\r\n        73 307,6\r\n    </th>\r\n	<th>\r\n        78 284,7\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">106,8%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n       Податок та збір на доходи фізичних осіб\r\n    </td>\r\n    <td>\r\n       73 161,1\r\n    </td>\r\n	<td>\r\n        78 300,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">107,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n        Податок на прибуток підприємств\r\n    </td>\r\n    <td>\r\n       146,5\r\n    </td>\r\n	<td>\r\n        -15,4\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">-10,5%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n        13000000\r\n    </th>\r\n    <th>\r\n        Рентна плата та плата за використання інших природних ресурсів\r\n\r\n    </th>\r\n    <th>\r\n        30,5\r\n    </th>\r\n	<th>\r\n        30,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">100,1%</div>\r\n    </th>\r\n</tr>\r\n\r\n<tr class="togg-budget">\r\n    <th>\r\n14000000\r\n    </th>\r\n    <th>\r\n    Акцизний податок (внутрішні податки на товари та послуги)  \r\n    </th>\r\n    <th>\r\n3 750,0\r\n    </th>\r\n	<th>\r\n4 216,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">112,4%</div>\r\n    </th>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n18000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Місцеві податки\r\n    </th>\r\n    <th>\r\n15 128,4\r\n    </th>\r\n	<th>\r\n15 566,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,9%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПодаток на майно\r\n    </td>\r\n    <td>\r\n9 941,4\r\n    </td>\r\n	<td>\r\n10 228,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">102,9%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТуристичний збір \r\n    </td>\r\n    <td>\r\n3,0\r\n    </td>\r\n	<td>\r\n3,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">112,3%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗбір за провадження деяких видів підприємницької діяльності, що справлявся до 1 січня 2015 року\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n-40,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-red">0,0%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЄдиний податок  \r\n    </td>\r\n    <td>\r\n5 184,0\r\n    </td>\r\n	<td>\r\n5 374,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">103,7%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n19000000\r\n    </th>\r\n    <th>\r\n        Екологічний податок \r\n    </th>\r\n    <th>\r\n58,2\r\n    </th>\r\n	<th>\r\n57,3\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">98,5%</div>\r\n    </th>\r\n</tr>\r\n  <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nНЕПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n1 009,3\r\n    </th>\r\n	<th>\r\n1 032,0\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,2%</div>\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n 21000000\r\n   </th>\r\n    <th>\r\n        <span>+</span> Доходи від  власності та підприємницької діяльності\r\n\r\n    </th>\r\n    <th>\r\n114,9\r\n    </th>\r\n	<th>\r\n116,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">101,5%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЧастина чистого прибутку (доходу) державних або комунальних унітарних підприємств та їх об\'єднань, що вилучається до відповідного бюджету, та дивіденди (дохід), нараховані на акції (частки, паї) господарських товариств, у статутних капіталах яких є державна або комунальна власність\r\n    </td>\r\n    <td>\r\n78,4\r\n    </td>\r\n	<td>\r\n78,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші надходження\r\n    </td>\r\n    <td>\r\n36,5\r\n    </td>\r\n	<td>\r\n38,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">104,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n22000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Адміністративні збори та платежі, доходи від некомерційної господарської діяльності \r\n\r\n    </th>\r\n    <th>\r\n765,1\r\n    </th>\r\n	<th>\r\n782,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,2%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПлата за надання адміністративних послуг\r\n\r\n    </td>\r\n    <td>\r\n193,9\r\n    </td>\r\n	<td>\r\n196,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">101,2%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадходження від орендної плати за користування цілісним майновим комплексом та іншим державним майном  \r\n    </td>\r\n    <td>\r\n466,7\r\n    </td>\r\n	<td>\r\n467,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДержавне мито\r\n    </td>\r\n    <td>\r\n104,5\r\n    </td>\r\n	<td>\r\n118,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">113,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n24000000\r\n    </th>\r\n    <th>\r\n     Інші неподаткові надходження  \r\n    </th>\r\n    <th>\r\n129,3\r\n    </th>\r\n	<th>\r\n133,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">103,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n<tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nОФІЦІЙНІ ТРАНСФЕРТИ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n121 042,9\r\n    </th>\r\n	<th>\r\n120 959,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">99,9%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nДОХОДИ ВІД ОПЕРАЦІЙ З КАПІТАЛОМ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n4,8\r\n    </th>\r\n	<th>\r\n5,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">114,6%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget">\r\n    <th colspan="2">\r\nВСЬОГО:\r\n\r\n    </th>\r\n\r\n    <th>\r\n214 447,1\r\n    </th>\r\n	<th>\r\n220 260,3\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">102,7%</div>\r\n    </th>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Загальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-red"></i> Податкові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 6,4%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Неподаткові надходження <span class="pull-right\r\n                text-green"><i class="fa\r\n                fa-angle-up"></i> 2,2%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Офіційны трансфери\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 0,1%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 14,6%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n            <div class="box-footer no-padding">\r\n\r\n            </div>\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n\r\n         <div class="col-md-7">\r\n   <h3> Cпеціальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті доходу\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Отримано, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="bud-tab-main">\r\n    <th colspan="2">\r\nПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n0\r\n    </th>\r\n	<th>\r\n-7,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-red">0,0%</div>\r\n    </th>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th>\r\n        12000000\r\n\r\n    </th>\r\n    <th>\r\n        Податки на власність\r\n    </th>\r\n    <th>\r\n        0\r\n    </th>\r\n	<th>\r\n        1,7\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">0,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n<tr class="togg-budget">\r\n    <th>\r\n18000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Місцеві податки і збори\r\n    </th>\r\n    <th>\r\n0,0\r\n    </th>\r\n	<th>\r\n-9,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-red">0,0%</div>\r\n    </th>\r\n</tr>\r\n  <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nНЕПОДАТКОВІ НАДХОДЖЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n4 148,9\r\n    </th>\r\n	<th>\r\n6 111,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">147,3%</div>\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n 24000000\r\n   </th>\r\n    <th>\r\n        <span>+</span> Інші неподаткові надходження\r\n    </th>\r\n    <th>\r\n85,2\r\n    </th>\r\n	<th>\r\n179,1\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">210,2%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадходження коштів пайової участі у розвитку інфраструктури населеного пункту\r\n    </td>\r\n    <td>\r\n80,0\r\n    </td>\r\n	<td>\r\n159,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">199,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші надходження\r\n    </td>\r\n    <td>\r\n5,2\r\n    </td>\r\n	<td>\r\n19,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">368,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th>\r\n25000000\r\n    </th>\r\n    <th>\r\n        <span>+</span> Власні надходження бюджетних установ\r\n    </th>\r\n    <th>\r\n4 063,7\r\n    </th>\r\n	<th>\r\n5 932,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">146,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадходження від плати за послуги, що надаються бюджетними установами згідно із законодавством \r\n\r\n    </td>\r\n    <td>\r\n4 063,7\r\n    </td>\r\n	<td>\r\n4 200,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">103,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші джерела власних надходжень бюджетних установ  \r\n  \r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n1 731,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n<tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nДОХОДИ ВІД ОПЕРАЦІЙ З КАПІТАЛОМ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n0\r\n    </th>\r\n	<th>\r\n12,4\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">0,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nЦІЛЬОВІ ФОНДИ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n45,0\r\n    </th>\r\n	<th>\r\n26,0\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">57,9%</div>\r\n    </th>\r\n</tr>\r\n     <tr class="togg-budget" bgcolor="#f4f4f4">\r\n    <th colspan="2">\r\nІНШІ СУБВЕНЦІЇ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n1 662,5\r\n    </th>\r\n	<th>\r\n1 638,0\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-yellow">98,5%</div>\r\n    </th>\r\n</tr>\r\n\r\n <tr class="togg-budget">\r\n    <th colspan="2">\r\nВСЬОГО:\r\n\r\n    </th>\r\n\r\n    <th>\r\n5 856,4\r\n    </th>\r\n	<th>\r\n7 780,6\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">132,9%</div>\r\n    </th>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Спеціальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart1" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-grey"></i> Податкові надходження\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                 <li><i class="fa fa-circle-o text-red"></i> Неподаткові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 47,3%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Інші субвенції <span class="pull-right\r\n                text-red"><i class="fa\r\n                fa-angle-down"></i> 1,5%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Цільові фонди\r\n                  <span class="pull-right text-yellow"><i class="fa fa-angle-down"></i> 42,1%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n\r\n                <div class="col-md-12">\r\n\r\n                    <div class="callout callout-danger">\r\n\r\n                <p><div class="text-center"><h3>Видатки бюджету</h3></div></p>\r\n              </div>\r\n                    </div>\r\n\r\n\r\n       <div class="col-md-7">\r\n   <h3> Загальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті витрати\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Витрачено, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВИКОНАВЧИЙ КОМІТЕТ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n63 984,0\r\n    </th>\r\n	<th>\r\n58 048,7\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">90,7%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n      Органи місцевого самоврядування\r\n    </td>\r\n    <td>\r\n      7 798,2\r\n    </td>\r\n	<td>\r\n    7 793,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n        Спеціалізовані лікарні та інші спеціалізовані заклади (центри, диспансери, госпіталі для інвалідів ВВВ, лепрозорії, медико - санітарні частини тощо, що мають ліжкову мережу)\r\n    </td>\r\n    <td>\r\n       46 739,1\r\n    </td>\r\n	<td>\r\n     41 143,5\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">88,0%</span>\r\n    </td>\r\n</tr>\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n        Інші видатки на соціальний захист населення\r\n    </td>\r\n    <td>\r\n        315,4\r\n    </td>\r\n	<td>\r\n        273,9\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">86,8%</div>\r\n    </td>\r\n</tr>\r\n\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n    Інші програми соціального захисту дітей\r\n \r\n    </td>\r\n    <td>\r\n17,5\r\n    </td>\r\n	<td>\r\n17,4\r\n    </td>\r\n    <th>\r\n        <div class="badge bg-green">99,9%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n     Соціальні програми і заходи державних органів у справах молоді\r\n    </td>\r\n    <td>\r\n16,6\r\n    </td>\r\n	<td>\r\n16,6\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">100,0%</div>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи з оздоровлення та відпочинку дітей, крім заходів з оздоровлення дітей, що здійснюються за рахунок коштів на оздоровлення громадян, які постраждали внаслідок Чорнобильської катастрофи\r\n\r\n    </td>\r\n    <td>\r\n73,1\r\n    </td>\r\n	<td>\r\n73,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЖитлово - експлуатаційне господарство\r\n\r\n    </td>\r\n    <td>\r\n184,1\r\n    </td>\r\n	<td>\r\n182,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,0%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nБлагоустрій міст, сіл, селищ\r\n\r\n    </td>\r\n    <td>\r\n5 064,8\r\n    </td>\r\n	<td>\r\n5 006,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-red">98,9%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВідшкодування різниці між розміром ціни (тарифу) на житлово - комунальні послуги, що затверджувалися або погоджувалися рішенням місцевого органу виконавчої влади та органу місцевого самоврядування, та розміру економічно обгрунтованих витрат на їх виробництво\r\n  \r\n    </td>\r\n    <td>\r\n400,0\r\n    </td>\r\n	<td>\r\n400,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПеріодичні видання (газети та журнали)\r\n\r\n    </td>\r\n    <td>\r\n180,0\r\n    </td>\r\n	<td>\r\n180,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПроведення навчально - тренувальних зборів і змагань\r\n\r\n    </td>\r\n    <td>\r\n277,0\r\n    </td>\r\n	<td>\r\n276,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nРозробка схем та проектних рішень масового застосування\r\n    </td>\r\n    <td>\r\n16,2\r\n    </td>\r\n	<td>\r\n16,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗемлеустрій\r\n    </td>\r\n    <td>\r\n220,1\r\n    </td>\r\n	<td>\r\n196,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">89,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші заходи у сфері автомобільного транспорту\r\n\r\n    </td>\r\n    <td>\r\n110,0\r\n    </td>\r\n	<td>\r\n16,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">15,2%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на проведення робіт, пов"язаних із будівництвом, реконструкцією, ремонтом та утриманням автомобільних доріг\r\n\r\n    </td>\r\n    <td>\r\n1 157,3\r\n    </td>\r\n	<td>\r\n1 157,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші природоохоронні заходи\r\n\r\n    </td>\r\n    <td>\r\n58,2\r\n    </td>\r\n	<td>\r\n44,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">75,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи у сфері захисту населенняі територій від надзвичайниї ситуацій техногенного та природного характеру\r\n\r\n    </td>\r\n    <td>\r\n161,5\r\n    </td>\r\n	<td>\r\n111,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">69,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПроведення виборів депутатів місцевих рад та сільських, селищних, міських голів\r\n\r\n    </td>\r\n    <td>\r\n422,5\r\n    </td>\r\n	<td>\r\n410,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">97,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n417,9\r\n    </td>\r\n	<td>\r\n417,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки\r\n\r\n    </td>\r\n    <td>\r\n354,4\r\n    </td>\r\n	<td>\r\n314,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">88,9%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ФІНАНСОВЕ УПРАВЛІННЯ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n16 861,1\r\n    </th>\r\n	<th>\r\n16 681,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">98,9%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n    </td>\r\n    <td>\r\n1 609,4\r\n    </td>\r\n	<td>\r\n1 609,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nРезервний фонд\r\n\r\n    </td>\r\n    <td>\r\n16,0\r\n    </td>\r\n	<td>\r\n0,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nРеверсна дотація\r\n\r\n    </td>\r\n    <td>\r\n15 056,4\r\n    </td>\r\n	<td>\r\n15 056,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n179,2\r\n    </td>\r\n	<td>\r\n16,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">8,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВІДДІЛ ОСВІТИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n59 899,6\r\n    </th>\r\n	<th>\r\n59 676,3\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,6%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n460,5\r\n    </td>\r\n	<td>\r\n460,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДошкільні заклади освіти\r\n\r\n    </td>\r\n    <td>\r\n24 795,7\r\n    </td>\r\n	<td>\r\n24 792,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗагальноосвітні школи ( в т.ч. школа-дитячий садок, інтернат при школі, спеціалізовані школи, ліцеї, гімназії, колегіуми)\r\n\r\n    </td>\r\n    <td>\r\n29 780,4\r\n    </td>\r\n	<td>\r\n29 564,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПозашкільні заклади освіти, заходи із позашкільної роботи з дітьми\r\n    </td>\r\n    <td>\r\n2 622,0\r\n    </td>\r\n	<td>\r\n2 622,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМетодична робота, інші заходи у сфері народної освіти\r\n\r\n    </td>\r\n    <td>\r\n748,4\r\n    </td>\r\n	<td>\r\n748,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦентралізовані бухгалтерії обласних, міських, районних відділів освіти\r\n    </td>\r\n    <td>\r\n457,6\r\n    </td>\r\n	<td>\r\n457,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nГрупи централізованого господарського обслуговування\r\n    </td>\r\n    <td>\r\n632,3\r\n    </td>\r\n	<td>\r\n632,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога дітям сиротам та дітям позбавлених батьківського піклування, яким виповнюється 18 років\r\n\r\n    </td>\r\n    <td>\r\n7,2\r\n    </td>\r\n	<td>\r\n7,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи з оздоровлення та відпочинку дітей, крім заходів з оздоровлення дітей, що здійснюються за рахунок коштів на оздоровлення громадян, які постраждали внаслідок Чорнобильської катастрофи\r\n\r\n    </td>\r\n    <td>\r\n313,9\r\n    </td>\r\n	<td>\r\n309,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">98,7%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n81,5\r\n    </td>\r\n	<td>\r\n81,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> УПРАВЛІННЯ ПРАЦІ ТА СОЦІАЛЬНОГО ЗАХИСТУ НАСЕЛЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n47 376,3\r\n    </th>\r\n	<th>\r\n47 299,9\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,8%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДитячі будинки (в т.ч. сімейного типу, прийомні сім"ї).\r\n\r\n    </td>\r\n    <td>\r\n231,4\r\n    </td>\r\n	<td>\r\n230,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги ветеранам війни, особам, на яких поширюється чинність Закону України "Про статус ветеранів війни, гарантії їх соціального захисту, особам які мають особливі заслуги перед Батьківщиною, вдовам (вдівцям) та батькам померлих (загиблих) осіб, які мають особливі заслуги перед Батьківщиною, жертвам нацистських переслідувань та реабілітованим громадянам , які стали інвалідами внаслідок репресій або є пенсіонерами на житлово - комунальні послуги\r\n\r\n    </td>\r\n    <td>\r\n732,4\r\n    </td>\r\n	<td>\r\n732,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги ветеранам війни, особам, на яких поширюється чинність Закону України "Про статус ветеранів війни, гарантії їх соціального захисту, особам які мають особливі заслуги перед Батьківщиною, вдовам (вдівцям) та батькам померлих (загиблих) осіб які мають особливі заслуги перед Батьківщиною, вдовам (вдівцям)  та батькам померлих (загиблих) осіб, які мають особливі заслуги перед Батьківщиною на придбання твердого палива та скрапленого газу\r\n\r\n    </td>\r\n    <td>\r\n20,9\r\n    </td>\r\n	<td>\r\n20,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші пільги ветеранам війни, ветеранам праці,  особам, на яких поширюється чинність Закону України "Про статус ветеранів війни, гарантії їх соціального захисту, особам які мають особливі  заслуги перед Батьківщиною, вдовам (вдівцям) та батькам померлих (загиблих) осіб, які мають особливі заслуги перед Батьківщиною, ветеранам праці, громадянам, які стали інвалідами внаслідок репресій або є пенсіонерами\r\n\r\n    </td>\r\n    <td>\r\n27,2\r\n    </td>\r\n	<td>\r\n17,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">65,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги ветеранам військової служби та ветеранам органів внутрішніх справ, ветеранам податкової міліції, ветеранам державної пожедної охорони, ветеранам Державної кримінально - виконавчої служби, ветеранам служби цивільного захисту, ветеранам Державної служби\r\n\r\n    </td>\r\n    <td>\r\n134,9\r\n    </td>\r\n	<td>\r\n134,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги громадян, які постраждали внаслідок Чорнобильської катастрофи, дружинам (чоловікам) та опікунам (на час опікунства) дітей померлих громадян, смерть яких пов"язана х Чорнобильської катастрофою на житлово-комунальні послуги\r\n\r\n    </td>\r\n    <td>\r\n981,1\r\n    </td>\r\n	<td>\r\n981,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги громадян, які постраждали внаслідок Чорнобильської катастрофи, дружинам (чоловікам) та опікунам (на час опікунства) дітей померлих громадян, смерть яких пов"язана х Чорнобильської катастрофою на придбання твердого палива\r\n\r\n    </td>\r\n    <td>\r\n1,9\r\n    </td>\r\n	<td>\r\n1,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші пільги громадян, які постраждали внаслідок Чорнобильської катастрофи, дружинам (чоловікам) та опікунам (на час опікунства) дітей померлих громадян, смерть яких пов"язана з Чорнобильською катастрофою\r\n\r\n    </td>\r\n    <td>\r\n14,0\r\n    </td>\r\n	<td>\r\n6,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">45,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги пенсіонерам з числа спеціалістів із захисту рослин, передбачені частиною чертвертою статті 20 Закону України "Про захист рослин" громадянам , передбачені пунктом "ї"частини першої статті 77 Основ законодавства про охорону здоров"я\r\n\r\n    </td>\r\n    <td>\r\n1,2\r\n    </td>\r\n	<td>\r\n1,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги на медичне обслуговування громадян, які постраждали внаслідок Чорнобильської катастрофи\r\n\r\n    </td>\r\n    <td>\r\n87,3\r\n    </td>\r\n	<td>\r\n87,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги окремим категоріям громадян з послуг зв"язку\r\n\r\n    </td>\r\n    <td>\r\n155,2\r\n    </td>\r\n	<td>\r\n149,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">96,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги багатодітним сім\'ям, дитячим будинкам сімейного типу та прийомним сім"ям , в яких не менше року проживають відповідно троє або більше дітей, а також сім"ям (крім багатодітних сімей), в в яких не менше року проживають троє і більше дітей, враховуючи тих, над якими встановлено опіку чи піклування на житлово-комунальні послуги\r\n\r\n    </td>\r\n    <td>\r\n362,6\r\n    </td>\r\n	<td>\r\n362,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги багатодітним сім\'ям, дитячим будинкам сімейного типу та прийомним сім"ям , в яких не менше року проживають відповідно троє або більше дітей, а також сім"я  (крім багатодітних сімей) , в яких не менше року проживають троє і більше дітей, враховуючи тих, над якими встановлено опіку чи піклування  на придбання твердого палива та скрапленого газу\r\n\r\n    </td>\r\n    <td>\r\n4,5\r\n    </td>\r\n	<td>\r\n4,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога в з"язку з вагітністю і пологами\r\n\r\n    </td>\r\n    <td>\r\n338,9\r\n    </td>\r\n	<td>\r\n338,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на догляд за дитиною віком до 3-х років\r\n\r\n    </td>\r\n    <td>\r\n298,3\r\n    </td>\r\n	<td>\r\n298,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога при народженні дитини\r\n\r\n    </td>\r\n    <td>\r\n20 609,6\r\n    </td>\r\n	<td>\r\n20 609,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на дітей, над якими встановлено опіку чи піклування\r\n\r\n    </td>\r\n    <td>\r\n1 416,7\r\n    </td>\r\n	<td>\r\n1 416,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на дітей одиноким матерям\r\n\r\n    </td>\r\n    <td>\r\n2 235,6\r\n    </td>\r\n	<td>\r\n2 235,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТимчасова державна допомога дітям\r\n\r\n    </td>\r\n    <td>\r\n801,7\r\n    </td>\r\n	<td>\r\n801,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога при усиновленні дитини\r\n\r\n    </td>\r\n    <td>\r\n71,4\r\n    </td>\r\n	<td>\r\n71,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДержавна соціальна допомога малозабезпеченим сім"ям\r\n\r\n    </td>\r\n    <td>\r\n4 897,9\r\n    </td>\r\n	<td>\r\n4 897,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nСубсидії населенню для відшкодування витрат на оплату житлово-комунальних послуг\r\n\r\n    </td>\r\n    <td>\r\n1 858,4\r\n    </td>\r\n	<td>\r\n1 858,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nСубвенція населенню на відшкодування витрат на придбання твердого та рідкого палива та скрапленого газу\r\n\r\n    </td>\r\n    <td>\r\n59,3\r\n    </td>\r\n	<td>\r\n56,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">95,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсація населенню додаткових витрат на оплату послуг газопостачання, центрального опалення та центрального постачання гарячої води\r\n\r\n    </td>\r\n    <td>\r\n0,02\r\n    </td>\r\n	<td>\r\n0,02\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки на соціальний захист населення\r\n\r\n    </td>\r\n    <td>\r\n523,4\r\n    </td>\r\n	<td>\r\n508,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">97,1%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДопомога на догляд за інвалідами І чи ІІ групи внаслідок психічного розгляду\r\n\r\n    </td>\r\n    <td>\r\n1 146,7\r\n    </td>\r\n	<td>\r\n1 146,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсація особам, які згідно із статтями 43 та 48 Гірничого закону України мають право на безоплатне отримання вугілля на побутові потреби, але проживають у будинках, що мають центральне опалення\r\n\r\n    </td>\r\n    <td>\r\n0,2\r\n    </td>\r\n	<td>\r\n0,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВитрати на поховання учасників бойових дій та інвалідів війни\r\n\r\n    </td>\r\n    <td>\r\n9,9\r\n    </td>\r\n	<td>\r\n3,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">33,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТериторіальні центри соціального обслуговування (надання соціальних послуг)\r\n\r\n    </td>\r\n    <td>\r\n1 001,6\r\n    </td>\r\n	<td>\r\n996,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВиплати грошової компенсації фізичним особам, які надають соціальні послуги гроиадянам похилого віку, інвалідам, дітям - інвалідам, хворим, які не здатні до самообслуговування і потребують сторонньої допомоги\r\n\r\n    </td>\r\n    <td>\r\n103,1\r\n    </td>\r\n	<td>\r\n102,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦентри рсоціальної реабілітації дітей інвалідів, центри професійної реабілітації інвалідів\r\n\r\n    </td>\r\n    <td>\r\n1 076,9\r\n    </td>\r\n	<td>\r\n1 076,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПільги що надаються населенню (крім  ВВв та праці, органів внутрішніх справ та громадянам , які постраждали внаслідок Чорнобильської катастрофи) на оплату житлово-комунальних послуг та природного газу\r\n\r\n    </td>\r\n    <td>\r\n136,6\r\n    </td>\r\n	<td>\r\n136,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nФінансова підтримка громадських організацій інвалідів і ветеранів\r\n\r\n    </td>\r\n    <td>\r\n21,0\r\n    </td>\r\n	<td>\r\n21,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДержавна соціальна допомога інвалідам з дитинства та дітям-інвалідам\r\n\r\n    </td>\r\n    <td>\r\n4 850,5\r\n    </td>\r\n	<td>\r\n4 850,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсаційні виплати інвалідам на бензин, ремонт, тех. обслуговування автотранспорту та транспортне обслуговування\r\n\r\n    </td>\r\n    <td>\r\n18,1\r\n    </td>\r\n	<td>\r\n16,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">93,2%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомпенсаційні виплати на пільговий проїзд автомобільним транспортом окремим категоріям населення\r\n\r\n    </td>\r\n    <td>\r\n152,0\r\n    </td>\r\n	<td>\r\n131,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">86,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n13,2\r\n    </td>\r\n	<td>\r\n13,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n2 979,9\r\n    </td>\r\n	<td>\r\n2 979,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n  <th colspan="2">\r\n        <span>+</span> ЦЕНТР СОЦІАЛЬНИХ СЛУЖБ ДЛЯ МОЛОДІ:\r\n    </th>\r\n    <th>\r\n816,9\r\n    </th>\r\n	<th>\r\n815,9\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,9%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nУтримання центрів соціальних служб сім"ї дітей та молоді\r\n\r\n    </td>\r\n    <td>\r\n814,7\r\n    </td>\r\n	<td>\r\n813,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗаходи з оздоровлення та відпочинку дітей\r\n\r\n    </td>\r\n    <td>\r\n1,5\r\n    </td>\r\n	<td>\r\n1,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n0,6\r\n    </td>\r\n	<td>\r\n0,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n <th colspan="2">\r\n        <span>+</span> ФОНД КОМУНАЛЬНОГО МАЙНА:\r\n    </th>\r\n    <th>\r\n182,0\r\n    </th>\r\n	<th>\r\n181,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">99,7%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n181,9\r\n    </td>\r\n	<td>\r\n181,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,7%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n0,08\r\n    </td>\r\n	<td>\r\n0,08\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Загальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart2" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-red"></i> Податкові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 6,4%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Неподаткові надходження <span class="pull-right\r\n                text-green"><i class="fa\r\n                fa-angle-up"></i> 2,2%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Офіційны трансфери\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 0,1%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 14,6%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n            <div class="box-footer no-padding">\r\n\r\n            </div>\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n\r\n\r\n         <div class="col-md-7">\r\n   <h3> Cпеціальний фонд </h3>\r\n<table class="table table-budget">\r\n<tr>\r\n    <th>\r\n        Код\r\n    </th>\r\n    <th>\r\n        Назва статті витрати\r\n    </th>\r\n    <th>\r\n        Затверджено, тис. грн.\r\n    </th>\r\n	<th>\r\n        Витрачено, тис. грн.\r\n    </th>\r\n    <th>\r\n        Відсоток\r\n    </th>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВИКОНАВЧИЙ КОМІТЕТ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n16 136,5\r\n    </th>\r\n	<th>\r\n15 126,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">93,7%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n      Органи місцевого самоврядування\r\n    </td>\r\n    <td>\r\n     969,9\r\n    </td>\r\n	<td>\r\n    1 028,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">106,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n       Дошкільні заклади освіти\r\n    </td>\r\n    <td>\r\n      99,0\r\n    </td>\r\n	<td>\r\n     0,0\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">0,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n\r\n    </td>\r\n    <td>\r\n        Спеціалізовані лікарні та інші спеціалізовані заклади (центри, диспансери, госпіталі для інвалідів ВВВ, лепрозорії, медико - санітарні частини тощо, що мають ліжкову мережу)\r\n    </td>\r\n    <td>\r\n       1 705,5\r\n    </td>\r\n	<td>\r\n    1 593,4\r\n    </td>\r\n    <td>\r\n        <span class="badge bg-red">93,4%</span>\r\n    </td>\r\n</tr>\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n      Житлово - експлуатаційне господарство\r\n\r\n    </td>\r\n    <td>\r\n        64,7\r\n    </td>\r\n	<td>\r\n        64,7\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">100,0%</div>\r\n    </td>\r\n</tr>\r\n\r\n<tr >\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n    Капітальний ремонт житлового фонду місцевих органів влади\r\n\r\n    </td>\r\n    <td>\r\n5 228,9\r\n    </td>\r\n	<td>\r\n4 569,7\r\n    </td>\r\n    <th>\r\n        <div class="badge bg-green">87,4%</div>\r\n    </th>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\n    Благоустрій міст, сіл, селищ\r\n    </td>\r\n    <td>\r\n2 198,9\r\n    </td>\r\n	<td>\r\n2 194,2\r\n    </td>\r\n    <td>\r\n        <div class="badge bg-green">99,8%</div>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКомбінати комунальних підприємств , районні виробничі об"єднання та інші підприємства, установи та організації житлово - комунального господарства\r\n    </td>\r\n    <td>\r\n60,0\r\n    </td>\r\n	<td>\r\n60,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n<tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПроведення навчально-тренувальних зборів і змагань\r\n\r\n\r\n    </td>\r\n    <td>\r\n231,0\r\n    </td>\r\n	<td>\r\n230,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">99,8%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nКапітальні вкладення\r\n    </td>\r\n    <td>\r\n2 221,8\r\n    </td>\r\n	<td>\r\n2 102,3\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-red">94,6%</span>\r\n    </td>\r\n</tr><tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на проведення робіт, пов"язаних із будівництвом, реконструкцію, ремонтом та утриманням автомобільних доріг\r\n  \r\n    </td>\r\n    <td>\r\n1 460,2\r\n    </td>\r\n	<td>\r\n1 424,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">97,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОхорона та раціональне використання природних ресурсів\r\n    </td>\r\n    <td>\r\n59,3\r\n    </td>\r\n	<td>\r\n57,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">96,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦільові фонди, утворені Верховною радою Автономної Республіки Крим, органами місцевого самоврядування і місцевими органами виконавчої влади\r\n    </td>\r\n    <td>\r\n45,0\r\n    </td>\r\n	<td>\r\n0,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n    </td>\r\n    <td>\r\n1 342,5\r\n    </td>\r\n	<td>\r\n1 321,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">98,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки\r\n\r\n    </td>\r\n    <td>\r\n449,6\r\n    </td>\r\n	<td>\r\n480,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">106,8%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nНадання пільгового довгострокового кредиту громадянам на будівництво (реконструкцію)та придбання житла\r\n\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n0,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n        <span>+</span> ВІДДІЛ ОСВІТИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n    </th>\r\n    <th>\r\n7 904,4\r\n    </th>\r\n	<th>\r\n 9 089,4\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">115,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДошкільні заклади освіти\r\n\r\n    </td>\r\n    <td>\r\n4 194,5\r\n    </td>\r\n	<td>\r\n4 801,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">114,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗагальноосвітні школи ( в т.ч. школа-дитячий садок, інтернат при школі) спеціалізовані школи, ліцеї, гімназії, колегіуми\r\n\r\n    </td>\r\n    <td>\r\n3 302,7\r\n    </td>\r\n	<td>\r\n3 740,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">113,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПозашкільні заклади освіти, заходи із позашкільної роботи з дітьми\r\n\r\n    </td>\r\n    <td>\r\n25,9\r\n    </td>\r\n	<td>\r\n75,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">290,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМетодична робота, інші заходи у сфері народної освіти\r\n\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n77,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nГрупи централізованого господарського обслуговування\r\n\r\n    </td>\r\n    <td>\r\n0,0\r\n    </td>\r\n	<td>\r\n13,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n381,2\r\n    </td>\r\n	<td>\r\n381,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n<tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ФІНАНСОВЕ УПРАВЛІННЯ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n31,4\r\n    </th>\r\n	<th>\r\n30,2\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">96,1%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n    </td>\r\n    <td>\r\n31,4\r\n    </td>\r\n	<td>\r\n30,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">96,1%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВІДДІЛ ОСВІТИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n7 904,0\r\n    </th>\r\n	<th>\r\n9 089,4\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">115,0%</div>\r\n    </th>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nДошкільні заклади освіти\r\n\r\n    </td>\r\n    <td>\r\n4 194,5\r\n    </td>\r\n	<td>\r\n4 801,6\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">114,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЗагальноосвітні школи ( в т.ч. школа-дитячий садок, інтернат при школі, спеціалізовані школи, ліцеї, гімназії, колегіуми)\r\n\r\n    </td>\r\n    <td>\r\n3 302,7\r\n    </td>\r\n	<td>\r\n3 740,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">113,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПозашкільні заклади освіти, заходи із позашкільної роботи з дітьми\r\n    </td>\r\n    <td>\r\n25,9\r\n    </td>\r\n	<td>\r\n75,4\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">260,6%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМетодична робота, інші заходи у сфері народної освіти\r\n\r\n    </td>\r\n    <td>\r\n0\r\n    </td>\r\n	<td>\r\n77,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nГрупи централізованого господарського обслуговування\r\n    </td>\r\n    <td>\r\n0\r\n    </td>\r\n	<td>\r\n13,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n381,2\r\n    </td>\r\n	<td>\r\n381,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> ВІДДІЛ КУЛЬТУРИ ВИКОНАВЧОГО КОМІТЕТУ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n2 140,6\r\n    </th>\r\n	<th>\r\n2 274,1\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">90,7%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nБібліотеки\r\n\r\n    </td>\r\n    <td>\r\n53,1\r\n    </td>\r\n	<td>\r\n76,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">143,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nМузеї і виставки\r\n\r\n    </td>\r\n    <td>\r\n34,0\r\n    </td>\r\n	<td>\r\n49,8\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">146,4%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nПалаци і будинки культури, клуби та інші заклади клубного типу\r\n\r\n    </td>\r\n    <td>\r\n1 361,1\r\n    </td>\r\n	<td>\r\n1 361,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nШколи естетичного виховання дітей\r\n\r\n    </td>\r\n    <td>\r\n503,9\r\n    </td>\r\n	<td>\r\n586,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">116,3%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші культурно - освітні заклади та заходи\r\n\r\n    </td>\r\n    <td>\r\n16,5\r\n    </td>\r\n	<td>\r\n29,1\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">176,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n171,9\r\n    </td>\r\n	<td>\r\n171,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n<span>+</span> УПРАВЛІННЯ ПРАЦІ ТА СОЦІАЛЬНОГО ЗАХИСТУ НАСЕЛЕННЯ:\r\n\r\n    </th>\r\n\r\n    <th>\r\n602,9\r\n    </th>\r\n	<th>\r\n955,8\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">158,5%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n67,6\r\n    </td>\r\n	<td>\r\n74,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">110,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nІнші видатки на соціальний захист населення\r\n\r\n    </td>\r\n    <td>\r\n50,1\r\n    </td>\r\n	<td>\r\n44,0\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">87,9%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nТериторіальні центри соціального обслуговування (надання соціальних послуг)\r\n\r\n    </td>\r\n    <td>\r\n29,3\r\n    </td>\r\n	<td>\r\n341,2\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">1164,5%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nЦентри соціальної реабілітації дітей-інвалідів, центри професійної реабілітації інвалідів\r\n\r\n    </td>\r\n    <td>\r\n438,1\r\n    </td>\r\n	<td>\r\n477,7\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">109,0%</span>\r\n    </td>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nВидатки на покриття інших заборгованостей, що виникли у попередні роки\r\n\r\n    </td>\r\n    <td>\r\n17,9\r\n    </td>\r\n	<td>\r\n17,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">100%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr class="togg-budget">\r\n    <th colspan="2">\r\n        <span>+</span> ЦЕНТР СОЦІАЛЬНИХ СЛУЖБ ДЛЯ МОЛОДІ:\r\n    </th>\r\n    <th>\r\n0\r\n    </th>\r\n	<th>\r\n6,9\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">0,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nУтримання центрів соціальних служб сім"ї дітей та молоді\r\n\r\n    </td>\r\n    <td>\r\n0\r\n    </td>\r\n	<td>\r\n6,9\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">0,0%</span>\r\n    </td>\r\n</tr>\r\n\r\n    <tr class="togg-budget">\r\n\r\n    <th colspan="2">\r\n        <span>+</span> ФОНД КОМУНАЛЬНОГО МАЙНА:\r\n    </th>\r\n    <th>\r\n35,9\r\n    </th>\r\n	<th>\r\n43,5\r\n    </th>\r\n    <th>\r\n        <div class="badge bg-green">121,0%</div>\r\n    </th>\r\n</tr>\r\n    <tr>\r\n    <td>\r\n\r\n    </td>\r\n    <td>\r\nОргани місцевого самоврядування\r\n\r\n    </td>\r\n    <td>\r\n35,9\r\n    </td>\r\n	<td>\r\n43,5\r\n    </td>\r\n    <td>\r\n         <span class="badge bg-green">121,0%</span>\r\n    </td>\r\n</tr>\r\n</table>\r\n</div>\r\n\r\n        <div class="col-md-5">\r\n<h3> Візуалізація </h3>\r\n<div class="box box-default">\r\n            <div class="box-header with-border">\r\n              <h3 class="box-title">Спеціальний фонд</h3>\r\n\r\n            </div>\r\n            <!-- /.box-header -->\r\n            <div class="box-body">\r\n              <div class="row">\r\n                <div class="col-md-12">\r\n                  <div class="chart-responsive">\r\n                    <canvas id="pieChart3" height="320" width="410" style="width: 410px;\r\n                    height: 320px; max-width: 100%"></canvas>\r\n                  </div>\r\n                  <!-- ./chart-responsive -->\r\n                </div>\r\n                <!-- /.col -->\r\n                <div class="col-md-12">\r\n                  <ul class="nav nav-pills nav-stacked">\r\n                <li><i class="fa fa-circle-o text-grey"></i> Податкові надходження\r\n                  <span class="pull-right text-red"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                 <li><i class="fa fa-circle-o text-red"></i> Неподаткові надходження\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 47,3%</span></li>\r\n                <li><i class="fa fa-circle-o text-green"></i>  Інші субвенції <span class="pull-right\r\n                text-red"><i class="fa\r\n                fa-angle-down"></i> 1,5%</span>\r\n                </li>\r\n                <li><i class="fa fa-circle-o text-yellow"></i> Доходи від операцій з капіталом\r\n                  <span class="pull-right text-green"><i class="fa fa-angle-left"></i> 0,0%</span></li>\r\n                  <li><i class="fa fa-circle-o text-aqua"></i> Цільові фонди\r\n                  <span class="pull-right text-yellow"><i class="fa fa-angle-down"></i> 42,1%</span></li>\r\n              </ul>\r\n                </div>\r\n                <!-- /.col -->\r\n              </div>\r\n              <!-- /.row -->\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <!-- /.footer -->\r\n          </div>\r\n</div>\r\n    \r\n\r\n    <script>\r\n\r\n  //-------------\r\n  //- PIE CHART -\r\n  //-------------\r\n  // Get context with jQuery - using jQuery\'s .get() method.\r\n  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");\r\n  var pieChart = new Chart(pieChartCanvas);\r\n  var PieData = [\r\n    {\r\n      value: 35,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Податкові надходження: 98155,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Неподаткові надходження: 1032,0 тис.грн."\r\n    },\r\n    {\r\n      value: 45,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Офіційний трансферт: 120959,8 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Доходи від операцій з капіталом: 5,5 тис.грн."\r\n    },\r\n\r\n  ];\r\n  var pieChartCanvas = $("#pieChart1").get(0).getContext("2d");\r\n  var pieChart1 = new Chart(pieChartCanvas);\r\n  var PieData1 = [\r\n    {\r\n      value: 70,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Неподаткові надходження: 6111,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Інші субвенції: 1638,0 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Доходи від операцій з капіталом: 12,4 тис.грн."\r\n    },\r\n    {\r\n      value: 10,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Цільові фонди: 26,0 тис.грн."\r\n    },\r\n\r\n  ];\r\n  //-------------\r\n  //- PIE CHART -\r\n  //-------------\r\n  // Get context with jQuery - using jQuery\'s .get() method.\r\n  var pieChartCanvas = $("#pieChart2").get(0).getContext("2d");\r\n  var pieChart2 = new Chart(pieChartCanvas);\r\n  var PieData2 = [\r\n    {\r\n      value: 35,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Податкові надходження: 98155,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Неподаткові надходження: 1032,0 тис.грн."\r\n    },\r\n    {\r\n      value: 45,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Офіційний трансферт: 120959,8 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Доходи від операцій з капіталом: 5,5 тис.грн."\r\n    },\r\n\r\n  ];\r\n    //-------------\r\n  //- PIE CHART -\r\n  //-------------\r\n  // Get context with jQuery - using jQuery\'s .get() method.\r\n  var pieChartCanvas = $("#pieChart3").get(0).getContext("2d");\r\n  var pieChart3 = new Chart(pieChartCanvas);\r\n  var PieData3 = [\r\n    {\r\n      value: 35,\r\n      color: "#f56954",\r\n      highlight: "#f56954",\r\n      label: "Податкові надходження: 98155,6 тис.грн."\r\n    },\r\n    {\r\n      value: 15,\r\n      color: "#00a65a",\r\n      highlight: "#00a65a",\r\n      label: "Неподаткові надходження: 1032,0 тис.грн."\r\n    },\r\n    {\r\n      value: 45,\r\n      color: "#f39c12",\r\n      highlight: "#f39c12",\r\n      label: "Офіційний трансферт: 120959,8 тис.грн."\r\n    },\r\n    {\r\n      value: 5,\r\n      color: "#00c0ef",\r\n      highlight: "#00c0ef",\r\n      label: "Доходи від операцій з капіталом: 5,5 тис.грн."\r\n    },\r\n\r\n  ];\r\n  var pieOptions = {\r\n    //Boolean - Whether we should show a stroke on each segment\r\n    segmentShowStroke: true,\r\n    //String - The colour of each segment stroke\r\n    segmentStrokeColor: "#fff",\r\n    //Number - The width of each segment stroke\r\n    segmentStrokeWidth: 1,\r\n    //Number - The percentage of the chart that we cut out of the middle\r\n    percentageInnerCutout: 50, // This is 0 for Pie charts\r\n    //Number - Amount of animation steps\r\n    animationSteps: 100,\r\n    //String - Animation easing effect\r\n    animationEasing: "easeOutBounce",\r\n    //Boolean - Whether we animate the rotation of the Doughnut\r\n    animateRotate: true,\r\n    //Boolean - Whether we animate scaling the Doughnut from the centre\r\n    animateScale: false,\r\n    //Boolean - whether to make the chart responsive to window resizing\r\n    responsive: true,\r\n    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container\r\n    maintainAspectRatio: false,\r\n    //String - A legend template\r\n    legendTemplate: "dfs",\r\n    //String - A tooltip template\r\n    tooltipTemplate: "<%=label%>"\r\n  };\r\n  //Create pie or douhnut chart\r\n  // You can switch between pie and douhnut using the method below.\r\n\r\n   pieChart.Doughnut(PieData, pieOptions);\r\n   pieChart1.Doughnut(PieData1, pieOptions);\r\n   pieChart2.Doughnut(PieData2, pieOptions);\r\n   pieChart3.Doughnut(PieData3, pieOptions)\r\n    </script>\r\n\r\n  </div><!-- /.box-body -->\r\n\r\n          </div><!-- /.box -->\r\n\r\n        </section><!-- /.content -->\r\n\r\n\r\n{% endblock %}\r\n\r\n{% block footerjs %}\r\n\r\n\r\n    <script type="application/javascript">\r\n $(\'.togg-budget\').nextUntil(\'tr.togg-budget\').toggle();\r\n\r\n$(\'.togg-budget\').click(function(){\r\n   $(this).find(\'span\').text(function(_, value){return value==\'-\'?\'+\':\'-\'});\r\n    $(this).nextUntil(\'tr.togg-budget\').slideToggle(100, function(){\r\n    });\r\n});\r\n</script>');
/*!40000 ALTER TABLE `town_budgets` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.town_edrpou
CREATE TABLE IF NOT EXISTS `town_edrpou` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town_ref` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `town_ref_code_title` (`town_ref`,`code`,`title`),
  CONSTRAINT `FK_town_edrpou_town` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Коди ЄДРПОУ';

-- Дамп данных таблицы weunion.town_edrpou: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `town_edrpou` DISABLE KEYS */;
INSERT INTO `town_edrpou` (`id`, `town_ref`, `code`, `title`) VALUES
	(3, 1, '03315879', 'Кузнецовська міська рада'),
	(1, 5, '04387220', 'Зорянська сільська рада');
/*!40000 ALTER TABLE `town_edrpou` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.town_gromada
CREATE TABLE IF NOT EXISTS `town_gromada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `main_town_ref` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_town_gromada_town` (`main_town_ref`),
  CONSTRAINT `FK_town_gromada_town` FOREIGN KEY (`main_town_ref`) REFERENCES `town` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Громады ';

-- Дамп данных таблицы weunion.town_gromada: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `town_gromada` DISABLE KEYS */;
INSERT INTO `town_gromada` (`id`, `title`, `main_town_ref`) VALUES
	(1, 'Зорянська громада', 5),
	(2, 'Кузнецовська громада', 1);
/*!40000 ALTER TABLE `town_gromada` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.town_types
CREATE TABLE IF NOT EXISTS `town_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Типи населених пунктів';

-- Дамп данных таблицы weunion.town_types: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `town_types` DISABLE KEYS */;
INSERT INTO `town_types` (`id`, `title`) VALUES
	(1, 'місто'),
	(2, 'смт.'),
	(3, 'село');
/*!40000 ALTER TABLE `town_types` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

from django.views.generic import DetailView, ListView, RedirectView
from django.shortcuts import redirect
from django.contrib import messages
from weunion.settings import CRON_SECRET
from polls.models import Choice, Poll, Vote
import datetime
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied


class PollListView(ListView):

    def get_queryset(self):
        if 'town' in self.request.session:
            return Poll.objects.filter(active=1, town=self.request.session['town']).order_by('id','archive')
        else:
            #return reverse_lazy('regions')
            return Poll.objects.filter(active=3)


class PollDetailView(DetailView):
    model = Poll

    def get_context_data(self, **kwargs):
        context = super(PollDetailView, self).get_context_data(**kwargs)

        if not(self.request.session.has_key('town')):
            self.request.session['town'] = self.object.town.id
            self.request.session['town_name'] = self.object.town.name

        if(self.request.user.is_authenticated() and self.request.user.is_active):
            context['poll'].votable = self.object.can_vote(self.request.user)
        else:
            context['poll'].votable = False
        return context


class PollVoteView(RedirectView):
    def post(self, request, *args, **kwargs):
        poll = Poll.objects.get(id=kwargs['pk'])
        user = request.user
        choice = Choice.objects.get(id=request.POST['choice_pk'])
        Vote.objects.create(poll=poll, user=user, choice=choice)
        messages.success(request,"Дякуємо за Ваш голос")
        return redirect('../../', args=kwargs['pk'])
        #return super(PollVoteView, self).post(request, *args, **kwargs)

    def get_redirect_url(self, **kwargs):
        return redirect('../polls', args=[kwargs['pk']])

#проверяем кроном просроченые голосования и переносим в архивные
def checktimeout(request, secret, townslug):
    if(secret == CRON_SECRET):
       polls = Poll.objects.filter(active=1, archive=0)
       count = 0
       for poll in polls:
           if(poll.date_end <= datetime.date.today() ):
               poll.archive= 1 # делаем архивной если у петиции кончилось время сбора подписей и она не набрала нужного количества голосов
               poll.save()
               count +=1
       if(count):
           return HttpResponse('Done! Find: '+str(count)+' poll(-s)')
       else:
           return HttpResponse("Not found any polls that mutch enddate!")
    else:
        raise PermissionDenied('Досуп заборонено.')
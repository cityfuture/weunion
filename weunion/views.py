from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.template import RequestContext
from django.shortcuts import render, render_to_response, get_object_or_404,redirect
from defects.models import Town
from .helper import Towns
import urllib.request, json, requests
from .models import Regions, User
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt

#cache_page(60 * 200)
def index(request,townslug = None):
    if ("town" in request.session and "town_name" in request.session):
        slug = get_object_or_404(Town, pk=request.session["town"]).slug
        return redirect('/%s' % slug)

    if("town" not in request.session and "town_name" not in request.session):
        if(request.user.is_authenticated()):
             request.session["town"] = request.user.towns.all()[0].id
             request.session["town_name"] = request.user.towns.all()[0].name
             return redirect('/%s' % request.user.towns.all()[0].slug)

        else:
            from petitions.models import Petitions, PetitionsVoices
            from news.models import News
            from defects.models import Issues
            from polls.models import Poll, Vote
            from weunion.models import Modules

            statistic = {}
            statistic['petitions'] = Petitions.objects.all().count()
            statistic['petitions_voices'] = PetitionsVoices.objects.all().count()
            statistic['issues'] = len([i for i in Issues.objects.filter(parent_task_ref=None).order_by('-id')])
            statistic['news'] = News.objects.all().count()
            statistic['polls'] = Poll.objects.all().count()
            statistic['polls_votes'] = Vote.objects.all().count()
            statistic['towns'] = Town.objects.all().count()
            statistic['users'] = User.objects.all().count()
            statistic['modules'] = Modules.objects.all().count()
            return render(request, 'guestpage.html', {'statistic': statistic})

def redirectToSummary(request, townslug):
    return redirect('../%s/home' % townslug)

def errorPage(request,townslug, brockenlink):
    return redirect(request,'error.html',{'desc': "Неправильно вказане посилання на модуль, або такого модуля не існує"})

def index_town(request, townslug):
    if("town" in request.session and "town_name" in request.session):
        issues = None
        petitions = None
        news = None
        polls = None
        allowed = None
        town = get_object_or_404(Town,pk=request.session["town"])
        town_banners = town.townbanners_set.all()
        modules = [m.module.id for m in Town.objects.get(pk=request.session["town"]).townallowedmodules_set.all()]
        if(1 in modules):# Дефекты ЖКХ
            from defects.models import Issues
            issues = [i for i in Issues.objects.filter(parent_task_ref=None, town_ref=request.session["town"]).order_by('-id') if i.last_issue().status !=0 and i.last_issue().status !=3]

        if(2 in modules):#Петиции
            from petitions.models import Petitions
            petitions = Petitions.objects.filter(town = request.session["town"], status = 2).order_by('-id')[:3]

        if(3 in modules):#Новости
            from news.models import News
            news = News.objects.filter(town=request.session["town"], publish=1).order_by('-datetime_publish')[:3]

        if(13 in modules):#Голосование
            from polls.models import Poll
            polls = Poll.objects.filter(active=1,archive=0, town=request.session['town']).order_by('id')[:3]

        return render(request, 'summarypage.html',{'issues': issues, 'petitions': petitions, 'news': news, 'town': town,'town_banners': town_banners,'polls': polls})
    else:
        return HttpResponseRedirect('/regions/')



def test(request):

    return render(request,'test.html')

#вывести список городов согласно области
def towns(request, region_ref):
    region = get_object_or_404(Regions, id=region_ref)
    tws = Town.objects.filter(region_ref = region).all()
    return render(request, 'towns.html', {'towns': tws})

#the ABOUT page
def about(request):
    return render(request, 'about.html')

#the CONTACTS page
def contacts(request):
    return render(request, 'contacts.html')

#the HELP page
def help(request):
    return render(request, 'help.html')

#the JOIN page
def join(request):
    return render(request, 'join.html')

#про децентрализацию
def decentralization(request):
    return render(request, 'decentralization.html')

#партнеры
def partners(request):
    return render(request, 'partners.html')

#правила
def rules(request):
    return render(request, 'rules_common.html')

#Выбор города с помещением в сессию
def choosetown(request, townid):
    town_name = get_object_or_404(Town, id=townid).name
    slug = get_object_or_404(Town, id=townid).slug
    response = redirect('/%s' % slug)
    request.session["town"]=townid
    request.session["town_name"]=town_name
    return response

#Формируем название города в хэддере
def gettown(request):
     if("town" in request.session and "town_name" in request.session):
        return HttpResponse(request.session["town_name"])
     elif(request.user.is_authenticated()):
         town_name = request.user.towns.all()[0].name
         townid = request.user.towns.all()[0].id
         response =  HttpResponse(town_name)
         request.session["town"]=townid
         request.session["town_name"]=town_name
         return response
     else:
         return HttpResponse(False)

def _get_town_name(town_id):
    '''Возвращаем Имя города'''
    if(town_id):
         town_name = Town.objects.get(id=town_id).name
         return town_name
    else:
         return 'Вкажіть ID міста'

#страница регионов
def regions(request):
    return render(request,'regions.html')

#сраничка IGOV
def igov(request, townslug):
    if("town" in request.session and "town_name" in request.session):
        if(request.is_ajax()):
            req=urllib.request.Request("https://igov.org.ua/api/catalog?%s" % Town.objects.get(pk=request.session["town"]).additions.get(type = 'igov').body)
            res=urllib.request.urlopen(req)
            data=res.read().decode("UTF-8")
            data=json.loads(data)
            return render_to_response('_igov-ajax.html',{"sData":data})
        else:
            return render(request,'igov.html')
    else:
        return redirect('regions')

#сраничка Open Budget
def budget(request, townslug):
    if("town" in request.session):
            openbudget = Town.objects.get(pk = request.session['town']).openbudget.all()[0].body
            return render(request,'budget.html',{"openbudget": openbudget})
    else:
        redirect('regions')


def prozorro(request, townslug):
    """Вытягиваем данные из системы Прозоро"""
    if("town" in request.session):
        try:
            url = "https://bid.e-tender.biz/api/services/etender/tender/GetTenders"
            edrpou = Town.objects.get(pk=request.session["town"]).additions.get(type = 'prozorro').body
            data = json.loads(edrpou)
            headers = {'content-type': 'application/json'}
            r=requests.post(url, data=json.dumps(data), headers=headers)
            data=r.content.decode("UTF-8")
            data=json.loads(data)
        except:
            return render(request, 'prozorro.html', {"error": "Нечуване нахабство! Ми не можемо отримати доступ до сервера 'Prozorro'!"})

        return render(request,'prozorro.html',{"data":data })
    else:
        return redirect('regions')



def profile(request,user_id):
    """показываем профиль пользователя"""
    if(request.user.is_authenticated() and request.user.is_active):
        moder = request.user.isAllowedToModerate(request.session["town"], 'Profile')
        user = get_object_or_404(User, pk=user_id)
        allowed = (user.towns.all()[0] in [t for t in request.user.towns.all()] and moder)
        return render(request,'profile.html',{'allowed':allowed,'user':user})
    else:
        return redirect('/accounts/login')

def karma(request,user_id):
    """Листинг кармы пользователя"""
    from .helper import Karma
    if(request.user.is_authenticated() and request.user.is_active):
            try:
                page = request.GET.get('page', 1)
            except PageNotAnInteger:
                page = 1

            p = Paginator(Karma.list(get_object_or_404(User, pk=user_id)),10, request=request)
            points = p.page(page)
            return render(request,'karma_list.html',{'points': points})
    else:
        return redirect('/accounts/login')

def userban(request,user_id,status):
    if(request.user.is_authenticated() and request.user.is_active):
        user = get_object_or_404(User, pk=user_id)
        moder = request.user.isAllowedToModerate(request.session["town"], 'Profile')
        allowed = (user.towns.all()[0] in [t for t in request.user.towns.all()] and moder)
        if(allowed):
            user.is_active = int(status)
            user.save()
            return redirect('/profile/'+user_id)
        else:
            raise PermissionDenied("Доступ заборонено")
    else:
        raise PermissionDenied("Доступ заборонено")

#показываем пользовател
# ю после регистрации
def confirmyouremail(request):
    return render(request,'please_confirm_your_email.html')

#контакты модераторов
def moderators(request, townslug):
    if ('town' in request.session):
        moders = Town.objects.get(pk=request.session["town"]).additions.filter(type = 'moderators')[0].body
        return render(request,'moderators_info_page.html',{'moders': moders})
    else:
        return redirect('regions')

#панель модераторов
def moderator(request):
    if(request.user.is_authenticated() and request.user.is_active):
        town = request.user.towns.all()[0]
        if request.user.isAllowedToModerate(town.id):
            from defects.models import Issues
            users = town.user_set.all()
            petitions = town.petitions_set.all()
            issues = len([i for i in Issues.objects.filter(parent_task_ref=None, town_ref=town).order_by('-id')])
            news = town.news_set.all()
            return render(request,'moderators.html',{'users': users,'petitions': petitions,'issues': issues,'news':news})
        else:
            raise PermissionDenied
    else:
        raise PermissionDenied

#Звіт для модератора за період
def moderatorsZvit(request):
     from defects.models import Issues

     if(request.user.is_authenticated() and request.user.is_active):
        town = request.user.towns.all()[0]
        modules = [m.module.id for m in town.townallowedmodules_set.all()]
        if request.user.isAllowedToModerate(town.id):
            stat = {}

            if not(request.is_ajax()):
                return render(request,'moder_zvit.html')

            start = request.POST.get('start')
            end = request.POST.get('end')

            #Вытаскиваем число зарегистрированых пользователей
            stat['users'] = len(town.user_set.filter(date_joined__range = [start, end]))

            if(1 in modules):
                #Дефекты на сегодняшний день
                stat['defects'] = {}
                stat['defects']['add'] = len([i for i in Issues.objects.filter(parent_task_ref=None, town_ref=town, created__range = [start, end]).order_by('id') if i.last_issue()])
                stat['defects']['defects_done'] = len([i for i in Issues.objects.filter(parent_task_ref=None, town_ref=town, created__range = [start, end]).order_by('id') if i.last_issue().status ==2])
                stat['defects']['defects_open'] = len([i for i in Issues.objects.filter(parent_task_ref=None, town_ref=town, created__range = [start, end]).order_by('id') if i.last_issue().status ==1])
                stat['defects']['defects_planning'] = len([i for i in Issues.objects.filter(parent_task_ref=None, town_ref=town, created__range = [start, end]).order_by('id') if i.last_issue().status ==5])
                stat['defects']['defects_get'] = len([i for i in Issues.objects.filter(parent_task_ref=None, town_ref=town, created__range = [start, end]).order_by('id') if i.last_issue().status ==4])

            if(2 in modules):
                #Петиции на сегодняшний день
                stat['petitions'] = {}
                #Число добавленых петиций
                stat['petitions']['add'] = len(town.petitions_set.filter(create_date__range = [start, end]))
                stat['petitions']['1'] = len(town.petitions_set.filter(status=1)) # Модерация
                stat['petitions']['2'] = len(town.petitions_set.filter(status=2)) # Сбор подписей
                stat['petitions']['4'] = len(town.petitions_set.filter(status=4)) # Розглянута
                stat['petitions']['5'] = len(town.petitions_set.filter(status=5)) # Архівна
                stat['petitions']['6'] = len(town.petitions_set.filter(status=6)) # Розглядається
                stat['petitions']['8'] = len(town.petitions_set.filter(status=8)) # На перевірці голосів

            if(3 in modules):
                #Число опубликованых новостей
                stat['news'] = len(town.news_set.filter(datetime_publish__range = [start, end]))

            return render_to_response('_moder_zvit_ajax.html',{"stat":stat, 'start': start, 'end': end})

        else:
            raise PermissionDenied
     else:
         raise PermissionDenied


#Интеграция с DONOR.UA
#https://donor.ua/api/cities
def donor(request, townslug):
    if("town" in request.session and "town_name" in request.session):
            data = []
            try:
                req=urllib.request.Request('https://donor.ua/api/cities(%s)/Recipients'%(Town.objects.get(pk=request.session["town"]).additions.filter(type = 'donor')[0].body))
                res=urllib.request.urlopen(req)
                data=res.read().decode("UTF-8")
                data=json.loads(data)
            except:
                print('Помилка завантаження данних')

            return render(request,'donor.html',{'data':data})
    else:
        return redirect('regions')

#Рейтинг пользователей
def rating(request, townslug):
    list = []
    if("town" in request.session and "town_name" in request.session):
        from django.db.models import Sum
        list = []
        for user in get_object_or_404(Town,pk=request.session['town']).user_set.all().annotate(points_sum=Sum('karma__points')).order_by('-points_sum'):
            if not user.isAllowedToModerate(request.session['town']):
                u = user
                list.append(u)

        if(request.POST):
            list = list[:3]
            return render_to_response('_rating-ajax.html',{"list":list})
        else:
            return render(request,'user_rating.html',{'list': list})
    else:
        return redirect('regions')

#выловить динамический url и показать документ
def getDocums(request, url_param, townslug):
    if('town' in request.session):
        try:
            doc = get_object_or_404(Town, pk=request.session['town']).additions.get(type=url_param)
        except:
            raise ObjectDoesNotExist("Такого обє'єкта неіснує")
        ###############

        return render(request,'docs.html',{'doc': doc})
    else:
        return redirect('regions')
    return render(request,'docs.html',{'doc': doc})

def gromadasVillages(request, gromada_id):
    from .models import TownGromada

    gromada =  get_object_or_404(TownGromada,pk=gromada_id)

    return render(request, 'gromadas_villages.html', {'gromada':gromada})

def handler404(request):
    response = render_to_response('404.html', {},context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('500.html', {},context_instance=RequestContext(request))
    response.status_code = 500
    return response

@csrf_exempt
def townLiveSearch(request):
    from .models import TownsGromadasVillages
    result=[]
    for t in Town.objects.all():
        town = {}
        town['id'] = t.id
        town['name'] = t.name
        town['type'] = t.town_type.title
        town['region'] = t.region_ref.name
        town['gromada'] = "%s. " % t.towngromada_set.first().title if t.towngromada_set.first() else ''
        #town['slug'] = t.slug
        result.append(town)
    for v in TownsGromadasVillages.objects.all():
        village = {}
        village['id'] = v.gromada_ref.main_town_ref_id
        village['type'] = v.type_ref.title
        village['name'] = v.title
        village['region'] = v.gromada_ref.main_town_ref.region_ref.name
        village['gromada'] = "%s. " %  v.gromada_ref.title
        result.append(village)
    return JsonResponse(result, safe=False)

def townRedirect(request, townredirect):
    return redirect('/%s' % townredirect)
from django.contrib.auth import get_user_model
from django import forms
from defects.models import Town
from defects.models import User

class SignupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

    error_css_class = 'myerror'
    required_css_class = 'myrequired'

    class Meta:
        model = get_user_model()
        fields = ('towns', 'last_name', 'first_name', 'middle_name','phone', 'email')
        widgets = {
            'first_name': forms.TextInput(attrs={'placeholder': "Ведіть ваше ім'я, напр. Євген",'class': 'form-control input-lg'}),
            'last_name': forms.TextInput(attrs={'placeholder': "Введіть ваше прізвище, напр. Поремчук",'class': 'form-control input-lg'}),
            'middle_name': forms.TextInput(attrs={'placeholder': "Введіть ім'я по батькові, напр. Володимирович. Вимагається деякими модулями, напр. 'Петиції'",'class': 'form-control input-lg'}),
            'phone': forms.TextInput(attrs={'placeholder': "Вкажіть Ваш номер телефону, напр. 097 111 22 33",'class': 'form-control input-lg'}),
        }

    def signup(self, request, user):
        import string
        user.first_name = string.capwords(self.cleaned_data['first_name'].lower())
        user.middle_name = string.capwords(self.cleaned_data['middle_name'].lower())
        user.last_name = string.capwords(self.cleaned_data['last_name'].lower())
        user.phone = self.cleaned_data['phone']
        user.save()
        town=Town.objects.get(pk=self.cleaned_data['towns'][0].id)
        user.towns.add(town)
        user.save()

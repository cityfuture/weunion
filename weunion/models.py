from django.contrib.auth.models import AbstractUser
from django.db import models
import hashlib


class AttrLookup(object):#http://www.stavros.io/posts/function-calls-in-django-templates/
    def __init__(self, func, instance=None):
        self._func = func
        self._instance = instance

    def __get__(self, instance, owner):
        # Return a new instance of the decorator with the class embedded, rather than
        # store instance here, to avoid race conditions in multithreaded scenarios.
        return AttrLookup(self._func, instance)

    def __getitem__(self, argument):
        return self._func(self._instance, argument)

    def __call__(self, argument=None):
        # When resolving something like {{ user.can.change_number }}, Django will call
        # each element in the dot sequence in order (or otherwise try to access it).
        # When trying to call can(), that will fail, because it expects an extra
        # argument, so Django will fail and leave it at that.
        # If there's no argument passed, we return itself, so Django can continue with
        # the attribute lookup down the chain.
        if argument is None:
            return self
        else:
            return self._func(self._instance, argument)

class User(AbstractUser):

    work_for = models.ManyToManyField("Subcontractors",blank=True,verbose_name='Огранизацiя',related_name="user_set", related_query_name="user")
    towns = models.ManyToManyField("Town", blank=False,verbose_name='Населений пункт проживання або центр громади',related_name="user_set", related_query_name="user")
    phone = models.CharField(max_length=50, blank=False, null=True,verbose_name='Номер телефону')
    middle_name = models.CharField(max_length=50,verbose_name='По батькові', blank=False)
    #this stuff is needed to use this model with django auth as a custom user class
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    #Возвращаем хэш имейла для чата поддержки
    def chathash(self):
        return hashlib.sha224(self.email.encode('UTF-8')+self.middle_name.encode('UTF-8')).hexdigest()

    def __str__(self):
        if(len(self.first_name)<0 and len(self.last_name)<0):
           return self.username
        else:
           return self.get_full_name()

    def get_full_name_atall(self):
        """
        Returns the first_name plus the last_name, with a space in between without initials
        """
        full_name = '%s %s %s' % (self.last_name, self.first_name, self.middle_name )
        return full_name.strip()

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        firstname = '.'.join(name[0].upper() for name in self.first_name.split())
        midlename = '.'.join(name[0].upper() for name in self.middle_name.split())
        full_name = '%s %s. %s.' % (self.last_name, firstname, midlename )
        return full_name.strip()

    def get_initials(self):
        """
        Показываем полные инициалы: П.В.С
        """
        last_name = '.'.join(name[0].upper() for name in self.last_name.split())
        firstname = '.'.join(name[0].upper() for name in self.first_name.split())
        midlename = '.'.join(name[0].upper() for name in self.middle_name.split())
        full_name = '%s.%s.%s.' % (last_name, firstname, midlename )
        return full_name.strip()


#Общая функция для проверки прав на модерацию. Как город можно кидать ID
    def isAllowedToModerate(self, town, module=None):
        if self.is_authenticated():
            if self.isAdmin() or self.isModerator(town, module):
                return True
            else:
                return False
        else:
            return False

#Проверяем на админа. Админы это работники City of the Future. Полные привелегии
    def isAdmin(self):
        if self.is_authenticated():
            try:
                self.groups.get(name="Admin")
                return True
            except:
                return False
        else:
            return False


    #Проверяем на модератора. Модераторы - это представители в городах. Привязаны к городу и модулям, которые они обслуживают
    def isModerator(self, town, module=None):
        moder = False
        if self.is_authenticated():
            try:
                self.groups.get(name="Moderator")
                moder = True
            except:
                moder = False

            if (moder == False and module != None):
                try:
                    self.groups.get(name="Moderator_%s" % module)
                    moder = True
                except:
                    moder = False

            if moder:
                if town in [t.id for t in self.towns.all()]:
                    return True
                else:
                    return False

            return False
        else:
            return False


    #Проверяем на контролера. Котроллеры- это представители ОО в городах. Привязаны к городу и модулям, которые они контролируют
    def isController(self, town, module=None):
        moder = False
        if self.is_authenticated():
            if module == None:
                try:
                    self.groups.get(name="Controller")
                    moder = True
                except:
                    moder = False
            else:
                try:
                    self.groups.get(name="Controller_%s" % module)
                    moder = True
                except:
                    moder = False

            if moder:
                if town in self.towns.all():
                    return True
                else:
                    return False

            return False
        else:
            return False


    #до рефакторинга, пусай болтается
    def isModer(self):
        try:
            self.groups.get(name="Moderator")
            return True
        except:
            return False


    #test mixin that enables single parram to be passed from template like {{user.test.zzz}}
    @AttrLookup
    def test(self,param):
        return "Param is:%s" % param

    class Meta:
        managed = False
        db_table = 'auth_user'


    #Возвращаем очки кармы для пользователя
    def points(self):
        from django.db.models import Sum
        return self.karma.aggregate(Sum('points'))['points__sum'] or '0'



class Subcontractors(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    town_ref = models.ForeignKey('Town', db_column='town_ref', verbose_name="Город")


    class Meta:
        managed = False
        db_table = 'subcontractors'

    def __str__(self):
        return self.name


class Regions(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'regions'


class Town(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    slug = models.CharField(max_length=50, blank=False)
    votes = models.IntegerField(default=0)
    region_ref = models.ForeignKey(Regions, db_column='region_ref')
    town_type = models.ForeignKey('TownTypes', db_column='type_id')
    pet_days = models.IntegerField()
    pet_number_templ = models.CharField(max_length=10, blank=True, null=True)
    map_lon=models.CharField(max_length=128)
    map_lat=models.CharField(max_length=128)
    zoom=models.IntegerField(default=14)
    map_radius=models.IntegerField()
    menu = models.CharField(max_length=10000, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'town'


class TownTypes(models.Model):
    title = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'town_types'


class TownBanners(models.Model):
    imgsource = models.CharField(max_length=100, blank=True, null=True)
    town = models.ForeignKey(Town, db_column='town', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'town_banners'


class Modules(models.Model):
    title = models.CharField(max_length=15, blank=True, null=True)
    slug = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'modules'


class TownAllowedModules(models.Model):
    town = models.ForeignKey(Town, db_column='town', blank=True, primary_key=True)
    module = models.ForeignKey(Modules, db_column='module', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'town_allowed_modules'


class AuthUserKarma(models.Model):
    user_ref = models.ForeignKey(User, db_column='user_ref', blank=True, null=True,related_name="karma")
    points = models.IntegerField(blank=True, null=True)
    forwhat = models.CharField(max_length=255)
    module_ref = models.ForeignKey('Modules', db_column='module_ref')
    datetime = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = False
        db_table = 'auth_user_karma'

class TownBudgets(models.Model):
    year = models.IntegerField(blank=True, null=True)
    town_ref = models.ForeignKey(Town, db_column='town_ref', blank=True, null=True,related_name="openbudget")
    body = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'town_budgets'

class TownsAdditions(models.Model):
    town_ref = models.ForeignKey(Town, db_column='town_ref',related_name="additions")
    type = models.CharField(max_length=50)
    body = models.TextField()
    title = models.CharField(max_length=150, blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'towns_additions'


class TownGromada(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True)
    main_town_ref = models.ForeignKey(Town, db_column='main_town_ref')

    class Meta:
        managed = False
        db_table = 'town_gromada'


class TownEdrpou(models.Model):
    town_ref = models.ForeignKey(Town, db_column='town_ref')
    code = models.CharField(unique=True, max_length=10)
    title = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'town_edrpou'

class TownsGromadasVillages(models.Model):
    title = models.CharField(max_length=100)
    gromada_ref = models.ForeignKey(TownGromada, db_column='gromada_ref')
    type_ref = models.ForeignKey(TownTypes, db_column='type_ref')

    class Meta:
        managed = False
        db_table = 'towns_gromadas_villages'


EDATA_PARAMETERS_CHOICES = (
    ('amount', 'Сума транзакції (гривень)'),
    ('recipient_edrpou', 'ЄРДПОУ-код отримувача'),
    ('recipient_title', 'Назва отримувача (по масці)'),
    ('payer_edrpou', 'ЄРДПОУ-код платника'),
)

COMPARISON_CHARACTERS = (
    ('==','Дорівнює (==)'),
    ('!=','Не дорівнює (!=)'),
    ('>','Більше (>)'),
    ('<','Менше (<)'),
)

class Subscriptions(models.Model):
    type_ref = models.ForeignKey('SubscriptionsTypes', db_column='type_ref', blank=True, null=True)
    user_ref = models.ForeignKey(User, db_column='user_ref')
    town_ref = models.ForeignKey(Town, db_column='town_ref')
    is_active = models.IntegerField()
    parameter = models.CharField(max_length=50,choices=EDATA_PARAMETERS_CHOICES)
    comparison = models.CharField(max_length=10, choices=COMPARISON_CHARACTERS)
    value = models.CharField(max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)
    gencode = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'subscriptions'


class SubscriptionsTypes(models.Model):
    name = models.CharField(max_length=50)
    is_active = models.IntegerField()
    periodic = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'subscriptions_types'




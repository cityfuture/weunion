import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'm9pz6qnnwdfl3xamj4ksd5wp8rcjv0z3ymrs'
CRON_SECRET = '666'
EDATA_URL = "http://195.177.72.222:8080/api/rest/1.0/transactions"

DEBUG = False
TEMPLATE_DEBUG = DEBUG

if(DEBUG):
    ALLOWED_HOSTS = ['127.0.0.1','192.168.86.1']
    INTERNAL_IPS=['192.168.86.1','127.0.0.1']
else:
    ALLOWED_HOSTS = ['*']
    INTERNAL_IPS=['*']


URL = 'www.rozumnemisto.org/'

# Application definition
#
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'defects',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'django.contrib.sites',
    'petitions',
    'pure_pagination',
    'django_activeurl',
    'weunion',
    'news',
    'easy_pjax',
    'ckeditor_uploader',
    'polls',
    'crowdfunding',
    'edata',
    'smartroads',
    'smartapi'
    )

MIDDLEWARE_CLASSES = (
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'weunion.cp.TownSessionMiddleware'
)

AUTH_USER_MODEL="weunion.User"

#migth need to disable this leter
CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'weunion.urls'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request',
                'django.core.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'weunion.cp.towntemp',
            ],
        },
    },
]

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)


WSGI_APPLICATION = 'weunion.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'read_default_file': BASE_DIR+'/weunion/'+'mysqldb.conf',
        },
    }
}

TIME_ZONE = 'Europe/Kiev'

LANGUAGE_CODE = 'uk_UA'
DEFAULT_CHARSET = 'utf8'
'''
USE_I18N = True

USE_L10N = True

USE_TZ = True
'''


STATIC_URL = '/static/'
STATIC_ROOT2 = os.path.join('static')
STATICFILES_DIRS = ( os.path.join('static'), )


STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


GOOGLE_API_KEY='AIzaSyB-QYhoFZpnc8JP397b8r3gaF9u6pyot5o'

#for alloauth module
SITE_ID = 1
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

DJANGORESIZED_DEFAULT_SIZE = [150, 75]
DJANGORESIZED_DEFAULT_QUALITY = 75
DJANGORESIZED_DEFAULT_KEEP_META = True

#Логин пользователя чезез email
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
USER_MODEL_EMAIL_FIELD = 'email'
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True
ACCOUNT_EMAIL_VERIFICATION = "optional"
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = "/confirmyouremail"
ACCOUNT_SIGNUP_FORM_CLASS = 'weunion.forms.SignupForm'
ACCOUNT_LOGOUT_ON_GET = True
LOGIN_REDIRECT_URL = '/'

#SMTP settings
EMAIL_USE_SSL= True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'office@rozumnemisto.org'
EMAIL_HOST_PASSWORD = 'fxe_5190'
DEFAULT_FROM_EMAIL = 'Розумне місто <office@rozumnemisto.org>'
DEFAULT_TO_EMAIL = 'office@rozumnemisto.org'
TIMEOUT = 1

#Пегинация
PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 10,
    'MARGIN_PAGES_DISPLAYED': 2,
    'SHOW_FIRST_PAGE_WHEN_INVALID': True,
}

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'height': 300,
        'width': 935,
        'extraAllowedContent': 'iframe[*]',
    },
}

CKEDITOR_UPLOAD_PATH = "news/content/"
CKEDITOR_IMAGE_BACKEND = 'pillow'

ADMINS = (
    ('Poremchuk Evgeniy', 'e.poremchuk@gmail.com'),
)
MANAGERS = ADMINS


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': STATIC_ROOT2+'/django_cache',
    }
}


KARMA = {
    'SIGNUP': 5,
    'PETITION_WAS_APPROVE': 10,
    'PETITION_VOTE': 1,
    'PETITION_DISVOTE': -1,
    'PETITION_ONCONSIDERATION': 30,
    'DEFECT_WAS_APPROVE': 5,
    'DEFECT_COMMENT': 1,
    'DEFECT_COMMENT_BLOCK': -5,
}

CKEDITOR_ALLOW_NONIMAGE_FILES = False 
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings
from . import views, urls_modules
from ckeditor_uploader import views as viewsck
from django.views.decorators.cache import cache_page
from django.views.decorators.cache import never_cache

urlpatterns = [
    url(r'^$',views.index, name='index'),
    url(r'^admin_rm/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^crowdfunding/', include('crowdfunding.urls')),
    url(r'^moderator/$', views.moderator, name="moderator"),
    url(r'^moderator/zvit', views.moderatorsZvit, name="moderatorsZvit"),
    url(r'^towns/+(?P<region_ref>[0-9]+)', views.towns, name="towns"),
    url(r'^choosetown/+(?P<townid>[0-9]+)', views.choosetown, name="choosetown"),
    url(r'^gromadasvillges/+(?P<gromada_id>[0-9]+)', views.gromadasVillages, name="gromadasvillges"),
    url(r'^regions/', views.regions, name="regions"),
    url(r'^gettown', views.gettown, name="gettown"),
    url(r'^about', views.about),
    url(r'^help', views.help),
    url(r'^partners', views.partners),
    url(r'^decentralization', views.decentralization),
    url(r'^contacts',views.contacts),
    url(r'^join',views.join),
    url(r'^api/',include('smartapi.urls', namespace='smartapi')),
    url(r'^rating',views.rating),
    url(r'^rules',views.rules),
    url(r'^test', views.test),
    url(r'^confirmyouremail',views.confirmyouremail),
    url(r'^profile/karma/+(?P<user_id>[0-9]+)',views.karma, name="karma"),
    url(r'^profile/+(?P<user_id>[0-9]+)',views.profile, name="profile"),
    url(r'^profile/userban/+(?P<user_id>[0-9]+)/+(?P<status>[0-9]+)',views.userban, name="userban"),
    url(r'^ckeditor/upload/', viewsck.upload, name='ckeditor_upload'),
    url(r'^ckeditor/browse/', never_cache(viewsck.browse), name='ckeditor_browse'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes':False}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT2, 'show_indexes':False}),
    url(r'^townslivesearch', views.townLiveSearch),
    url(r'^town/(?P<townredirect>.*)$', views.townRedirect),
    url(r'(?P<townslug>[a-z]+)/',include(urls_modules)),
]